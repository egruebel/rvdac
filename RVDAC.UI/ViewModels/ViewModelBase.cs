﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using RVDAC.Common;
using RVDAC.Common.ScsClient;
using RVDAC.UI.Views;

namespace RVDAC.UI
{
    public class ViewModelBase: INotifyPropertyChanged 
    {
        public enum FormCloseRoute
        {
            Cancel, Submit, Default
        }

        public Action Close { get; set; }

        public FormCloseRoute CloseMethod { get; set; } = FormCloseRoute.Default;

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        public ICommand ShowLogDisplay
        {
            get { return new RelayCommand(ShowLogDisplayExecute, CanShowLogDisplay); }
        }

        bool CanShowLogDisplay()
        {
            return true;
        }

        void ShowLogDisplayExecute()
        {
            var p = new LogDisplay();
            p.Show();
        }
    }
}
