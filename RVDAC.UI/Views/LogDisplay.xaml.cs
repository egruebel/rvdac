﻿using System.Collections.Generic;
using System.Windows;
using RVDAC.Common.LogFile;

namespace RVDAC.UI.Views
{
    /// <summary>
    /// Interaction logic for DataStreamLogDisplay.xaml
    /// </summary>
    public partial class LogDisplay : Window
    {
        public LogDisplay()
        {
            InitializeComponent();
        }

        public List<LogFile.LogObject> Errors
        {
            get { return LogFile.GetErrors(); }
        }

        public List<LogFile.LogObject> Infos
        {
            get { return LogFile.GetInfo(); }
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

    }
}
