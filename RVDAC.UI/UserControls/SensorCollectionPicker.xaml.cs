﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
using RVDAC.Common;
using RVDAC.Common.Models;

namespace RVDAC.UI.UserControls
{
    /// <summary>
    /// Interaction logic for SensorCollectionPicker.xaml
    /// </summary>
    public partial class SensorCollectionPicker : UserControl
    {
        public SensorCollectionPicker()
        {
            InitializeComponent();

            //Stops Visual Studio from running this code in the Designer Window
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject())) return;

            //SensorCollection = new List<Guid>();

            SensorConfig = GetSensorConfig();
            if (SensorConfig == null)
            {
                ShowCriticalError("Unable to load the current sensor configuration.", "Invalid configuration");
                return;
            }

            Loaded += SensorCollectionPicker_Loaded;

            
        }

        private SensorConfiguration SensorConfig { get; set; }

        public ObservableCollection<SensorCollectionItem> SensorCollection
        {

            get { return (ObservableCollection<SensorCollectionItem>)GetValue(SensorCollectionProperty); }
            set { SetValue(SensorCollectionProperty, value); }
        }

        public static readonly DependencyProperty SensorCollectionProperty = DependencyProperty.Register("SensorCollection", typeof(ObservableCollection<SensorCollectionItem>),
            typeof(SensorCollectionPicker), new PropertyMetadata(null, OnSensorCollectionChanged));

        public bool NumericSensorsOnly
        {
            get { return (bool) GetValue(NumericSensorsOnlyProperty); }
            set { SetValue(NumericSensorsOnlyProperty, value); }
        }

        public static readonly DependencyProperty NumericSensorsOnlyProperty =
            DependencyProperty.Register("NumericSensorsOnly", typeof (bool),
                typeof (SensorCollectionPicker), new PropertyMetadata(true));

        public static readonly DependencyProperty IncludeParentSensorsProperty =
            DependencyProperty.Register("IncludeParentSensors", typeof(bool),
                typeof(SensorCollectionPicker), new PropertyMetadata(false));

        public bool IncludeParentSensors
        {
            get { return (bool)GetValue(IncludeParentSensorsProperty); }
            set { SetValue(IncludeParentSensorsProperty, value); }
        }

        private ObservableCollection<SensorPickerItem> SensorsInConfigDictionary { get; set; } = new ObservableCollection<SensorPickerItem>();

        public ObservableCollection<SensorPickerItem> SensorsSelectedDictionary { get; set; } = new ObservableCollection<SensorPickerItem>();

        public ObservableCollection<SensorPickerItem> SensorsAvailableDictionary { get; set; } = new ObservableCollection<SensorPickerItem>();

        private void SensorCollectionPicker_Loaded(object sender, RoutedEventArgs e)
        {
            //Create dictionary of all sensors
            IEnumerable<NodeBase> allSensors;

            allSensors = IncludeParentSensors
                ? SensorConfig.AllSensors()
                : (NumericSensorsOnly ? SensorConfig.AllNumericSensors() : SensorConfig.AllChildSensors());


            foreach (var sensor in allSensors)
            {
                SensorsInConfigDictionary.Add(new SensorPickerItem(sensor));
            }
            SensorCollectionChanged();
        }

        static void OnSensorCollectionChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            //if (sender == null)
                //return;
            //(sender as SensorCollectionPicker).SensorCollectionChanged();
        }

        private void SensorCollectionChanged()
        {
            //SensorsDictionary = new ObservableCollection<KeyValuePair<Guid, string>>();
            SensorsAvailableDictionary.Clear();
            SensorsSelectedDictionary.Clear();
            
            //Create dictionary of sensors in this profile
            foreach (var sci in SensorCollection)
            {
                foreach (var spi in SensorsInConfigDictionary)
                {
                    if (spi.Guid.Equals(sci.Guid))
                    {
                        spi.UseRaw = sci.UseRawValue;
                        SensorsSelectedDictionary.Add(spi);
                    }
                }
            }

            //Create a dictionary of the remaining sensors
            foreach (var sensor in SensorsInConfigDictionary)
            {
                if (!SensorsSelectedDictionary.Contains(sensor))
                {
                    SensorsAvailableDictionary.Add(sensor);
                }
            }
        }

        
        private void BtnAddSingle_OnClick(object sender, RoutedEventArgs e)
        {
            var selected = new List<SensorPickerItem>();
            foreach (SensorPickerItem selectedItem in AvailableFields.SelectedItems)
            {
                selected.Add(selectedItem);
            }
            //The ListBox.SelectedItems collection is NOT ORDERED! this has to be corrected
            selected = selected.OrderBy(x => AvailableFields.Items.IndexOf(x)).ToList();
            foreach (var item in selected)
            {
                SensorsSelectedDictionary.Add(item);
                SensorsAvailableDictionary.Remove(item);
            }
            UpdateSensorCollectionBinding();
        }

        private void BtnAddAll_OnClick(object sender, RoutedEventArgs e)
        {
            SensorsSelectedDictionary.Clear();
            SensorsAvailableDictionary.Clear();
            foreach (var s in SensorsInConfigDictionary)
            {
                SensorsSelectedDictionary.Add(s);
            }
            UpdateSensorCollectionBinding();
        }

        private void BtnRemoveSingle_OnClick(object sender, RoutedEventArgs e)
        {
            var selected = new List<SensorPickerItem>();
            foreach (SensorPickerItem selectedItem in IncludedFields.SelectedItems)
            {
                selected.Add(selectedItem);
            }
            foreach (var item in selected)
            {
                SensorsSelectedDictionary.Remove(item);
                SensorsAvailableDictionary.Add(item);
            }
            UpdateSensorCollectionBinding();
        }

        private void BtnRemoveAll_OnClick(object sender, RoutedEventArgs e)
        {
            SensorsSelectedDictionary.Clear();
            SensorsAvailableDictionary.Clear();
            foreach (var s in SensorsInConfigDictionary)
            {
                SensorsAvailableDictionary.Add(s);
            }
            UpdateSensorCollectionBinding();
        }

        private void MenuItemUp_OnClick(object sender, RoutedEventArgs e)
        {
            //if nothing is selected then exit
            if (IncludedFields.SelectedItems.Count == 0)
                return;
            //create a copy of the selected items
            var selected = new List<SensorPickerItem>();
            foreach (SensorPickerItem selectedItem in IncludedFields.SelectedItems)
            {
                selected.Add(selectedItem);
            }
            //The ListBox.SelectedItems collection is NOT ORDERED! this has to be corrected
            selected = selected.OrderBy(x => IncludedFields.Items.IndexOf(x)).ToList();
            //if the last selected item is already at the bottom of the listbox then don't move anything
            if (SensorsSelectedDictionary.IndexOf(selected.First()) == 0)
                return;
            foreach (var item in selected)
            {
                var index = SensorsSelectedDictionary.IndexOf(item);
                if (index != 0)
                    SensorsSelectedDictionary.Move(index, index - 1);
            }
            UpdateSensorCollectionBinding();
        }

        private void MenuItemDown_OnClick(object sender, RoutedEventArgs e)
        {
            //if nothing is selected then exit
            if (IncludedFields.SelectedItems.Count == 0)
                return;
            //create a copy of the selected items
            var selected = new List<SensorPickerItem>();
            foreach (SensorPickerItem selectedItem in IncludedFields.SelectedItems)
            {
                selected.Add(selectedItem);
            }
            //The ListBox.SelectedItems collection is NOT ORDERED! this has to be corrected
            selected = selected.OrderBy(x => IncludedFields.Items.IndexOf(x)).ToList();
            //if the last selected item is already at the bottom of the listbox then don't move anything
            if (SensorsSelectedDictionary.IndexOf(selected.Last()) == SensorsSelectedDictionary.Count - 1)
                return;
            //go from the bottom up to avoid a behavior where selected items can't be replaced in a listbox
            selected.Reverse();
            foreach (var item in selected)
            {
                var index = SensorsSelectedDictionary.IndexOf(item);
                if (index != SensorsSelectedDictionary.Count - 1)
                    SensorsSelectedDictionary.Move(index, index + 1);
            }
            UpdateSensorCollectionBinding();
        }

        private void UpdateSensorCollectionBinding()
        {
            SensorCollection.Clear();
            foreach (var item in SensorsSelectedDictionary)
            {
                SensorCollection.Add(item.ToSensorCollectionItem());
            }
        }

        private void ShowCriticalError(string message, string title)
        {
            MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Asterisk);
        }

        private static SensorConfiguration GetSensorConfig()
        {
            //First see if the latest config is in this application directory
            var fi = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "\\Sensor.xml");
            if (fi.Exists)
            {
                //Load sensor config from this application's directory
                try
                {
                    var xDoc = XDocument.Load(fi.FullName);
                    var cfg = new SensorConfiguration();
                    var builder = new ConfigImporter(xDoc, cfg);
                    if (builder.TryBuildFromExtendedScs())
                        return cfg;
                }
                catch
                {

                }
            }
            //If you're here then a valid Sensor.xml file could not be found in the application directory
            // Create OpenFileDialog 
            var dlg = new Microsoft.Win32.OpenFileDialog()
            {
                DefaultExt = ".xml",
                Filter = "XML Files (*.xml)|*.xml"
            };

            // Display OpenFileDialog by calling ShowDialog method 
            var result = dlg.ShowDialog();

            if (result == true)
            {
                try
                {
                    var xDoc = XDocument.Load(dlg.FileName);
                    var cfg = new SensorConfiguration();
                    var builder = new ConfigImporter(xDoc, cfg);
                    if (builder.TryBuildFromExtendedScs())
                        return cfg;
                }
                catch
                {
                    return null;
                }
            }
            return null;
        }

        public class SensorPickerItem
        {
            public SensorPickerItem(NodeBase sensor)
            {
                Name = sensor.Name;
                Guid = sensor.Guid;
                UseRaw = false;
            }

            public SensorCollectionItem ToSensorCollectionItem()
            {
                return new SensorCollectionItem()
                {
                    Guid = Guid,
                    UseRawValue = UseRaw
                };
            }

            public string Name { get; set; }
            public Guid Guid { get; set; }
            public bool UseRaw { get; set; }
        }

        private void ToggleButton_OnChecked(object sender, RoutedEventArgs e)
        {
            UpdateSensorCollectionBinding();
        }
    }

    
}
