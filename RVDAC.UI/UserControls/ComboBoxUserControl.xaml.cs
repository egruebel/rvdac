﻿using System.Collections;
using System.Windows;
using System.Windows.Controls;


namespace RVDAC.UI.UserControls
{
    /// <summary>
    /// Interaction logic for ComboBoxUserControl.xaml
    /// </summary>
    public partial class ComboBoxUserControl : UserControl
    {
        public ComboBoxUserControl()
        {
            InitializeComponent();
            
        }

        public static DependencyProperty ComboBoxSelectedValueProperty = DependencyProperty.Register(nameof(ComboBoxSelectedValue), typeof(object),
            typeof(ComboBoxUserControl), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public static DependencyProperty ComboBoxItemsSourceProperty = DependencyProperty.Register(nameof(ComboBoxItemsSource), typeof(IEnumerable),
            typeof(ComboBoxUserControl), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public static DependencyProperty ComboBoxDisplayMemberPathProperty = DependencyProperty.Register(nameof(ComboBoxDisplayMemberPath), typeof(string),
            typeof(ComboBoxUserControl), new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public static DependencyProperty ComboBoxSelectedValuePathProperty = DependencyProperty.Register(nameof(ComboBoxSelectedValuePath), typeof(string),
            typeof(ComboBoxUserControl), new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public static DependencyProperty LabelTextProperty = DependencyProperty.Register(nameof(LabelText), typeof(string),
            typeof(ComboBoxUserControl), new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public static DependencyProperty SubLabelTextProperty = DependencyProperty.Register(nameof(SubLabelText), typeof(string),
            typeof(ComboBoxUserControl), new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public static DependencyProperty ComboBoxEnabledProperty = DependencyProperty.Register(nameof(ComboBoxEnabled),
            typeof(bool),
            typeof(ComboBoxUserControl),
            new FrameworkPropertyMetadata(true, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public object ComboBoxSelectedValue
        {
            get { return (string)GetValue(ComboBoxSelectedValueProperty); }
            set
            {
                SetValue(ComboBoxSelectedValueProperty, value);
                
            }
        }

        public object ComboBoxItemsSource
        {
            get { return (string)GetValue(ComboBoxItemsSourceProperty); }
            set
            {
                SetValue(ComboBoxItemsSourceProperty, value);

            }
        }

        public string ComboBoxDisplayMemberPath
        {
            get { return (string)GetValue(ComboBoxDisplayMemberPathProperty); }
            set
            {
                SetValue(ComboBoxDisplayMemberPathProperty, value);

            }
        }

        public string ComboBoxSelectedValuePath
        {
            get { return (string) GetValue(ComboBoxSelectedValuePathProperty); }
            set { SetValue(ComboBoxSelectedValuePathProperty, value); }
        }

        public string LabelText
        {
            get { return (string)GetValue(LabelTextProperty); }
            set
            {
                SetValue(LabelTextProperty, value);
                
            }
        }

        public string SubLabelText
        {
            get { return (string)GetValue(LabelTextProperty); }
            set
            {
                SetValue(LabelTextProperty, value);
                
            }
        }

        public bool ComboBoxEnabled
        {
            get { return (bool)GetValue(ComboBoxEnabledProperty); }
            set
            {
                SetValue(ComboBoxEnabledProperty, value);

            }
        }
    }
}
