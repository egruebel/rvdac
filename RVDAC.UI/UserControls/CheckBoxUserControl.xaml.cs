﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RVDAC.UI.UserControls
{
    /// <summary>
    /// Interaction logic for CheckBoxUserControl.xaml
    /// </summary>
    public partial class CheckBoxUserControl : UserControl
    {
        public CheckBoxUserControl()
        {
            InitializeComponent();
        }

        public static DependencyProperty CheckBoxIsCheckedProperty = DependencyProperty.Register(nameof(CheckBoxIsChecked), typeof(bool),
            typeof(CheckBoxUserControl), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public static DependencyProperty LabelTextProperty = DependencyProperty.Register(nameof(LabelText), typeof(string),
            typeof(CheckBoxUserControl), new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public static DependencyProperty SubLabelTextProperty = DependencyProperty.Register(nameof(SubLabelText), typeof(string),
            typeof(CheckBoxUserControl), new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public static DependencyProperty CheckBoxEnabledProperty = DependencyProperty.Register(nameof(CheckBoxEnabled),
            typeof(bool),
            typeof(CheckBoxUserControl),
            new FrameworkPropertyMetadata(true, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public bool CheckBoxIsChecked
        {
            get { return (bool)GetValue(CheckBoxIsCheckedProperty); }
            set
            {
                SetValue(CheckBoxIsCheckedProperty, value);

            }
        }

        public string LabelText
        {
            get { return (string)GetValue(LabelTextProperty); }
            set
            {
                SetValue(LabelTextProperty, value);

            }
        }

        public string SubLabelText
        {
            get { return (string)GetValue(LabelTextProperty); }
            set
            {
                SetValue(LabelTextProperty, value);

            }
        }

        public bool CheckBoxEnabled
        {
            get { return (bool)GetValue(CheckBoxEnabledProperty); }
            set
            {
                SetValue(CheckBoxEnabledProperty, value);

            }
        }
    }
}
