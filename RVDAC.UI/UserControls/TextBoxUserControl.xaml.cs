﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RVDAC.UI.UserControls
{
    /// <summary>
    /// Interaction logic for TextBlockInputControl.xaml
    /// </summary>
    public partial class TextBoxUserControl : UserControl
    {
        public TextBoxUserControl()
        {
            InitializeComponent();
            //this.DataContext = this;
        }

        public static DependencyProperty TextBoxTextProperty = DependencyProperty.Register("Text", typeof(string),
            typeof(TextBoxUserControl), new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public static DependencyProperty LabelTextProperty = DependencyProperty.Register("LabelText", typeof(string),
            typeof(TextBoxUserControl), new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public static DependencyProperty SubLabelTextProperty = DependencyProperty.Register("SubLabelText", typeof(string),
            typeof(TextBoxUserControl), new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public static DependencyProperty TextBoxIsEnabledProperty = DependencyProperty.Register("IsEnabled", typeof(bool),
            typeof(TextBoxUserControl), new FrameworkPropertyMetadata(true, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public string Text { 
            get { return (string)GetValue(TextBoxTextProperty); }
            set { SetValue(TextBoxTextProperty, value); }
        }

        public object LabelText
        {
            get { return (string)GetValue(LabelTextProperty); }
            set { SetValue(LabelTextProperty, value); }
        }

        public object SubLabelText
        {
            get { return (string)GetValue(SubLabelTextProperty); }
            set { SetValue(SubLabelTextProperty, value); }
        }

        public new bool IsEnabled
        {
            get { return (bool)GetValue(TextBoxIsEnabledProperty); }
            set { SetValue(TextBoxIsEnabledProperty, value); }
        }

        public TextWrapping TextWrapping
        {
            get { return FieldTextBox.TextWrapping; }
            set { FieldTextBox.TextWrapping = value; }
        }

        public bool AcceptsReturns
        {
            get { return FieldTextBox.AcceptsReturn; }
            set { FieldTextBox.AcceptsReturn = value; }
        }

        public ScrollBarVisibility VerticalScrollBarVisibility
        {
            get { return FieldTextBox.VerticalScrollBarVisibility; }
            set { FieldTextBox.VerticalScrollBarVisibility = value; }
        }
        
    }
}
