﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RVDAC.Common.ScsClient;

namespace RVDAC.UI.UserControls
{
    /// <summary>
    /// Interaction logic for ScsClientIoUserControl.xaml
    /// </summary>
    public partial class ScsClientIoUserControl : UserControl
    {
        public ScsClientIoUserControl()
        {
            InitializeComponent();
        }

        public static DependencyProperty ScsClientProperty = DependencyProperty.Register("ScsClient", typeof(ScsClient),
            typeof(ScsClientIoUserControl), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public ScsClient ScsClient
        {
            get { return (ScsClient)GetValue(ScsClientProperty); }
            set
            {
                SetValue(ScsClientProperty, value);

            }
        }
    }
}
