﻿using System.Collections.Generic;
using System.Windows.Input;
using RVDAC.Common;
using RVDAC.Common.Models;

namespace RVDAC.Config.ViewModels
{
    public class PhysicalParentViewModel : ViewModelBase
    {
        private PhysicalParentNode _sensor;

        public PhysicalParentViewModel()
        {
            Sensor = new PhysicalParentNode();
        }

        public PhysicalParentViewModel(PhysicalParentNode sensor)
        {
            Sensor = sensor;
        }

        public PhysicalParentNode Sensor
        {
            get { return _sensor; }
            set
            {
                _sensor = value;
                OnPropertyChanged(nameof(Sensor));
            }
        }


        public static Dictionary<int, string> AsciiTerminations
        {
            get
            {
                var a = new Dictionary<int, string>();
                a.Add(0, "NONE (null)");
                a.Add(1, "SOH (01)");
                a.Add(2, "STX (02)");
                a.Add(3, "ETX (03)");
                a.Add(4, "EOT (04)");
                a.Add(5, "ENQ (05)");
                a.Add(6, "ACK (06)");
                a.Add(7, "BEL (07)");
                a.Add(8, "BS  (08)");
                a.Add(9, "TAB (09)");
                a.Add(10, "LF (10)");
                a.Add(11, "VT (11)");
                a.Add(12, "FF (12)");
                a.Add(13, "CR (13)");
                a.Add(14, "SO (14)");
                a.Add(15, "SI (15)");
                a.Add(16, "DLE (16)");
                a.Add(17, "DC1 (17)");
                a.Add(18, "DC2 (18)");
                a.Add(19, "DC3 (19)");
                a.Add(20, "DC4 (20)");
                a.Add(21, "NAK (21)");
                a.Add(22, "SYN (22)");
                a.Add(23, "ETB (23)");
                a.Add(24, "CAN (24)");
                a.Add(25, "EM  (25)");
                a.Add(26, "SUB (26)");
                a.Add(27, "ESC (27)");
                a.Add(28, "FS  (28)");
                a.Add(29, "GS  (29)");
                a.Add(30, "RS  (30)");
                a.Add(31, "US  (31)");
                a.Add(32, "SPACE (32)");
                return a;
            }
        }
    }
}