﻿using System;
using System.Collections.Generic;
using System.Linq;
using RVDAC.Common.Models;

namespace RVDAC.Config.ViewModels
{
    public class ChildViewModel : ViewModelBase
    {
        private ChildNode _sensor;

        public ChildViewModel()
        {
            //Dt = DateTime.Now.ToString();
            Sensor = new ChildNode();
        }

        public List<ChildNode.DecodeTypeEnum> GetDecodeTypes
        {
            get
            {
                return Enum.GetValues(typeof (ChildNode.DecodeTypeEnum)).OfType<ChildNode.DecodeTypeEnum>().ToList();
            }
        }

        public ChildViewModel(ChildNode sensor)
        {
            Sensor = sensor;
        }

        public ChildNode Sensor
        {
            get { return _sensor; }
            set
            {
                _sensor = value;
                OnPropertyChanged(nameof(Sensor));
                _sensor.PropertyChanged += _sensor_PropertyChanged;
            }
        }

        public bool SensorIsSerial
        {
            get { return Sensor?.DeviceType == NodeBase.DeviceTypeEnum.SerialChild; }
        }

        public bool SensorIsDelimited
        {
            get { return Sensor?.DeviceType == NodeBase.DeviceTypeEnum.NmeaChild; }
        }

        public
            void _sensor_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "DecodeType" && Sensor.DecodeType == ChildNode.DecodeTypeEnum.Navsec)
            {
                OnPropertyChanged(nameof(IsNavsec));
            }
        }

        public bool IsNavsec
        {
            get { return Sensor.DecodeType == ChildNode.DecodeTypeEnum.Navsec; }
        }
    }
}