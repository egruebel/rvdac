﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using RVDAC.Common;

namespace RVDAC.Config.ViewModels
{
    class ClassEditorViewModel: INotifyPropertyChanged
    {
        private string _newMeasurementClass;

        public struct MeasurementClassModel
        {
            public string ClassName { get; set; }
            public bool CanDelete { get; set; }
        }

        public Action FormCloseAction { get; set; }

        public ObservableCollection<MeasurementClassModel> MeasurementClasses { get; set; } = new ObservableCollection<MeasurementClassModel>();

        public string NewMeasurementClass
        {
            get { return _newMeasurementClass; }
            set
            {
                if (_newMeasurementClass != value)
                {
                    _newMeasurementClass = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("NewMeasurementClass"));
                }
            }
        }

        public ICommand AddNewMeasurementCommand
        {
            get { return new RelayCommand(AddMeasurementClassExecute, CanAddNewMeasurementClass); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        bool CanAddNewMeasurementClass()
        {
            if (string.IsNullOrEmpty(NewMeasurementClass))
                return false;
            if (NewMeasurementClass.Contains(" "))
                return false;
            return true;
        }

        void AddMeasurementClassExecute()
        {
            MeasurementClasses.Add(new MeasurementClassModel()
            {
                ClassName = NewMeasurementClass,
                CanDelete = true
            });
            NewMeasurementClass = string.Empty;
        }

        public ICommand DeleteMeasurementClassCommand
        {
            get { return new RelayCommand<string>(DeleteMeasurementClassExecute, CanDeleteMeasurementClass); }
        }

        public bool CanDeleteMeasurementClass(string value)
        {
            return true;
        }

        public void DeleteMeasurementClassExecute(string value)
        {
            MeasurementClasses.Remove(MeasurementClasses.FirstOrDefault(x => x.ClassName.Equals(value)));
        }

        public ICommand SaveClassEditor
        {
            get { return new RelayCommand(SaveClassEditorExecute, CanSaveClassEditor); }
        }

        bool CanSaveClassEditor()
        {
            return true;
        }

        void SaveClassEditorExecute()
        {
            FormCloseAction();
            return;
        }

    }
}
