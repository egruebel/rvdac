﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using RVDAC.Common;
using RVDAC.Common.Models;

namespace RVDAC.Config.ViewModels
{
    partial class MainWindowViewModel : INotifyPropertyChanged
    {
        private ViewModelBase _selectedSensorView;
        private NodeBase _selectedSensor;
        private SensorConfiguration _config;
        private string _windowTitle = AppDomain.CurrentDomain.FriendlyName;

        public MainWindowViewModel()
        {
            SelectedSensorView = new ViewModelBase();
            Application.Current.MainWindow.Closing += MainWindow_Closing;
            PropertyChanged += MainWindowViewModel_PropertyChanged;
        }

        private void Config_ModelDirtyChanged(object sender, bool e)
        {
        }

        private void MainWindowViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Config")
            {
                if (Config == null)
                {
                    SelectedSensor = null;
                    SelectedSensorView = null;
                    return;
                }


                WindowTitle = AppDomain.CurrentDomain.FriendlyName + "  " + Config.FilePath;

                if (Config.AllParentSensors.Any())
                {
                    SelectSensorExecute(Config.AllParentSensors[0].Guid);
                }
                if (!string.IsNullOrEmpty(Config.FilePath))
                    Properties.Settings.Default.LastFilePath = Config.FilePath;
            }
        }

        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (Config != null && Config.IsDirty)
            {
                var msg = "There are unsaved changes in the current configuration. Do you still want to exit?";
                var title = "Unsaved Changes";
                var result = MessageBox.Show(msg, title, MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result == MessageBoxResult.No)
                    e.Cancel = true;
            }
        }

        public SensorConfiguration Config
        {
            get { return _config; }
            private set
            {
                if (_config != null)
                {
                    _config.ModelDirtyChanged -= Config_ModelDirtyChanged;
                    _config.PropertyChanged -= _config_PropertyChanged;
                }
                _config = value;
                OnPropertyChanged(nameof(Config));
                if (value != null)
                {
                    _config.ModelDirtyChanged += Config_ModelDirtyChanged;
                    _config.PropertyChanged += _config_PropertyChanged;
                }
            }
        }

        public List<ChildNode> GetAllChildSensors
        {
            get
            {
                if (Config != null)
                    return Config.AllChildSensors();
                return new List<ChildNode>();
            }
        }

        private void _config_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "FilePath")
                WindowTitle = AppDomain.CurrentDomain.FriendlyName + "  " + Config.FilePath;
        }

        public string WindowTitle
        {
            get { return _windowTitle; }
            set
            {
                _windowTitle = value;
                OnPropertyChanged(nameof(WindowTitle));
            }
        }

        public ViewModelBase SelectedSensorView
        {
            get { return _selectedSensorView; }
            private set
            {
                _selectedSensorView = value;
                OnPropertyChanged(nameof(SelectedSensorView));
            }
        }

        public NodeBase SelectedSensor
        {
            get { return _selectedSensor; }
            set
            {
                if (value == null)
                    return;
                _selectedSensor = value;
                SelectSensorExecute(_selectedSensor.Guid);
                OnPropertyChanged(nameof(SelectedSensor));
            }
        }

        private void ShowCriticalError(string message, string title)
        {
            MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        string GetNewOpenFilePath()
        {
            // Create OpenFileDialog 
            var dlg = new Microsoft.Win32.OpenFileDialog()
            {
                DefaultExt = ".xml",
                Filter = "XML Files (*.xml)|*.xml"
            };

            // Display OpenFileDialog by calling ShowDialog method 
            var result = dlg.ShowDialog();

            if (result == true)
            {
                //return the file name
                return dlg.FileName;
            }
            return null;
        }

        string GetNewSaveFilePath()
        {
            // Create OpenFileDialog 
            var dlg = new Microsoft.Win32.SaveFileDialog()
            {
                DefaultExt = ".xml",
                Filter = "XML Files (*.xml)|*.xml"
            };

            // Display OpenFileDialog by calling ShowDialog method 
            var result = dlg.ShowDialog();

            if (result == true)
            {
                //return the file name
                return dlg.FileName;
            }
            return null;
        }

        void LoadNewConfigFile(Func<bool> openMethod, ConfigImporter builder)
        {
            try
            {
                if (openMethod.Invoke())
                {
                    Config = builder.SensorConfig;
                    Config.ValidateAllAsync();
                }
            }
            catch (Exception ex)
            {
                var msg = "Last Sensor Processed:  ";
                msg += builder.LastSensorProcessed ?? "None.";
                msg += "\r\n\r\nLast Field Processed:  ";
                msg += builder.LastFieldProcessed ?? "None.";
                msg += "\r\n\r\nError: ";
                msg += ex.Message;
                ShowCriticalError(msg, "File Open/Import Error");
                Config = null;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}