﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Xml.Linq;
using RVDAC.Common;
using RVDAC.Common.Models;
using RVDAC.Config.Views;

namespace RVDAC.Config.ViewModels
{
    partial class MainWindowViewModel
    {
        public ICommand AddChildCommand
        {
            get { return new RelayCommand<Guid>(AddChildExecute, CanAddChild); }
        }

        public bool CanAddChild(Guid guid)
        {
            var clickedSensor = Config.AllParentSensors.FirstOrDefault(x => x.Guid == guid);
            if (clickedSensor == null)
                return false;
            return !clickedSensor.IsVirtual;
        }

        public void AddChildExecute(Guid guid)
        {
            var clickedSensor = Config.AllParentSensors.FirstOrDefault(x => x.Guid == guid);
            if (clickedSensor == null)
                return;
            clickedSensor.ChildNodes.Add(new ChildNode()
            {
                Guid = Guid.NewGuid(),
                DeviceType =
                    clickedSensor.DeviceType == NodeBase.DeviceTypeEnum.Nmea
                        ? NodeBase.DeviceTypeEnum.NmeaChild
                        : NodeBase.DeviceTypeEnum.SerialChild,
                Name = clickedSensor.Name + "-NewChild",
                Parent = clickedSensor
            });
            clickedSensor.ChildNodes.Last().ValidateAsync();
        }

        public ICommand AddSensorCommand
        {
            get { return new RelayCommand<NodeBase.DeviceTypeEnum>(AddSensorExecute, CanAddSensor); }
        }

        public bool CanAddSensor(NodeBase.DeviceTypeEnum device)
        {
            return Config != null;
        }

        public void AddSensorExecute(NodeBase.DeviceTypeEnum device)
        {
            switch (device)
            {
                case NodeBase.DeviceTypeEnum.Nmea:
                    Config.AllParentSensors.Add(NodeBuilder.NewNmeaParentSensor());
                    break;
                case NodeBase.DeviceTypeEnum.Serial:
                    Config.AllParentSensors.Add(NodeBuilder.NewSerialParentSensor());
                    break;
                case NodeBase.DeviceTypeEnum.VirtualAverage:
                    Config.AllParentSensors.Add(NodeBuilder.NewVirtualAverageSensor());
                    break;
                case NodeBase.DeviceTypeEnum.VirtualTrueWind:
                    Config.AllParentSensors.Add(NodeBuilder.NewVirtualTrueWindSensor());
                    break;
                default:
                    throw new InvalidOperationException("Unexpected device type.");
            }
            SelectedSensor = Config.AllParentSensors.Last();
            Config.ValidateAllAsync();
        }

        public ICommand CloseCommand
        {
            get { return new RelayCommand(CloseExecute, CanClose); }
        }

        bool CanClose()
        {
            return true;
        }

        void CloseExecute()
        {
            Application.Current.MainWindow.Close();
        }

        public ICommand DeleteSensorCommand
        {
            get { return new RelayCommand<Guid>(DeleteSensorExecute, CanDeleteSensor); }
        }

        public bool CanDeleteSensor(Guid guid)
        {
            var result = Config.CheckCrossReference(guid);
            if (result.IsReferenced)
                return false;
            return true;
        }

        public void DeleteSensorExecute(Guid guid)
        {
            var sensor = Config.GetSensorByGuid(guid);
            if (sensor is ParentNodeBase)
            {
                Config.AllParentSensors.Remove((ParentNodeBase) sensor);
                return;
            }
            ((ChildNode) sensor).Parent.ChildNodes.Remove((ChildNode) sensor);
        }

        public ICommand ImportFromScs
        {
            get { return new RelayCommand(ImportFromScsExecute, CanImportFromScs); }
        }

        bool CanImportFromScs()
        {
            return true;
        }

        void ImportFromScsExecute()
        {
            var fileName = GetNewOpenFilePath();
            if (fileName == null)
                return;
            var config = new SensorConfiguration();
            try
            {
                var xDoc = XDocument.Load(fileName);
                var builder = new ConfigImporter(xDoc, config);
                LoadNewConfigFile(builder.TryBuildFromLegacyScs, builder);
            }
            catch
            {
                ShowCriticalError("The file " + fileName + " is not a valid SCS configuration file.", "Invalid file");
            }
            
        }

        public ICommand LaunchIoPortEditor
        {
            get { return new RelayCommand(LaunchIoPortEditorExecute, CanOpenIoPortEditor); }
        }

        bool CanOpenIoPortEditor()
        {
            return true;
        }

        void LaunchIoPortEditorExecute()
        {
            var ioWin = new IoPortEditor()
            {
                DataContext = new IoPortEditorViewModel(Config)
            };
            ioWin.Show();
        }
        
        public ICommand LaunchClassEditor
        {
            get { return new RelayCommand(LaunchClassEditorExecute, CanOpenClassEditor); }
        }

        bool CanOpenClassEditor()
        {
            return Config != null;
        }

        void LaunchClassEditorExecute()
        {
            
            var classWinVm = new ClassEditorViewModel();
            var classList = new ObservableCollection<ClassEditorViewModel.MeasurementClassModel>();
            foreach (var cls in Config.MeasurementClasses)
            {
                var model = new ClassEditorViewModel.MeasurementClassModel()
                {
                    ClassName = cls,
                    CanDelete = true
                };
                if (Config.AllChildSensors().Where(x => x.Measurement != null).Any(x => x.Measurement.Equals(cls)))
                    model.CanDelete = false;
                classList.Add(model);
            }
            classWinVm.MeasurementClasses = classList;
            var classWin = new ClassEditor()
            {
                DataContext = classWinVm
            };
            classWinVm.FormCloseAction = classWin.Close;
            classWin.ShowDialog();
            //now update the config
            Config.MeasurementClasses.Clear();
            foreach (var cls in classWinVm.MeasurementClasses)
            {
                Config.MeasurementClasses.Add(cls.ClassName);
            }
            Config.IsDirty = true;
        }

        public ICommand OpenExtendedScsFile
        {
            get { return new RelayCommand(OpenExtendedScsFileExecute, CanOpenExtendedScsFile); }
        }

        bool CanOpenExtendedScsFile()
        {
            return true;
        }

        void OpenExtendedScsFileExecute()
        {
            var fileName = GetNewOpenFilePath();
            if (fileName == null)
                return;
            var config = new SensorConfiguration();
            try
            {
                var xDoc = XDocument.Load(fileName);
                var builder = new ConfigImporter(xDoc, config);
                LoadNewConfigFile(builder.TryBuildFromExtendedScs, builder);
                config.FilePath = fileName;
            }
            catch
            {
                ShowCriticalError("The file " + fileName + " is not a valid SCS configuration file.", "Invalid file");
            }
            
        }

        public ICommand SaveConfiguration
        {
            get { return new RelayCommand(SaveConfigExecute, CanSaveConfig); }
        }

        bool CanSaveConfig()
        {
            if (Config != null && Config.IsDirty && !Config.ConfigHasErrors)
            {
                return true;
            }

            return false;
        }

        public void SaveConfigExecute()
        {
            Config.ValidateAll();
            if (Config.ConfigHasErrors)
                return;
            //This is a two step process
            var errorMsg = "Error saving backup file ";
            try
            {
                if (string.IsNullOrEmpty(Config.FilePath))
                {
                    var path = GetNewSaveFilePath();
                    if (path == null)
                        return;
                    Config.FilePath = path;
                }
                else
                {
                    //save a backup file of the original
                    var fi = new FileInfo(Config.FilePath);
                    var dt = DateTime.Now.ToString("yyyyMMdd-HHmmss");
                    var fn = string.Format("{0}.{1}.BAK", fi.FullName, dt);
                    errorMsg += fn;
                    fi.CopyTo(fn);
                }

                errorMsg = "Error saving configuration file " + Config.FilePath;
                ConfigExporter.OverwriteToFile(Config, Config.FilePath);
                Config.ClearDirtyStatus();
                ReportGenerator.GenerateReport(Config, Config.FilePath);
            }
            catch (Exception ex)
            {
                ShowCriticalError(errorMsg + "\r\n" + ex.Message, "Configuration Save Error");
            }
        }

        public ICommand SelectorMoveItem
        {
            get { return new RelayCommand<string>(SelectorMoveItemExecute, CanMoveItem); }
        }

        public bool CanMoveItem(string direction)
        {
            return Config != null && Config.AllParentSensors.Count > 1;
        }

        public void SelectorMoveItemExecute(string direction)
        {
            var selectedItem = Config.AllParentSensors.FirstOrDefault(x => x.Guid == SelectedSensor.Guid);
            if (selectedItem == null)
                return;
            var indexOfSelected = Config.AllParentSensors.IndexOf(selectedItem);
            var newIndex = (direction == "up") ? (indexOfSelected - 1) : (indexOfSelected + 1);

            //dont exceed bounds
            if (newIndex < 0 || newIndex == Config.AllParentSensors.Count)
                return;

            Config.AllParentSensors.Move(indexOfSelected, newIndex);
        }

        public ICommand SelectSensor
        {
            get { return new RelayCommand<Guid>(SelectSensorExecute, CanSelectSensor); }
        }

        bool CanSelectSensor(Guid id)
        {
            return true;
        }

        void SelectSensorExecute(Guid id)
        {
            var clickedParent = Config.AllParentSensors.FirstOrDefault(x => x.Guid == id);
            if (clickedParent == null)
            {
                var clickedChild =
                    Config.AllParentSensors.SelectMany(x => x.ChildNodes).FirstOrDefault(x => x.Guid == id);
                SelectedSensorView = new ChildViewModel(clickedChild);
                return;
            }
            if (clickedParent is PhysicalParentNode)
            {
                SelectedSensorView = new PhysicalParentViewModel(clickedParent as PhysicalParentNode);
                return;
            }
            SelectedSensorView = new VirtualParentViewModel(clickedParent as VirtualParentBase);
        }

        public ICommand StartNewConfig
        {
            get { return new RelayCommand(StartNewConfigExecute, CanStartNewConfig); }
        }

        public bool CanStartNewConfig()
        {
            return true;
        }

        public void StartNewConfigExecute()
        {
            var cfg = new SensorConfiguration();
            //add a sample io port
            cfg.IoPorts.Add(NodeBuilder.NewIoPort());
            cfg.AllParentSensors.Add(NodeBuilder.NewNmeaParentSensor());
            Config = cfg;
            Config.ValidateAllAsync();
        }
    }
}