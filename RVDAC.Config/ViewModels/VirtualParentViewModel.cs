﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using RVDAC.Common.Models;

namespace RVDAC.Config.ViewModels
{
    public class VirtualParentViewModel : ViewModelBase
    {
        private VirtualParentBase _sensor;

        public VirtualParentViewModel()
        {
            Sensor = new VirtualTrueWind();
        }

        public VirtualParentViewModel(VirtualParentBase sensor)
        {
            Sensor = sensor;
        }

        public List<VirtualAverage.Average> GetAverageTypes
        {
            get { return Enum.GetValues(typeof (VirtualAverage.Average)).OfType<VirtualAverage.Average>().ToList(); }
        }

        public List<VirtualAverage.AverageDomain> GetAverageModes
        {
            get
            {
                return
                    Enum.GetValues(typeof (VirtualAverage.AverageDomain))
                        .OfType<VirtualAverage.AverageDomain>()
                        .ToList();
            }
        }

        public VirtualParentBase Sensor
        {
            get { return _sensor; }
            set
            {
                _sensor = value;
                OnPropertyChanged(nameof(Sensor));
            }
        }
    }
}