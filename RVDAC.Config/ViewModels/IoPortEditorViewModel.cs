﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.Ports;
using System.Linq;
using System.Windows.Input;
using RVDAC.Common;
using RVDAC.Common.Models;

namespace RVDAC.Config.ViewModels
{
    public class IoPortEditorViewModel : INotifyPropertyChanged
    {
        private IoPort _selectedPort;

        public IoPortEditorViewModel()
        {
            Config = new SensorConfiguration();
            PropertyChanged += IoPortEditorViewModel_SelectedPortChanged;
        }

        public IoPortEditorViewModel(SensorConfiguration config)
        {
            Config = config;
            PropertyChanged += IoPortEditorViewModel_SelectedPortChanged;
        }

        void IoPortEditorViewModel_SelectedPortChanged(object sender, PropertyChangedEventArgs e)
        {
            if (string.Equals(e.PropertyName, "SelectedPort"))
            {
                //We intercept this event so the user is only editing 
                //a copy of the selected port and can submit changes on click
                OnPropertyChanged(nameof(SelectedPortIsSerial));
                OnPropertyChanged(nameof(SelectedPortIsTcpUdp));
            }
        }

        public SensorConfiguration Config { get; private set; }

        public IoPort SelectedPort
        {
            get { return _selectedPort; }
            set
            {
                //if it's a real IoPort then make a copy, if not then set to null
                _selectedPort = value == null ? null : CopyPort(value);
                OnPropertyChanged(nameof(SelectedPort));
            }
        }

        private IoPort CopyPort(IoPort from)
        {
            var p = new IoPort()
            {
                Guid = from.Guid,
                PortType = from.PortType,
                ComPort = from.ComPort,
                BaudRate = from.BaudRate,
                DataBits = from.DataBits,
                Parity = from.Parity,
                StopBits = from.StopBits,
                IpAddress = from.IpAddress,
                PortNo = from.PortNo
            };
            return p;
        }

        public bool SelectedPortIsSerial
        {
            get
            {
                if (_selectedPort != null)
                    return _selectedPort.PortType == IoPort.DacPortType.Serial;
                return false;
            }
        }

        public bool SelectedPortIsTcpUdp
        {
            get
            {
                if (_selectedPort != null)
                    return _selectedPort.PortType == IoPort.DacPortType.Tcp ||
                           _selectedPort.PortType == IoPort.DacPortType.Udp;
                return false;
            }
        }

        public List<int> GetBaudRates
        {
            get { return new List<int> {1200, 2400, 4800, 9600, 14400, 19200, 28800, 38400, 56000, 57600, 115200}; }
        }

        public List<int> GetDataBits
        {
            get { return new List<int> {5, 6, 7, 8, 9}; }
        }

        public List<Parity> GetParitys
        {
            get { return Enum.GetValues(typeof (Parity)).OfType<Parity>().ToList(); }
        }

        public List<StopBits> GetStopBits
        {
            get { return Enum.GetValues(typeof (StopBits)).OfType<StopBits>().ToList(); }
        }

        public ICommand SaveIoPort
        {
            get { return new RelayCommand(SaveIoPortExecute, CanSaveIoPort); }
        }

        bool CanSaveIoPort()
        {
            return SelectedPort != null && !SelectedPort.HasErrors;
        }

        void SaveIoPortExecute()
        {
            if (SelectedPort == null)
                return;
            //match object by guid;
            var match = Config.IoPorts.FirstOrDefault(x => x.Guid == SelectedPort.Guid);
            if (match == null)
            {
                //new port
                //make sure it doesn't already exist
                if (Config.IoPorts.Contains(SelectedPort))
                {
                    //Port already exists
                    SelectedPort.ValidationSummary.Add(string.Format("The port {0} already exists.", SelectedPort.Name));
                    return;
                }
                Config.IoPorts.Add(SelectedPort);
            }
            else
            {
                //existing port
                //new port, make sure the user isn't trying to redefine it as a port that already exists
                foreach (var conflict in Config.IoPorts.Where(x => x.Equals(SelectedPort)))
                {
                    if (!conflict.Guid.Equals(SelectedPort.Guid))
                    {
                        //Port already exists
                        SelectedPort.ValidationSummary.Add(string.Format("The port {0} already exists.",
                            SelectedPort.Name));
                        return;
                    }
                }
                UpdatePort(match, SelectedPort);
            }
            SelectedPort = null;
        }

        private void UpdatePort(IoPort oldSettings, IoPort newSettings)
        {
            oldSettings.ComPort = newSettings.ComPort;
            oldSettings.BaudRate = newSettings.BaudRate;
            oldSettings.DataBits = newSettings.DataBits;
            oldSettings.Parity = newSettings.Parity;
            oldSettings.StopBits = newSettings.StopBits;
            oldSettings.IpAddress = newSettings.IpAddress;
            oldSettings.PortNo = newSettings.PortNo;
        }

        public ICommand AddNewPort
        {
            get { return new RelayCommand<IoPort.DacPortType>(AddNewPortExecute, CanAddNewPort); }
        }

        bool CanAddNewPort(IoPort.DacPortType portType)
        {
            return Config != null;
        }

        void AddNewPortExecute(IoPort.DacPortType portType)
        {
            var np = new IoPort()
            {
                PortType = portType,
            };
            SelectedPort = np;
            SelectedPort.ValidateAsync();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}