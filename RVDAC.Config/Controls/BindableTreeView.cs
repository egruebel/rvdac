﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace RVDAC.Config.Controls
{
    internal class BindableTreeView : TreeView
    {
        public static DependencyProperty SelectedItemObject = DependencyProperty.Register("SelectedObject",
            typeof (object), typeof (BindableTreeView), new FrameworkPropertyMetadata(new object()));

        public BindableTreeView()
        {
            SelectedItemChanged += SelectedItem_Changed;
        }

        void SelectedItem_Changed(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (SelectedItem != null)
            {
                var tvi = (TreeViewItem)ItemContainerGenerator.ContainerFromItem(this.SelectedItem);
                
                //tvi.IsSelected = true;
                //tvi.IsExpanded = true;
                tvi.Focus();

                SetValue(SelectedItemObject, SelectedItem);
            }
        }

        public object SelectedObject
        {
            get { return (object) GetValue(SelectedItemObject); }
            set { SetValue(SelectedItemObject, value); }
        }
    }
}