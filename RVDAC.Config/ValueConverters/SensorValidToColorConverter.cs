﻿using System;
using System.Windows.Data;
using System.Windows.Media;

namespace RVDAC.Config.ValueConverters
{
    public class SensorValidToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var hasErrors = (bool) value;
            return hasErrors ? Brushes.Red : Brushes.Black;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}