﻿using System;
using RVDAC.Common.Models;

namespace RVDAC.RemoteMessenger.Models
{
    public class DataUseTracker: ValidatableModel
    {
        private int _hour;
        private int _day;

        private double _dataThisHour, _dataLastHour, _dataToday, _dataYesterday;

        public event EventHandler<string> DataUseRollover;

        public DataUseTracker()
        {
            _hour = DateTime.Now.Hour;
            _day = DateTime.Now.Day;
        }

        public void AddDataUsed(int bytes)
        {
            if (DateTime.Now.Hour != _hour)
            {
                DataUseRollover?.Invoke(this, "Data use hourly report: " + DataThisHour.ToString("0.0") + "kb");
                DataLastHour = DataThisHour;
                _hour = DateTime.Now.Hour;
                DataThisHour = 0;
            }
            if (DateTime.Now.Day != _day)
            {
                DataUseRollover?.Invoke(this, "Data use daily report: " + DataToday.ToString("0.0") + "kb");
                DataYesterday = DataToday;
                _day = DateTime.Now.Day;
                DataToday = 0;
            }
            if (bytes == 0)
                return;
            var db = (double)bytes / 1024;
            DataThisHour += db;
            DataToday += db;
        }

        public double DataLastHour {
            get { return _dataLastHour; }
            private set { SetProperty(ref _dataLastHour, value); }
        }
        public double DataThisHour
        {
            get { return _dataThisHour; }
            private set { SetProperty(ref _dataThisHour, value); }
        }
        public double DataYesterday
        {
            get { return _dataYesterday; }
            private set { SetProperty(ref _dataYesterday, value); }
        }
        public double DataToday
        {
            get { return _dataToday; }
            private set { SetProperty(ref _dataToday, value); }
        }
    }
}
