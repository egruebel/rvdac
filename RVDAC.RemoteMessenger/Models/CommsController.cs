﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using RVDAC.Common;
using RVDAC.Common.Messenger;
using RVDAC.Common.Models;
using RVDAC.Common.ScsClient;
using Timer = System.Timers.Timer;

namespace RVDAC.RemoteMessenger.Models
{
    public class CommsController: ValidatableModel
    {
        private bool _ioInProgress, _connected;
        private long _latency;
        private IPEndPoint _ipEndpoint;
        private readonly SqliteRepository _connectionLogRepository;
        private readonly SensorConfiguration _sensorConfig;
        private readonly object _portLock = new object();
        private readonly Timer _retryDelayTimer;
        private bool _holdForConnectionDelay;
        public event EventHandler IoPortFreed;
        private const int DefaultTimeout = 4600;

        public CommsController(IPEndPoint endpoint, SqliteRepository connectionLogRepository, ScsClient scsClient)
        {
            RemoteEndpoint = endpoint;
            _connectionLogRepository = connectionLogRepository;
            _sensorConfig = scsClient.SensorConfig;
            _retryDelayTimer = new Timer(RetryConnectionDelay * 1000);
            _retryDelayTimer.Elapsed += (sender, args) => { _holdForConnectionDelay = false; };
            DataUseTracker.DataUseRollover += DataUseTracker_DataUseRollover;
        }

        private void DataUseTracker_DataUseRollover(object sender, string e)
        {
            //Adds a periodic log about how much data has been used
            _connectionLogRepository.InsertTcpConnection(DateTime.Now, e, 0);
        }

        public enum ResponseType
        {
            Success, Fail, IoProblem, Delay
        }

        private Guid RemoteReceiverSensorConfigGuid { get; set; }

        public IPEndPoint RemoteEndpoint
        {
            get
            {
                return _ipEndpoint;
            }
            set
            {
                _ipEndpoint = value;
                try
                {
                    if (TcpClient == null)
                        return;
                    if (TcpClient.Connected)
                        TcpClient.GetStream().Close();
                    TcpClient.Close();
                }
                finally
                {
                    TcpClient = null;
                }
            }
        }

        public DataUseTracker DataUseTracker { get; } = new DataUseTracker();

        private CustomTcpClient TcpClient { get; set; }

        public int RetryConnectionDelay { get; set; } = 6;

        public bool IsConnected
        {
            get { return _connected;}
            set { SetProperty(ref _connected, value); }
        }

        public bool IoInProgress
        {
            get { return _ioInProgress; }
            set { SetProperty(ref _ioInProgress, value); }
        }

        public long Latency
        {
            get { return _latency;}
            set { SetProperty(ref _latency, value); }
        }

        private void SetRetryDelay()
        {
            _retryDelayTimer.Interval = Convert.ToDouble(RetryConnectionDelay * 1000);
            _holdForConnectionDelay = true;
            _retryDelayTimer.Start();
        }

        private void SetupNewTcpClient()
        {
            TcpClient = new CustomTcpClient(DefaultTimeout);

            //remove events
            TcpClient.DataCounterChanged -= TcpClient_DataCounterChanged;
            TcpClient.LatencyMeasured -= TcpClient_LatencyMeasured;
            TcpClient.ConnectionChanged -= TcpClient_ConnectionChanged;

            //add events
            TcpClient.DataCounterChanged += TcpClient_DataCounterChanged;
            TcpClient.LatencyMeasured += TcpClient_LatencyMeasured;
            TcpClient.ConnectionChanged += TcpClient_ConnectionChanged;
        }

        private void TcpClient_ConnectionChanged(object sender, bool e)
        {
            IsConnected = e;
        }

        private void TcpClient_LatencyMeasured(object sender, long e)
        {
            Latency = e;
        }

        private void TcpClient_DataCounterChanged(object sender, int e)
        {
            DataUseTracker.AddDataUsed(e);
        }

        //This sould be the only public method
        public ResponseType TrySendMessage(byte[] serializedAndCompressedMessage, bool isRealTime)
        {
            if (_holdForConnectionDelay)
            {
                return ResponseType.Delay;
            }

            if (!_sensorConfig.IsInitialized)
            {
                //Don't send anything until we're sure that each client has the correct configuration
                return ResponseType.Delay;
            }

            //Now send the message
            ResponseType transmitResponse;

            lock (_portLock)
            {
                IoInProgress = true;
                //Step 1: Connect to client or make sure that we're still connected
                if (!TryConnect())
                {
                    IoInProgress = false;
                    return ResponseType.IoProblem;
                }

                //Step 2: Make sure that the client has the same sensorconfig as we do
                if (!TrySendSensorConfig())
                {
                    IoInProgress = false;
                    return ResponseType.IoProblem;
                }

                transmitResponse = TransmitMessage(serializedAndCompressedMessage, isRealTime);

                if (isRealTime)
                    Thread.Sleep(90);

                IoInProgress = false;
            }

            IoPortFreed?.Invoke(this, new EventArgs());
            return transmitResponse;
        }

        

        private bool TrySendSensorConfig()
        {
            if (_sensorConfig.Guid.Equals(RemoteReceiverSensorConfigGuid))
                return true;

            //Create the message
            var configMsg = new MessengerMessage()
            {
                DateTime = DateTime.Now,
                InstanceName = "Config",
                IsRealTime = false,
                DataPoints = new List<MessengerDataPoint>(),
                ScsConfiguration = ConfigExporter.ToString(_sensorConfig)
            };

            //Serialize, compress, and wrap the message
            var compressedBytes = MessengerSerializer.Serialize(configMsg);

            var response = TransmitMessage(compressedBytes, configMsg.IsRealTime);
            if (response == ResponseType.Success)
            {
                RemoteReceiverSensorConfigGuid = _sensorConfig.Guid;
                return true;
            }
            return false;
        }

        
        //This is the only method that uses the TCP connection
        private ResponseType TransmitMessage(byte[] serializedAndCompressedMessage, bool isRealTime)
        {
            try
            {

                var responseFromReceiver = TcpClient.SendMessageAndGetResponse(serializedAndCompressedMessage, isRealTime);
                //If you made it here the comms went okay

                if (isRealTime)
                    return ResponseType.Success;

                if (responseFromReceiver == 10)
                {
                    return ResponseType.Success;
                }
                if (responseFromReceiver == 20)
                {
                    return ResponseType.Fail;
                }
            }
            catch (MessageNotSentException ex)
            {
                _connectionLogRepository.InsertError(DateTime.Now, ex.Message);
                SetRetryDelay();
            }
            catch (NoReplyReceivedException ex)
            {
                _connectionLogRepository.InsertError(DateTime.Now, ex.Message);
            }
            catch (UnexpectedReplyReceivedException ex)
            {
                _connectionLogRepository.InsertError(DateTime.Now, ex.Message + " --Data Dump-- " + ex.ReplyReceived);
            }
            catch (Exception ex)
            {
                _connectionLogRepository.InsertError(DateTime.Now, ex.Message);
            }
            return ResponseType.IoProblem;
        }
        

        private bool TryConnect()
        {
            try
            {
                //Should only happen at startup
                if (TcpClient == null)
                    SetupNewTcpClient();

                //socket still registers as connected and we've verified that it's a proper RVDAC receiver
                if (TcpClient.Client != null && TcpClient.Client.Connected && TcpClient.HasCompletedConnectionHandshake)
                    return true;

                //Set up the TCP client with the settings we want
                SetupNewTcpClient();

                //Connect to an RVDAC receiver
                var connectionMessageReceived = TcpClient.ConnectToReceiverAndReturnResponse(RemoteEndpoint, 80);
                _connectionLogRepository.InsertTcpConnection(DateTime.Now, RemoteEndpoint.ToString(), 0);
                //Save the SensorConfig GUID that the client is using
                RemoteReceiverSensorConfigGuid = ParseGuidFromConnectionMessage(connectionMessageReceived);
                return true;
            }
            catch (CouldNotConnectException ex)
            {
                SetRetryDelay();
                _connectionLogRepository.InsertError(DateTime.Now,
                    $"Error at {nameof(TryConnect)}. {ex.Message} {ex.GetType()}.");
            }
            catch (UnexpectedReplyReceivedException ex)
            {
                SetRetryDelay();
                _connectionLogRepository.InsertError(DateTime.Now,
                    $"Error at {nameof(TryConnect)}. {ex.Message + " --Data Dump-- " + ex.ReplyReceived}");
            }
            catch (NoReplyReceivedException ex)
            {
                SetRetryDelay();
                _connectionLogRepository.InsertError(DateTime.Now,
                    $"Error at {nameof(TryConnect)}. {ex.Message} {ex.GetType()}.");
            }
            return false;
        }

        private static Guid ParseGuidFromConnectionMessage(string connectionMessage)
        {
            try
            {
                string output = connectionMessage.Split('*', '*')[1];
                return Guid.Parse(output);
            }
            catch
            {
                return Guid.Empty;
            }
        }
    }
}
