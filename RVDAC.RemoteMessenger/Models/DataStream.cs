﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using System.Xml.Serialization;
using ProtoBuf;
using RVDAC.Common;
using RVDAC.Common.Messenger;
using RVDAC.Common.Models;
using RVDAC.Common.ScsClient;

namespace RVDAC.RemoteMessenger.Models
{
    [XmlRoot]
    public class DataStream: ValidatableModel
    {
        private string _streamName;
        private int _payloadSize;
        private bool _realTimeStream;
        private int _pollingInterval;
        private SqliteRepository _logfileRepository;
        private DateTime _timeToNextSend, _timeLastSend;
        
        private readonly Timer _countdownTimer = new Timer(600);
 
        public event EventHandler<MessengerMessage> NewMessageReady;
        public event EventHandler<DateTime> ReadyForDataPoint;

        public DataStream()
        {
            _countdownTimer.Elapsed += _countdownTimer_Elapsed;
        }

        public void Start()
        {
            ResetCountdownTimerAsync();
        }

        public void Stop()
        {
            _countdownTimer.Stop();
        }

        public TimeSpan GetInterval()
        {
            var nextDate = new DateTime();
            var nextSpan = new TimeSpan();

            if (PollingInterval > 60)
            {
                //minutes
                nextSpan = SyncronousTimer.GetNextIntervalSpan(PollingInterval/60, SyncronousTimer.TimeSpanInterval.Minutes,
                    out nextDate);
            }
            else
            {
                //seconds
                nextSpan = SyncronousTimer.GetNextIntervalSpan(PollingInterval, SyncronousTimer.TimeSpanInterval.Seconds,
                    out nextDate);
            }
            
            TimeToNextSend = nextDate;
            return nextSpan;
        }

        private void _countdownTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            //Let the MessengerClient know that you need sensor values from the SCS client
            ReadyForDataPoint?.Invoke(this, TimeToNextSend);
            ResetCountdownTimerAsync();
        }

        private async void ResetCountdownTimerAsync()
        {
            await Task.Run(() =>
            {
                var interval = GetInterval().TotalMilliseconds;
                while (interval <= 0)
                {
                    interval = GetInterval().TotalMilliseconds;
                }
                _countdownTimer.Interval = interval;
            });
            _countdownTimer.Start();
        }

        public void GenerateNewMessage(List<MessengerDataPoint> values, DateTime receivedTime)
        {
            Task.Run(() =>
            {
                var pollingIntervalMs = PollingInterval * 1000;

                //process took too long. Throw away the point unless it's a buffered stream
                if (IsRealTimeStream && (DateTime.Now - receivedTime).TotalMilliseconds > (pollingIntervalMs*.1))
                    return;

                //If there's no sensors then there's no reason to transmit
                if (!SensorList.Any())
                    return;

                //create new message
                var msg = new MessengerMessage()
                {
                    DateTime = receivedTime,
                    InstanceName = StreamName,
                    IsRealTime = IsRealTimeStream,
                    DataPoints = values ?? new List<MessengerDataPoint>()
                };

                if (!IsRealTimeStream)
                {
                    //In the off chance that there's a collision we'll try 4 times
                    int tries = 1;
                    while (!LogfileRepository.TryInsertDataPoint(ToProtobuf(msg), DateTime.Now))
                    {
                        tries++;
                        if (tries == 4)
                            break;
                    }
                }
                //Trigger a new send operation
                NewMessageReady?.Invoke(this, msg);
            });
        }

        public void SendRealtimeMessage(MessengerMessage msg, CommsController commsController)
        {
            //serialize and compress the message
            var bytes = MessengerSerializer.Serialize(msg);
            PayloadSize = bytes.Length;

            //try to send object
            var result = commsController.TrySendMessage(bytes, true);

            switch (result)
            {
                case CommsController.ResponseType.Success:
                    LogfileRepository.DeleteFirstDataPoint();
                    LogfileRepository.InsertSent(DateTime.Now, DateTime.Now, "Message sent", bytes.Length);
                    TimeLastSend = DateTime.Now;
                    break;
                case CommsController.ResponseType.Fail:
                    LogfileRepository.InsertError(DateTime.Now, "Client failed to process data point");
                    break;
                case CommsController.ResponseType.IoProblem:
                case CommsController.ResponseType.Delay:
                    break;
            }
        }

        public void SendMessageFromQueue(CommsController commsController)
        {
            if (EmptyQueueInProgress)
                return;

            //emptyqueue prevents other threads from sending datapoints. This is intended to prevent sending duplicates
            //before a previous transaction has finished
            EmptyQueueInProgress = true;

            //Get the protobuf serialized FIFO datapoint from the database
            var serializedDataPoint = LogfileRepository.GetFirstDataPoint();
            MessengerMessage messengerMessage;
            byte[] bytes;

            //if serializedDataPoint is null that means that the datapoint queue is empty and there's nothing to send
            if (serializedDataPoint == null)
            {
                EmptyQueueInProgress = false;
                return;
            }
                
            //Deserialize the protobuf from the database, then re-serialize it and compress it
            try
            {
                using (var ms = new MemoryStream(serializedDataPoint.Object))
                {
                    messengerMessage = Serializer.Deserialize<MessengerMessage>(ms);
                }
                bytes = MessengerSerializer.Serialize(messengerMessage);
                PayloadSize = bytes.Length;
            }
            catch
            {
                //The message was unable to be unpacked or repacked. Delete it. 
                LogfileRepository.DeleteFirstDataPoint();
                EmptyQueueInProgress = false;
                return;
            }

            //try to send object
            var result = commsController.TrySendMessage(bytes, false);
            //do stuff depending on the result
            switch (result)
            {
                case CommsController.ResponseType.Success:
                    //delete the point
                    LogfileRepository.DeleteFirstDataPoint();
                    LogfileRepository.InsertSent(DateTime.Now, messengerMessage.DateTime, "Message sent.", bytes.Length);
                    TimeLastSend = DateTime.Now;
                    break;
                case CommsController.ResponseType.Fail:
                    //client received but could not process the message (returned a 0)
                    serializedDataPoint.ErrorCount += 1;
                    LogfileRepository.UpdateDatapointErrorCount(serializedDataPoint.Id, serializedDataPoint.ErrorCount);
                    //if this object has failed numerous times, delete it
                    if (serializedDataPoint.ErrorCount > 50)
                        LogfileRepository.DeleteFirstDataPoint();
                    break;
                case CommsController.ResponseType.IoProblem:
                case CommsController.ResponseType.Delay:
                    //client is not available. try again later
                    break;
            }
            EmptyQueueInProgress = false;
        }

        public static byte[] ToProtobuf(MessengerMessage msg)
        {
            byte[] data;
            using (var ms = new MemoryStream())
            {
                Serializer.Serialize(ms, msg);
                data = ms.ToArray();
            }
            return data;
        }

#region properties

        [XmlIgnore]
        public SqliteRepository LogfileRepository
        {
            get {return _logfileRepository;}
            set { SetProperty(ref _logfileRepository, value); }
        }

        [XmlIgnore]
        public DateTime TimeLastSend
        {
            get {return _timeLastSend;}
            set
            {
                SetProperty(ref _timeLastSend, value);
            }
        }

        [XmlIgnore]
        public DateTime TimeToNextSend
        {
            get {return _timeToNextSend;}
            private set
            {
                SetProperty(ref _timeToNextSend, value);
            }
        }

        [XmlIgnore]
        public int PayloadSize
        {
            get { return _payloadSize; }
            private set
            {
                SetProperty(ref _payloadSize, value);
            }
        }

        [Required]
        [MinLength(3)]
        public string StreamName {
            get { return _streamName; }
            set
            {
                SetProperty(ref _streamName, value);
                LogfileRepository = new SqliteRepository(_streamName + "_log");
            }
        }

        public Guid StreamGuid { get; set; } = Guid.NewGuid();

        [XmlIgnore]
        public bool EmptyQueueInProgress { get; set; }

        public int NumberOfSensors
        {
            get { return SensorList.Count; }
        }

        [CustomValidation(typeof(DataStream), "PollingIntervalValidation")]
        public int PollingInterval
        {
            get { return _pollingInterval; }
            set { SetProperty(ref _pollingInterval, value); }
        }

        public bool IsRealTimeStream
        {
            get { return _realTimeStream; }
            set { SetProperty(ref _realTimeStream, value); }
        }

        public ObservableCollection<SensorCollectionItem> SensorList { get; set; } = new ObservableCollection<SensorCollectionItem>();

        #endregion

        //Validate that the polling interval makes sense and is evenly divisible by 1 hour or minute
        public static ValidationResult PollingIntervalValidation(object obj, ValidationContext context)
        {
            var ds = (DataStream)context.ObjectInstance;
            if(ds.PollingInterval == 0)
                return new ValidationResult("Polling Interval must be a minimum of 1 second", new List<string> { "PollingInterval" });

            if (ds.PollingInterval > 60)
            {
                //minutes
                if(ds.PollingInterval % 60 != 0)
                    return new ValidationResult("Polling Interval must be an even minute value", new List<string> { "PollingInterval" });
                var pollIntervalMinutes = ds.PollingInterval/60;
                if(60 % pollIntervalMinutes != 0)
                    return new ValidationResult("Polling Interval must be evenly divisible by one hour", new List<string> { "PollingInterval" });
            }
            else
            {
                if (60 % ds.PollingInterval != 0)
                    return new ValidationResult("Polling Interval must be evenly divisible by one minute", new List<string> { "PollingInterval" });
            }
            return ValidationResult.Success;
        }
    }
}
