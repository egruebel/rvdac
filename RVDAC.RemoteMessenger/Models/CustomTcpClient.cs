﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace RVDAC.RemoteMessenger.Models
{
    public class CustomTcpClient: TcpClient
    {
        public event EventHandler<int> DataCounterChanged;
        public event EventHandler<long> LatencyMeasured;
        public event EventHandler<bool> ConnectionChanged;
        //public event EventHandler<bool> IoChanged;
        private bool _isConnected;
        private Stopwatch _latencyStopwatch;

        public CustomTcpClient(int timeout)
        {
            ReceiveTimeout = timeout + 1000;
            SendTimeout = timeout;
            ConnectTimeout = timeout + 1000;
            Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);
            Client.NoDelay = true;
        }

        public bool IsConnected
        {
            get
            {
                return _isConnected;
            }
            set
            {
                if (value != _isConnected)
                {
                    _isConnected = value;
                    ConnectionChanged?.Invoke(this, _isConnected);
                }
            }
        }

        public bool HasCompletedConnectionHandshake { get; private set; }

        public int ConnectTimeout { get; private set; } = 15000;

        
        public byte SendMessageAndGetResponse(byte[] serializedAndCompressedData, bool ignoreReply)
        {
            _latencyStopwatch = new Stopwatch();
            try
            {
                //First clear any data that might be stuck in the TCP stack
                ClearNetworkStream();
                //Start the timer to measure delay
                _latencyStopwatch.Start();
                //Send the message
                GetStream().Write(serializedAndCompressedData, 0, serializedAndCompressedData.Length);
                //If you made it here, the message was sent
                OnDataCounterIncrease(serializedAndCompressedData.Length + 64 + 64); //Payload + overhead + ack
                //If we expect a reply, then get it, otherwise we're done
                if (ignoreReply)
                {
                    return 0;
                }
                    
                //Receive a reply
            }
            catch (Exception)
            {
                IsConnected = false;
                throw new MessageNotSentException();
            }


            try
            {
                var response = new byte[2048];
                var dataRead = GetStream().Read(response, 0, response.Length);
                //If you made it here then the read was successful
                _latencyStopwatch.Stop();
                OnLatencyMeasured(_latencyStopwatch.ElapsedMilliseconds);
                OnDataCounterIncrease(dataRead + 64 + 64); //payload + overhead + ack
                //We should only expect a reply of one byte
                if (dataRead > 1)
                    throw new UnexpectedReplyReceivedException(response, dataRead);
                if (response[0] != 10 && response[0] != 20)
                    throw new UnexpectedReplyReceivedException(response, dataRead);
                return response[0];
            }
            catch (UnexpectedReplyReceivedException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new NoReplyReceivedException();
            }
        }

        public string ConnectToReceiverAndReturnResponse(IPEndPoint endpoint, int maxResponseLength)
        {
            try
            {
                ConnectWithTimeout(endpoint.Address, endpoint.Port);
                //if you're here it means that you've connected. Congrats. 
                OnDataCounterIncrease(64 + (64 + 64) + 64); //syn + (syn + ack) + syn;
            }
            catch
            {
                throw new CouldNotConnectException();
            }

            try
            {
                var connectMessage = "";
                var bytesRead = 0;
                var buffer = new byte[ReceiveBufferSize];
                while (!connectMessage.EndsWith("RVDAC.RemoteMessenger\r\n"))
                {
                    var stream = this.GetStream();
                    bytesRead += stream.Read(buffer, bytesRead, buffer.Length - bytesRead);
                    connectMessage = Encoding.ASCII.GetString(buffer, 0, bytesRead);
                    if (connectMessage.Length > maxResponseLength)
                    {
                        throw new UnexpectedReplyReceivedException(buffer, bytesRead);
                    }
                }
                //if you're here it means that a connection reply was received
                HasCompletedConnectionHandshake = true;
                IsConnected = true;
                OnDataCounterIncrease(bytesRead + 64 + 64); //Each character is 2 bytes, plus overhead, plus ack
                return connectMessage;
            }
            catch (UnexpectedReplyReceivedException)
            {
                throw;
            }
            catch
            {
                throw new NoReplyReceivedException();
            }
        }

        private void ConnectWithTimeout(IPAddress address, int port)
        {
            _latencyStopwatch = new Stopwatch();
            _latencyStopwatch.Start();
            //address = IPAddress.Parse("131.128.001.244");
            //port = 3000;
            IAsyncResult result = BeginConnect(address, port, ConnectCallback , null);
            var success = result.AsyncWaitHandle.WaitOne(TimeSpan.FromMilliseconds(ConnectTimeout));
            _latencyStopwatch.Stop();
            if (success)
            {
                OnLatencyMeasured(_latencyStopwatch.ElapsedMilliseconds);
                return;
            }

            //if you're here it means that the connection timeout has expired
            throw new TimeoutException($"Failed to connect before the timeout period of {ConnectTimeout}ms.");
        }

        private void ConnectCallback(IAsyncResult result)
        {
        }

        private void ClearNetworkStream()
        {
            var buffer = new byte[4096];
            var ns = GetStream();
            while (ns.DataAvailable)
            {
                ns.Read(buffer, 0, buffer.Length);
            }
        }

        private void OnLatencyMeasured(long elapsed)
        {
            LatencyMeasured?.Invoke(this, elapsed);
        }

        private void OnDataCounterIncrease(int data)
        {
            DataCounterChanged?.Invoke(this, data);
        }
    }

    public class NoReplyReceivedException : Exception
    {
        public NoReplyReceivedException():base("A message was sent but no reply was received.")
        {

        }
    }

    public class MessageNotSentException : Exception
    {
        public MessageNotSentException() : base("Message could not be sent.")
        {

        }
    }

    public class CouldNotConnectException : Exception
    {
        public CouldNotConnectException() : base("Could not connect to client.")
        {

        }
    }

    public class UnexpectedReplyReceivedException : Exception
    {
        public UnexpectedReplyReceivedException(byte[] reply, int bytesRead) : base("An unexpected reply was received.")
        {
            if (bytesRead < 256)
            {
                for (int i = 0; i < bytesRead; i++)
                {
                    ReplyReceived += $"[{reply[i]}] ";
                }
            }
        }
        
        public string ReplyReceived { get; }
    }
}
