﻿using System;
using System.Collections.ObjectModel;
using System.Xml;
using System.Xml.Serialization;

namespace RVDAC.RemoteMessenger.Models
{
    public class MessengerConfiguration
    {
        public const string DefaultFileName = "RVDAC.RemoteMessenger.Settings.xml";

        public ObservableCollection<MessengerClient> Clients { get; set; } = new ObservableCollection<MessengerClient>();

        public bool SaveToFile()
        {
            try
            {
                var serializer = new XmlSerializer(this.GetType());
                var writerSettings = new XmlWriterSettings
                {
                    Indent = true
                };
                using (var writer = XmlWriter.Create(AppDomain.CurrentDomain.BaseDirectory + "\\" + DefaultFileName, writerSettings))
                {
                    serializer.Serialize(writer, this);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
