﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Xml.Serialization;
using RVDAC.Common.Messenger;
using RVDAC.Common.Models;
using RVDAC.Common.ScsClient;


namespace RVDAC.RemoteMessenger.Models
{
    public class MessengerClient: ValidatableModel
    {
        private string _name, _ipAddress;
        private int _port;
        private int _retryConnectionDelay = 6;
        private CommsController _commsController;
        private ScsClient _scsClient;
        private readonly object _sensorValueListLock = new object();

        private void CommsController_IoPortFreed(object sender, EventArgs e)
        {
            foreach (var stream in DataStreams)
            {
                if(!stream.IsRealTimeStream && stream.LogfileRepository.NumDataPoints > 0)
                Task.Run(() =>
                {
                    stream.SendMessageFromQueue(CommsController);
                });
            }
        }

        private IPEndPoint GetEndPoint()
        {
            try
            {
                var address = IPAddress.Parse(ClientEndpointIpAddress);
                return new IPEndPoint(address, ClientEndpointPort);
            }
            catch
            {
                var address = IPAddress.Parse("0.0.0.0");
                return new IPEndPoint(address, 13101);
            }
        }

        public void StartAllStreams(ScsClient scsClient)
        {
            foreach (var stream in DataStreams)
            {
                StartOneStream(stream.StreamGuid, scsClient);
            }
        }

        public void StopAllStreams()
        {
            foreach (var stream in DataStreams)
            {
                StopOneStream(stream.StreamGuid);
            }
        }

        public void StartOneStream(Guid guid, ScsClient scsClient)
        {
            if (ScsClient == null)
                ScsClient = scsClient;
            var stream = DataStreams.FirstOrDefault(x => x.StreamGuid.Equals(guid));
            if (stream == null)
                return;

            //This event is triggered when the stream is ready for the latest
            //values from SCS
            stream.ReadyForDataPoint -= Stream_ReadyForDataPoint;
            stream.ReadyForDataPoint += Stream_ReadyForDataPoint;
            //This event is triggered when a new message is fully built and ready to send
            stream.NewMessageReady -= Stream_NewMessageReady;
            stream.NewMessageReady += Stream_NewMessageReady;
            stream.Start();
        }

        private void Stream_NewMessageReady(object sender, MessengerMessage messengerMessage)
        {
            var stream = (DataStream) sender;
            Task.Run(() =>
            {
                if (stream.IsRealTimeStream)
                {
                    //send single
                    stream.SendRealtimeMessage(messengerMessage, CommsController);
                    return;
                }
                //send from buffer
                stream.SendMessageFromQueue(CommsController);
            });
        }

        public void StopOneStream(Guid guid)
        {
            var stream = DataStreams.FirstOrDefault(x => x.StreamGuid.Equals(guid));
            if (stream == null)
                return;

            stream.ReadyForDataPoint -= Stream_ReadyForDataPoint;
            stream.Stop();
        }

        private void Stream_ReadyForDataPoint(object sender, DateTime timeStamp)
        {
            //if the SCS client isn't acquiring data then don't generate a datapoint
            if (ScsClient == null)
                return;
            var stream = (DataStream) sender;
            stream.GenerateNewMessage(PopulateSensorCollectionForStream(stream).ToList(), timeStamp);
        }

        private IEnumerable<MessengerDataPoint> PopulateSensorCollectionForStream(DataStream stream)
        {
            SensorValueHolder[] sensorValueList;
            lock (_sensorValueListLock)
            {
                sensorValueList = ScsClient.SensorConfig.CopyOfSensorValues();
            }

            foreach (var sensor in stream.SensorList)
            {
                //get the matching sensor from the SCS Configuration
                for (int i = 0; i < sensorValueList.Length; i++)
                {
                    if (sensorValueList[i].Guid.Equals(sensor.Guid))
                    {
                        yield return new MessengerDataPoint(sensorValueList[i].Id, sensor.UseRawValue ? sensorValueList[i].RawValue : sensorValueList[i].DecodedValue);
                        break;
                    }
                }
            }
        }

        #region properties

        [CustomValidation(typeof (MessengerClient), "IpAddressValidation")]
        [Required]
        public string ClientEndpointIpAddress
        {
            get { return _ipAddress; }
            set {
                if (value != _ipAddress)
                {
                    if (CommsController != null)
                        CommsController.RemoteEndpoint = GetEndPoint();
                }
                SetProperty(ref _ipAddress, value); }
        }

        public Guid ClientGuid { get; set; } = Guid.NewGuid();

        [Range(1, 65535)]
        [Required]
        public int ClientEndpointPort
        {
            get { return _port; }
            set
            {
                if (value != _port)
                {
                    if (CommsController != null)
                        CommsController.RemoteEndpoint = GetEndPoint();
                }
                    
                SetProperty(ref _port, value);
            }
        }

        [Range(1, 1000)]
        [Required]
        public int RetryConnectionDelay
        {
            get { return _retryConnectionDelay;}
            set
            {
                SetProperty(ref _retryConnectionDelay, value);
                if(CommsController != null)
                    CommsController.RetryConnectionDelay = value;
            }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                SetProperty(ref _name, value);
                //logfilename is changed when the client name is changed.
                //This will happen reliably at startup.
                LogfileRepository = new SqliteRepository(Name + "_ConnectionLog");
            }
        }

        [XmlIgnore]
        public SqliteRepository LogfileRepository { get; set; }

        [XmlIgnore]
        private ScsClient ScsClient {
            get { return _scsClient; }
            set
            {
                _scsClient = value;
                if (_scsClient != null)
                {
                    CommsController = new CommsController(GetEndPoint(), LogfileRepository, _scsClient);
                    CommsController.IoPortFreed -= CommsController_IoPortFreed;
                    CommsController.IoPortFreed += CommsController_IoPortFreed;
                }
            }
                
        }

        [XmlIgnore]
        public CommsController CommsController {
            get { return _commsController; }
            private set {SetProperty(ref _commsController, value);}
        }
        
        public ObservableCollection<DataStream> DataStreams { get; set; } =
            new ObservableCollection<DataStream>();

        public static ValidationResult IpAddressValidation(object obj, ValidationContext context)
        {
            var client = (MessengerClient)context.ObjectInstance;
            IPAddress ip;
            if (IPAddress.TryParse(client.ClientEndpointIpAddress, out ip))
                return ValidationResult.Success;

            return new ValidationResult("IP Address is not valid", new List<string> { "ClientEndpointIpAddress" });
        }

        #endregion
    }
}
