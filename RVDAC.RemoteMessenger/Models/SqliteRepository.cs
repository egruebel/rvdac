﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Windows.Data;

namespace RVDAC.RemoteMessenger.Models
{
    public class SqliteRepository: INotifyPropertyChanged
    {
        private readonly string _dBFileName;
        private readonly string _dBConnectionString;
        private uint _datapoints, _errors, _sent, _connections;
        private const int MaxTableSize = 25000;
        private const int MaxRecentListSize = 200;
        
        private static class Tables
        {
            public const string DataPoint = "DataPoint";
            public const string Sent = "Sent";
            public const string Error = "Error";
            public const string TcpConnection = "TcpConnection";
        }

        private readonly object _listLock = new object();

        public event PropertyChangedEventHandler PropertyChanged;

        public SqliteRepository(string name) 
        {
            //create the database if it doesn't exist
            
            Name = name;
            _dBFileName = $"{name}-{DateTime.Now.Year}.sqlite";
            _dBConnectionString = $"Data Source={_dBFileName};Version=3;FKSupport=True";
            var dbFile = new FileInfo(_dBFileName);
            if (!dbFile.Exists)
            {
                CreateSqliteDatabase();
            }

            LatestErrors = new ObservableCollection<RepoErrorLog>(GetListOf<RepoErrorLog>(MaxRecentListSize, "Error"));
            BindingOperations.EnableCollectionSynchronization(LatestErrors, _listLock);

            LatestSents = new ObservableCollection<RepoSentLog>(GetListOf<RepoSentLog>(MaxRecentListSize, "Sent"));
            BindingOperations.EnableCollectionSynchronization(LatestSents, _listLock);

            LatestConnections = new ObservableCollection<RepoConnectionLog>(GetListOf<RepoConnectionLog>(MaxRecentListSize, "TcpConnection"));
            BindingOperations.EnableCollectionSynchronization(LatestConnections, _listLock);

            InitializeTableCounts();
        }

        private void InitializeTableCounts()
        {
            NumDataPoints = GetTableCount(Tables.DataPoint);
            NumSentPoints = GetTableCount(Tables.Sent);
            NumConnections = GetTableCount(Tables.TcpConnection);
            NumErrors = GetTableCount(Tables.Error);
        }

        public string Name { get; set; }

        public ObservableCollection<RepoErrorLog> LatestErrors { get; private set; }

        public ObservableCollection<RepoSentLog> LatestSents { get; private set; }

        public ObservableCollection<RepoConnectionLog> LatestConnections { get; private set; }

        private uint GetTableCount(string tableName)
        {
            
            try
            {
                uint count = 0;
                using (var db = new SQLiteConnection(_dBConnectionString))
                {
                    db.Open();
                    using (var transaction = db.BeginTransaction()) //all or nothing
                    {
                        using (var command = new SQLiteCommand(db))
                        {
                            command.Transaction = transaction;
                            command.CommandText = $"Select count(*) from {tableName}";
                            count = Convert.ToUInt32(command.ExecuteScalar());
                        }
                        transaction.Commit();
                    }
                    db.Close();
                }
                return count;
            }
            catch (Exception e)
            {
                //we cant log the message because the backup database is not working for some reason
                Console.WriteLine(e.Message);
                return 0;
            }
        }

        private void UpdateLatestList<T>(ObservableCollection<T> list, object itemToAdd)
        {
            //determine type of 

            lock (_listLock)
            {
                while (list.Count > MaxRecentListSize)
                {
                    list.RemoveAt(MaxRecentListSize);
                }
                list.Insert(0, (T)itemToAdd);
            }
        }

        public uint NumDataPoints
        {
            get { return _datapoints; }
            set
            {
                _datapoints = value;
                OnPropertyChanged(nameof(NumDataPoints));
            }
        }

        public uint NumSentPoints
        {
            get { return _sent; }
            set
            {
                _sent = value;
                OnPropertyChanged(nameof(NumSentPoints));
            }
        }

        public uint NumConnections
        {
            get { return _connections; }
            set
            {
                _connections = value;
                OnPropertyChanged(nameof(NumConnections));
            }
        }

        public uint NumErrors
        {
            get { return _errors; }
            set
            {
                _errors = value;
                OnPropertyChanged(nameof(NumErrors));
            }
        }

        private void OnPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        public void ShrinkTable(string tableName)
        {
            var query = $"DELETE FROM {tableName} WHERE Id IN (SELECT Id FROM {tableName} ORDER BY Id ASC LIMIT {MaxTableSize / 2})";
            try
            {
                using (var db = new SQLiteConnection(_dBConnectionString))
                {
                    db.Open();
                    using (var transaction = db.BeginTransaction()) //all or nothing
                    {
                        using (var command = new SQLiteCommand(db))
                        {
                            command.Transaction = transaction;
                            command.CommandText = query;
                            command.ExecuteNonQuery();
                        }
                        transaction.Commit();
                        InitializeTableCounts();
                    }
                    db.Close();
                }
            }
            catch (Exception e)
            {
                //we cant log the message because the backup database is not working for some reason
                Console.WriteLine(e.Message);
            }
        }

        public void DeleteOldLogs()
        {
            if(NumDataPoints > MaxTableSize)
                ShrinkTable(Tables.DataPoint);
            if (NumSentPoints > MaxTableSize)
                ShrinkTable(Tables.Sent);
            if (NumErrors > MaxTableSize)
                ShrinkTable(Tables.Error);
            if (NumConnections > MaxTableSize)
                ShrinkTable(Tables.TcpConnection);
        }

        public void DeleteAllDataPoints()
        {
            try
            {
                using (var db = new SQLiteConnection(_dBConnectionString))
                {
                    db.Open();
                    using (var transaction = db.BeginTransaction()) //all or nothing
                    {
                        using (var command = new SQLiteCommand(db))
                        {
                            command.Transaction = transaction;
                            command.CommandText =
                                $"DELETE FROM {Tables.DataPoint}";
                            command.ExecuteNonQuery();
                        }
                        transaction.Commit();
                    }
                    
                    db.Close();
                }
                NumDataPoints = GetTableCount(Tables.DataPoint);
            }
            catch (Exception e)
            {
                //we cant log the message because the backup database is not working for some reason
                Console.WriteLine(e.Message);
            }
        }


        public void DeleteFirstDataPoint()
        {
            try
            {
                using (var db = new SQLiteConnection(_dBConnectionString))
                {
                    db.Open();
                    using (var transaction = db.BeginTransaction()) //all or nothing
                    {
                        using (var command = new SQLiteCommand(db))
                        {
                            command.Transaction = transaction;
                            command.CommandText =
                                $"DELETE FROM {Tables.DataPoint} Where Id in (SELECT Id FROM {Tables.DataPoint} ORDER BY Id ASC LIMIT 1)";
                            command.ExecuteNonQuery();
                        }
                        transaction.Commit();
                        
                    }
                    db.Close();
                }
                NumDataPoints = GetTableCount(Tables.DataPoint);

            }
            catch (Exception e)
            {
                //we cant log the message because the backup database is not working for some reason
                Console.WriteLine(e.Message);
            }
        }

        public void UpdateDatapointErrorCount(long datapointId, long errors)
        {
            try
            {
                using (var db = new SQLiteConnection(_dBConnectionString))
                {
                    db.Open();
                    using (var transaction = db.BeginTransaction()) //all or nothing
                    {
                        using (var command = new SQLiteCommand(db))
                        {
                            command.Transaction = transaction;
                            command.CommandText =
                                $"UPDATE {Tables.DataPoint} SET ErrorCount = " + errors + " WHERE ID =" + datapointId + ";";
                            command.ExecuteNonQuery();
                        }
                        transaction.Commit();
                    }
                    db.Close();
                }
            }
            catch (Exception e)
            {
                //we cant log the message because the backup database is not working for some reason
                Console.WriteLine(e.Message);
            }
        }

        public bool TryInsertDataPoint(byte[] dataBytes, DateTime insertTime)
        {
            try
            {
                using (var db = new SQLiteConnection(_dBConnectionString))
                {
                    db.Open();
                    using (var transaction = db.BeginTransaction()) //all or nothing
                    {
                        using (var command = new SQLiteCommand(db))
                        {
                            command.Transaction = transaction;
                            command.CommandText =
                                $"INSERT INTO {Tables.DataPoint} VALUES (@Id, @DateTime, @ErrorCount, @Object );";
                            command.Parameters.Add("@Id", DbType.Int32);
                            command.Parameters.Add("@DateTime", DbType.DateTime).Value = insertTime;
                            command.Parameters.Add("@ErrorCount", DbType.Int32).Value = 0;
                            command.Parameters.Add("@Object", DbType.Binary).Value = dataBytes;
                            command.ExecuteNonQuery();
                        }
                        transaction.Commit();
                        
                    }
                    db.Close();
                }
                NumDataPoints = GetTableCount(Tables.DataPoint);
                return true;
            }
            catch (Exception e)
            {
                //we cant log the message because the backup database is not working for some reason
                return false;
            }
        }

        public void InsertSent(DateTime date, DateTime msgDate, string text, int size)
        {
            try
            {
                long lastId;
                using (var db = new SQLiteConnection(_dBConnectionString))
                {
                    db.Open();
                    using (var transaction = db.BeginTransaction()) //all or nothing
                    {
                        using (var command = new SQLiteCommand(db))
                        {
                            command.Transaction = transaction;
                            command.CommandText =
                                $"INSERT INTO {Tables.Sent} VALUES (@Id, @DateTime, @MessageDateTime, @MessageDelay, @Text, @Size, @TransferSize);";
                            command.Parameters.Add("@Id", DbType.Int32);
                            command.Parameters.Add("@DateTime", DbType.DateTime).Value = date;
                            command.Parameters.Add("@MessageDateTime", DbType.DateTime).Value = msgDate;
                            command.Parameters.Add("@MessageDelay", DbType.Int32).Value = ((int)(date - msgDate).TotalSeconds) / 60;
                            command.Parameters.Add("@Text", DbType.AnsiString).Value = text;
                            command.Parameters.Add("@Size", DbType.Int32).Value = size;
                            command.Parameters.Add("@TransferSize", DbType.Int32).Value = 0;
                            command.ExecuteNonQuery();
                            command.CommandText = @"select last_insert_rowid()";
                            lastId = (long)command.ExecuteScalar();
                        }
                        transaction.Commit();
                        
                    }
                    db.Close();
                }
                NumSentPoints = GetTableCount(Tables.Sent);
                //Update the recent list
                UpdateLatestList(LatestSents, new RepoSentLog()
                {
                    Id = lastId,
                    Date = date,
                    MessageDate = msgDate,
                    Size = size,
                    MessageDelay = ((int)(date - msgDate).TotalSeconds) / 60,
                    TransferSize = 0,
                    Text = text
                });

            }
            catch (Exception e)
            {
                //we cant log the message because the database is not working for some reason
                Console.WriteLine(e.Message);
            }
        }

        public void InsertTcpConnection(DateTime date, string text, int size)
        {
            try
            {
                long lastId;
                using (var db = new SQLiteConnection(_dBConnectionString))
                {
                    db.Open();
                    using (var transaction = db.BeginTransaction()) //all or nothing
                    {
                        using (var command = new SQLiteCommand(db))
                        {
                            command.Transaction = transaction;
                            command.CommandText =
                                $"INSERT INTO {Tables.TcpConnection} VALUES (@Id, @DateTime, @Size, @Text);";
                            command.Parameters.Add("@Id", DbType.Int32);
                            command.Parameters.Add("@DateTime", DbType.DateTime).Value = date;
                            command.Parameters.Add("@Size", DbType.Int32).Value = size;
                            command.Parameters.Add("@Text", DbType.AnsiString).Value = text;
                            command.ExecuteNonQuery();
                            command.CommandText = @"select last_insert_rowid()";
                            lastId = (long)command.ExecuteScalar();
                        }
                        transaction.Commit();
                        
                    }
                    db.Close();
                }
                NumConnections = GetTableCount(Tables.TcpConnection);
                //Update the recent list
                UpdateLatestList(LatestConnections, new RepoConnectionLog()
                {
                    Id = lastId,
                    Date = date,
                    Size = size,
                    Text = text
                });
            }
            catch (Exception e)
            {
                //we cant log the message because the database is not working for some reason
                Console.WriteLine(e.Message);
            }
        }

        public void InsertError(DateTime date, string text)
        {
            try
            {
                long lastId;
                using (var db = new SQLiteConnection(_dBConnectionString))
                {
                    db.Open();
                    using (var transaction = db.BeginTransaction()) //all or nothing
                    {
                        using (var command = new SQLiteCommand(db))
                        {
                            command.Transaction = transaction;
                            command.CommandText =
                                $"INSERT INTO {Tables.Error} VALUES (@Id, @DateTime, @Text);";
                            command.Parameters.Add("@Id", DbType.Int32);
                            command.Parameters.Add("@DateTime", DbType.DateTime).Value = date;
                            command.Parameters.Add("@Text", DbType.AnsiString).Value = text;
                            command.ExecuteNonQuery();
                            command.CommandText = @"select last_insert_rowid()";
                            lastId = (long)command.ExecuteScalar();
                        }
                        transaction.Commit();
                        
                    }
                    db.Close();
                }
                NumErrors++;
                //Update the recent list
                UpdateLatestList(LatestErrors, new RepoErrorLog()
                {
                    Id = lastId,
                    Date = date,
                    Text = text
                });
            }
            catch (Exception e)
            {
                //we cant log the message because the database is not working for some reason
                Console.WriteLine(e.Message);
            }
        }

        public List<T> GetListOf<T>(int limit, string tableName) where T: IRepositoryObject
        {
            var vals = new List<T>();
            try
            {
                using (var db = new SQLiteConnection(_dBConnectionString))
                {
                    db.Open();
                    using (var command = new SQLiteCommand(db))
                    {
                        command.CommandText = $"SELECT * FROM {tableName} ORDER BY Id DESC LIMIT {limit.ToString()}" ;
                        using (SQLiteDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var obj = (T)Activator.CreateInstance(typeof(T));
                                obj.CreateFromReader(reader);
                                vals.Add(obj);
                            }
                        }
                    }
                    db.Close();
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
            }
            return vals;
        }

        public RepoDataPointLog GetFirstDataPoint()
        {
            try
            {
                using (var db = new SQLiteConnection(_dBConnectionString))
                {
                    db.Open();
                    using (var transaction = db.BeginTransaction()) //all or nothing
                    {
                        using (var command = new SQLiteCommand(db))
                        {
                            command.Transaction = transaction;
                            command.CommandText =
                                $"SELECT * FROM {Tables.DataPoint} ORDER BY Id ASC LIMIT 1";
                            using (SQLiteDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    var dp = new RepoDataPointLog();
                                    dp.CreateFromReader(reader);
                                    return dp;
                                }
                            }
                        }
                        transaction.Commit();
                    }
                    db.Close();
                }
                return null;
            }
            catch (Exception e)
            {
                //we cant log the message because the backup database is not working for some reason
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public static byte[] GetBytes(SQLiteDataReader reader)
        {
            const int chunkSize = 2 * 1024;
            byte[] buffer = new byte[chunkSize];
            long bytesRead;
            long fieldOffset = 0;
            using (MemoryStream stream = new MemoryStream())
            {
                while ((bytesRead = reader.GetBytes(0, fieldOffset, buffer, 0, buffer.Length)) > 0)
                {
                    stream.Write(buffer, 0, (int)bytesRead);
                    fieldOffset += bytesRead;
                }
                return stream.ToArray();
            }
        }

        private void CreateSqliteDatabase()
        {
            try
            {
                SQLiteConnection.CreateFile(_dBFileName);

                const string createTableDatapoint = @"CREATE TABLE IF NOT EXISTS [DataPoint] (
                          [Id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                          [DateTime] DATETIME NOT NULL,
                          [ErrorCount] INTEGER NOT NULL,
                          [Object] BLOB NOT NULL
                          )";

                const string createTableSent = @"CREATE TABLE IF NOT EXISTS [Sent] (
                          [Id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                          [DateTime] DATETIME NOT NULL,
                          [MessageDateTime] DATETIME NOT NULL,
                          [MessageDelay] INTEGER NOT NULL,
                          [Text] VARCHAR(2048) NULL,
                          [Size] INTEGER NOT NULL,
                          [TransferSize] INTEGER NOT NULL
                          )";

                const string createTableConnection = @"CREATE TABLE IF NOT EXISTS [TcpConnection] (
                          [Id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                          [DateTime] DATETIME NOT NULL,
                          [Size] INTEGER NOT NULL,
                          [Text] VARCHAR(2048) NULL
                          )";

                const string createTableError = @"CREATE TABLE IF NOT EXISTS [Error] (
                          [Id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                          [DateTime] DATETIME NOT NULL,
                          [Text] VARCHAR(2048) NULL
                          )";
                using (var db = new SQLiteConnection(_dBConnectionString))
                {
                    db.Open();
                    using (var transaction = db.BeginTransaction())
                    {
                        using (var connection = new SQLiteCommand(db))
                        {
                            connection.Transaction = transaction;
                            connection.CommandText = createTableDatapoint;
                            connection.ExecuteNonQuery();
                            connection.CommandText = createTableSent;
                            connection.ExecuteNonQuery();
                            connection.CommandText = createTableConnection;
                            connection.ExecuteNonQuery();
                            connection.CommandText = createTableError;
                            connection.ExecuteNonQuery();
                        }
                        transaction.Commit();
                    }
                    db.Close();
                }
            }
            catch (Exception e)
            {
                
                Console.WriteLine(e.Message);
            }
        }
    }

    public class RepoDataPointLog : IRepositoryObject
    {
        public long Id { get; set; }
        public long ErrorCount { get; set; }
        public DateTime DateTime { get; set; }
        public byte[] Object { get; set; }

        public void CreateFromReader(SQLiteDataReader reader)
        {
            Id = (long)reader["Id"];
            DateTime = (DateTime)reader["DateTime"];
            ErrorCount = (long)reader["ErrorCount"];
            Object = (byte[])reader["Object"];
        }
    }

    public class RepoErrorLog : IRepositoryObject
    {
        public long Id { get; set; }
        public DateTime Date { get; set; }
        public string Text { get; set; }

        public void CreateFromReader(SQLiteDataReader reader)
        {
            Id = (long)reader["Id"];
            Date = (DateTime)reader["DateTime"];
            Text = (string)reader["Text"];
        }
    }

    public class RepoConnectionLog : IRepositoryObject
    {
        public long Id { get; set; }
        public DateTime Date { get; set; }
        public long Size { get; set; }
        public string Text { get; set; }

        public void CreateFromReader(SQLiteDataReader reader)
        {
            Id = (long)reader["Id"];
            Date = (DateTime)reader["DateTime"];
            Size = (long)reader["Size"];
            Text = (string)reader["Text"];
        }
    }

    public class RepoSentLog: IRepositoryObject
    {
        public long Id { get; set; }
        public DateTime Date { get; set; }
        public DateTime MessageDate { get; set; }
        public string Text { get; set; }
        public long Size { get; set; }
        public long TransferSize { get; set; }
        public long MessageDelay { get; set; }

        public void CreateFromReader(SQLiteDataReader reader)
        {
            Id = (long)reader["Id"];
            Date = (DateTime)reader["DateTime"];
            MessageDate = (DateTime)reader["MessageDateTime"];
            MessageDelay = (long)reader["MessageDelay"];
            Text = (string)reader["Text"];
            Size = (long)reader["Size"];
            TransferSize = (long)reader["TransferSize"];
        }
    }

    public interface IRepositoryObject
    {
        void CreateFromReader(SQLiteDataReader reader);
    }
}
