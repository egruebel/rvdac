﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using RVDAC.Common;
using RVDAC.RemoteMessenger.Models;
using RVDAC.UI;

namespace RVDAC.RemoteMessenger.ViewModels
{
    class ClientEditorViewModel: ViewModelBase 
    {
        public ClientEditorViewModel(MessengerClient client, MessengerConfiguration messengerConfig)
        {
            ClientReference = client;
            //reference the messenger configuration so we can save any changes to a file
            MessengerConfigurationReference = messengerConfig;
            ClientEditable = new MessengerClient();
            CopyMembers(client, ClientEditable);
        }

        public ClientEditorViewModel() { }

        private MessengerConfiguration MessengerConfigurationReference { get; set; }

        private MessengerClient ClientReference { get; set; }

        public MessengerClient ClientEditable { get; set; }

        public void CopyMembers(MessengerClient from, MessengerClient to)
        {
            to.Name = from.Name;
            to.ClientEndpointIpAddress = from.ClientEndpointIpAddress;
            to.ClientEndpointPort = from.ClientEndpointPort;
            to.ClientGuid = from.ClientGuid;
            to.RetryConnectionDelay = from.RetryConnectionDelay;
            to.DataStreams = from.DataStreams;
        }

        public ICommand AddNewStreamCommand
        {
            get { return new RelayCommand(AddNewStreamExecute, CanAddNewStream); }
        }

        bool CanAddNewStream()
        {
            return true;
        }

        void AddNewStreamExecute()
        {
            var newDataStream = new DataStream();
            var window = new StreamEditor();
            var viewModel = new StreamEditorViewModel(newDataStream, MessengerConfigurationReference)
            {
                Close = window.Close
            };
            window.DataContext = viewModel;
            window.ShowDialog();
            //If you're here then the window has closed
            if(viewModel.CloseMethod == FormCloseRoute.Submit)
                ClientEditable.DataStreams.Add(newDataStream);
        }

        public ICommand EditStreamCommand
        {
            get { return new RelayCommand<Guid>(EditStreamExecute, CanEditStream); }
        }

        bool CanEditStream(Guid guid)
        {
            return true;
        }

        void EditStreamExecute(Guid guid)
        {
            var dataStream = ClientEditable.DataStreams.FirstOrDefault(x => x.StreamGuid.Equals(guid));
            if (dataStream == null)
                return;
            var window = new StreamEditor();
            var viewModel = new StreamEditorViewModel(dataStream, MessengerConfigurationReference)
            {
                Close = window.Close
            };
            window.DataContext = viewModel;
            window.ShowDialog();
        }

        public ICommand DeleteStreamCommand
        {
            get { return new RelayCommand<Guid>(DeleteStreamExecute, CanDeleteStream); }
        }

        bool CanDeleteStream(Guid guid)
        {
            return true;
        }

        void DeleteStreamExecute(Guid guid)
        {
            var popup = MessageBox.Show("Are you sure you want to delete?", "Delete Stream", MessageBoxButton.YesNo,
                MessageBoxImage.Asterisk);
            if (popup != MessageBoxResult.Yes)
                return;
            //User clicked yes
            var itemToDelete = ClientEditable.DataStreams.FirstOrDefault(x => x.StreamGuid.Equals(guid));
            if (itemToDelete == null)
                return;
            ClientEditable.DataStreams.Remove(itemToDelete);
        }

        public ICommand SaveFormCommand
        {
            get { return new RelayCommand(SaveFormExecute, CanSaveForm); }
        }

        bool CanSaveForm()
        {
            return true;
        }

        void SaveFormExecute()
        {
            ClientEditable.Validate();
            if (ClientEditable.HasErrors)
                return;
            //okay to save
            ClientReference.StopAllStreams();
            CopyMembers(ClientEditable, ClientReference);
            MessengerConfigurationReference.SaveToFile();
            CloseMethod = FormCloseRoute.Submit;
            Close();
        }

        public ICommand CancelFormCommand
        {
            get { return new RelayCommand(CancelFormExecute, CanCancelForm); }
        }

        bool CanCancelForm()
        {
            return true;
        }

        void CancelFormExecute()
        {
            CloseMethod = FormCloseRoute.Cancel;
            Close();
        }

    }
}
