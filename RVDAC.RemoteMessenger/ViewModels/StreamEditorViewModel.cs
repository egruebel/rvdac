﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using RVDAC.Common;
using RVDAC.RemoteMessenger.Models;
using RVDAC.UI;

namespace RVDAC.RemoteMessenger.ViewModels
{
    public class StreamEditorViewModel: ViewModelBase
    {
        public StreamEditorViewModel()
        {
            
        }

        public StreamEditorViewModel(DataStream stream, MessengerConfiguration messengerConfig)
        {
            DataStreamReference = stream;
            DataStreamEditable = new DataStream();
            MessengerConfigurationReference = messengerConfig;

            CopyDataStreamMembers(DataStreamReference, DataStreamEditable);
        }

        private void CopyDataStreamMembers(DataStream from, DataStream to)
        {
            to.StreamName = from.StreamName;
            to.PollingInterval = from.PollingInterval;
            to.IsRealTimeStream = from.IsRealTimeStream;
            to.SensorList.Clear();
            foreach (var item in from.SensorList)
            {
                to.SensorList.Add(item);
            }
        }

        private DataStream DataStreamReference { get; set; }

        public DataStream DataStreamEditable { get; set; } = new DataStream();

        private MessengerConfiguration MessengerConfigurationReference { get; set; }

        public ICommand SaveStream
        {
            get { return new RelayCommand(SaveStreamExecute, CanSaveStream); }
        }

        bool CanSaveStream()
        {
            return !DataStreamEditable.HasErrors && DataStreamEditable.IsDirty;
        }

        void SaveStreamExecute()
        {
            DataStreamEditable.Validate();
            if (DataStreamEditable.HasErrors)
                return;
            CopyDataStreamMembers(DataStreamEditable, DataStreamReference);
            DataStreamEditable.IsDirty = false;
            CloseMethod = FormCloseRoute.Submit;
            Close();
        }

        public ICommand CancelCommand
        {
            get { return new RelayCommand(CancelExecute, CanCancel); }
        }

        bool CanCancel()
        {
            return true;
        }

        void CancelExecute()
        {
            CloseMethod = FormCloseRoute.Cancel;
            Close();
        }
    }
}
