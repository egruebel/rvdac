﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Xml;
using System.Xml.Serialization;
using RVDAC.Common;
using RVDAC.Common.LogFile;
using RVDAC.Common.ScsClient;
using RVDAC.RemoteMessenger.Models;
using RVDAC.RemoteMessenger.UserControls;
using RVDAC.UI;

namespace RVDAC.RemoteMessenger.ViewModels
{
    class MainWindowViewModel : ViewModelBase
    {
        private string _scsServerIp = "127.0.0.1";

        public MainWindowViewModel()
        {
#if DEBUG
            //Stops Visual Studio from running this code in the Designer Window
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject())) return;
            _scsServerIp = ScsClient.DebugScsServerIp;
#endif
            ScsClient = new ScsClient(_scsServerIp, 505);
            //Poll every second
            ScsClient.LockPollingTimeToClock();
            //Bind scsclient events
            ScsClient.SensorConfigurationReceived += ScsClient_SensorConfigurationReceived;
            //Load streams configurations
            ReadConfigFromFile();
            //Begin ACQ
            Task.Run(() => ScsClient.BeginAcquisition(1000));
        }

        private void ScsClient_SensorConfigurationReceived(object sender, EventArgs e)
        {
            Start_Streams();
        }

        public MessengerConfiguration MessengerConfiguration { get; set; } = new MessengerConfiguration();


        private void Start_Streams()
        {
            foreach (var client in MessengerConfiguration.Clients)
            {
                client.StopAllStreams();
                client.StartAllStreams(ScsClient);
            }
        }

        private void ReadConfigFromFile()
        {
            var serializer = new XmlSerializer(MessengerConfiguration.GetType());
            try
            {
                using (var reader = XmlReader.Create(AppDomain.CurrentDomain.BaseDirectory + "\\" + MessengerConfiguration.DefaultFileName))
                {
                    MessengerConfiguration = (MessengerConfiguration) serializer.Deserialize(reader);
                }
            }
            catch (Exception e)
            {
                var msg = $"Unable to load the RVDAC.RemoteMessenger configuration file. {e.Message} Would you like to create a new one?";
                var iom = MessageBox.Show(msg, "Config Error", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                if (iom == MessageBoxResult.Yes)
                {
                    MessengerConfiguration = new MessengerConfiguration();
                    MessengerConfiguration.Clients.Add(new MessengerClient());
                    MessengerConfiguration.SaveToFile();
                }
            }
        }

        public ICommand EditClientCommand
        {
            get { return new RelayCommand<Guid>(EditClientExecute, CanEditClient); }
        }

        bool CanEditClient(Guid guid)
        {
            return true;
        }

        void EditClientExecute(Guid guid)
        {
            var client = MessengerConfiguration.Clients.FirstOrDefault(x => x.ClientGuid.Equals(guid));
            if (client == null)
                return;

            var editorWindow = new ClientEditorWindow();
            var viewModel = new ClientEditorViewModel(client, MessengerConfiguration)
            {
                Close = editorWindow.Close
            };
            editorWindow.DataContext = viewModel;
            editorWindow.ShowDialog();
            //edit complete
            if (viewModel.CloseMethod == FormCloseRoute.Submit)
            {
                client.StartAllStreams(ScsClient);
            }
        }

        public ICommand AddNewClientCommand
        {
            get { return new RelayCommand(AddNewClientExecute, CanAddNewClient); }
        }

        bool CanAddNewClient()
        {
            return true;
        }

        void AddNewClientExecute()
        {
            var client = new MessengerClient();

            var editorWindow = new ClientEditorWindow();
            var viewModel = new ClientEditorViewModel(client, MessengerConfiguration)
            {
                Close = editorWindow.Close
            };
            editorWindow.DataContext = viewModel;
            editorWindow.ShowDialog();
            if (viewModel.CloseMethod == FormCloseRoute.Submit)
            {
                MessengerConfiguration.Clients.Add(client);
                MessengerConfiguration.SaveToFile();
                client.StartAllStreams(ScsClient);
            }
        }

        public ICommand DeleteClientCommand
        {
            get { return new RelayCommand<Guid>(DeleteClientExecute, CanDeleteClient); }
        }

        bool CanDeleteClient(Guid guid)
        {
            return true;
        }

        public void DeleteClientExecute(Guid guid)
        {
            var result = MessageBox.Show("Are you sure you want to delete?", "Delete Client", MessageBoxButton.YesNo,
                MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                var clientToDelete = MessengerConfiguration.Clients.FirstOrDefault(x => x.ClientGuid == guid);
                if (clientToDelete == null)
                    return;
                MessengerConfiguration.Clients.Remove(clientToDelete);
                MessengerConfiguration.SaveToFile();
            }
        }

        public ICommand ShowStreamDetailsCommand
        {
            get { return new RelayCommand<Guid>(ShowDetailsExecute, CanShowDetails); }
        }

        bool CanShowDetails(Guid guid)
        {
            return true;
        }

        void ShowDetailsExecute(Guid guid)
        {
            //get the proper stream
            var stream =
                MessengerConfiguration.Clients.SelectMany(x => x.DataStreams).FirstOrDefault(x => x.StreamGuid == guid);
            if (stream == null)
                return;

            var window = new DataStreamLogDisplay();
            var viewModel = new StreamLogDisplayViewModel()
            {
                Stream = stream,
                Close = window.Close
            };
            window.DataContext = viewModel;
            window.Show();
        }

        public ICommand ShowClientLogCommand
        {
            get { return new RelayCommand<Guid>(ShowClientLogExecute, CanShowClientLog); }
        }

        bool CanShowClientLog(Guid guid)
        {
            return true;
        }

        void ShowClientLogExecute(Guid guid)
        {
            var client = MessengerConfiguration.Clients.FirstOrDefault(x => x.ClientGuid == guid);
            if (client == null)
                return;
            var window = new ClientLogDisplay();
            var viewModel = new ClientLogDisplayViewModel()
            {
                Client = client,
                Close = window.Close
            };
            window.DataContext = viewModel;
            window.Show();
        }

        /*
        private static string GenerateRandomNumber()
        {
            var s = "";
            var rnd = new Random();
            s += rnd.Next(0, 9);
            s += rnd.Next(0, 9);
            s += rnd.Next(0, 9);
            s += rnd.Next(0, 9);
            return s;
        }
        */

        public ScsClient ScsClient { get; }

    }
}