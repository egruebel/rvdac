﻿using System.Windows.Input;
using RVDAC.Common;
using RVDAC.RemoteMessenger.Models;
using RVDAC.UI;

namespace RVDAC.RemoteMessenger.ViewModels
{
    public class StreamLogDisplayViewModel: ViewModelBase
    {
        public DataStream Stream { get; set; }

        public ICommand CloseWindowCommand
        {
            get { return new RelayCommand(CloseWindowExecute, CanCloseWindow); }
        }

        bool CanCloseWindow()
        {
            return true;
        }

        void CloseWindowExecute()
        {
            Close();
        }
    }
}
