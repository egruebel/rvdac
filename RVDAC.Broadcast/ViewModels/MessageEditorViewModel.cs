﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using RVDAC.Broadcast.Models;
using RVDAC.Common;
using RVDAC.UI;

namespace RVDAC.Broadcast.ViewModels
{
    public class MessageEditorViewModel : ViewModelBase
    {
        public MessageEditorViewModel()
        {
            
        }

        public MessageEditorViewModel(Message message, BroadcastConfiguration config)
        {
            MessageReference = message;
            CopyMembers(MessageReference, MessageEditable);
            BroadcastConfig = config;
        }

        private void CopyMembers(Message from, Message to)
        {
            to.Name = from.Name;
            to.Delimiter = from.Delimiter;
            to.SentencePreamble = from.SentencePreamble;
            to.BroadcastIntervalSeconds = from.BroadcastIntervalSeconds;
            to.IncludeChecksum = from.IncludeChecksum;
            to.IoPort = from.IoPort;
            to.SensorCollection.Clear();
            foreach (var sci in from.SensorCollection)
            {
                to.SensorCollection.Add(sci);
            }

        }

        public Message MessageReference  { get; set; }

        public Message MessageEditable { get; set; } = new Message();

        public BroadcastConfiguration BroadcastConfig { get; set; }

        public ICommand OpenIoPortManagerCommand
        {
            get { return new RelayCommand(OpenIoPortManagerExecute, CanOpenIoPortManager); }
        }

        private bool CanOpenIoPortManager()
        {
            return true;
        }

        private void OpenIoPortManagerExecute()
        {
            var viewModel = new IoPortWindowViewModel(BroadcastConfig);
            var window = new IoPortWindow()
            {
                DataContext = viewModel
            };
            viewModel.Close = window.Close;
            window.ShowDialog();
        }

        public ICommand SaveCommand
        {
            get { return new RelayCommand(SaveExecute, CanSave); }
        }

        private bool CanSave()
        {
            return true;
        }

        private void SaveExecute()
        {
            MessageEditable.Validate();
            if (MessageEditable.HasErrors)
                return;
            CopyMembers(MessageEditable, MessageReference);
            CloseMethod = FormCloseRoute.Submit;
            BroadcastConfig.SaveToFile();
            Close();
        }

        public ICommand CancelCommand
        {
            get { return new RelayCommand(CancelExecute, CanCancel); }
        }

        private bool CanCancel()
        {
            return true;
        }

        private void CancelExecute()
        {
            CloseMethod = FormCloseRoute.Cancel;
            Close();
        }
    }
}
