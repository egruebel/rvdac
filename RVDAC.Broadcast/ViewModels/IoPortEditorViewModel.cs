﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using RVDAC.Broadcast.Models;
using RVDAC.Common;
using RVDAC.UI;

namespace RVDAC.Broadcast.ViewModels
{
    class IoPortEditorViewModel: ViewModelBase
    {
        public IoPortEditorViewModel() { }

        public IoPortEditorViewModel(IIoPort port, BroadcastConfiguration config)
        {
            IoPortReference = port;

            //Instantiate a new port based on the type passed in the constructor
            var portType = port.GetType();
            IoPortEditable = (IIoPort)System.Activator.CreateInstance(portType);
            //OnPropertyChanged("IoPortEditable");
            //Copy the members
            IoPortReference.CopyMembersTo(IoPortEditable);

            BroadcastConfig = config;
        }

        private IIoPort IoPortReference { get; set; } 

        public IIoPort IoPortEditable { get; set; } 

        private BroadcastConfiguration BroadcastConfig { get; set; }

        public ICommand SaveCommand
        {
            get { return new RelayCommand(SaveExecute, CanSave); }
        }

        private bool CanSave()
        {
            return true;
        }

        private void SaveExecute()
        {
            IoPortEditable.Validate();
            if (IoPortEditable.HasErrors)
                return;
            IoPortEditable.CopyMembersTo(IoPortReference);
            CloseMethod = FormCloseRoute.Submit;
            Close();
        }

        public ICommand CancelCommand
        {
            get { return new RelayCommand(CancelExecute, CanCancel); }
        }

        private bool CanCancel()
        {
            return true;
        }

        private void CancelExecute()
        {
            CloseMethod = FormCloseRoute.Cancel;
            Close();
        }
    }
}
