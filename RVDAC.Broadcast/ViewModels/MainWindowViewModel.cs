﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Xml;
using System.Xml.Serialization;
using RVDAC.Broadcast.Models;
using RVDAC.Common;
using RVDAC.Common.ScsClient;
using RVDAC.UI;

namespace RVDAC.Broadcast.ViewModels
{
    class MainWindowViewModel: ViewModelBase 
    {
        private string _scsServerIp = "127.0.0.1";

        public ScsClient ScsClient { get; set; }

        public BroadcastConfiguration BroadcastConfiguration { get; set; }

        public MessageBroadcastController MessageController { get; set; }

        public MainWindowViewModel()
        {
#if DEBUG
            //Stops Visual Studio from running this code in the Designer Window
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject())) return;
            _scsServerIp = ScsClient.DebugScsServerIp;
#endif
            ScsClient = new ScsClient(_scsServerIp, 505);
            //Bind client events
            ScsClient.ErrorReceived += ScsClient_ErrorReceived;
            ScsClient.MessageReceived += ScsClient_MessageReceived;
            ScsClient.SensorConfigurationReceived += ScsClient_SensorConfigurationReceived;
            //Load the application config
            BroadcastConfiguration = ReadConfigFromFile();
            //Build the file controller
            MessageController = new MessageBroadcastController(BroadcastConfiguration, ScsClient);
            //Begin ACQ
            Task.Run(() => ScsClient.BeginAcquisition(1000));
        }

        private void ScsClient_SensorConfigurationReceived(object sender, EventArgs e)
        {
            
        }

        private void ScsClient_MessageReceived(object sender, ScsClientMessageReceivedEventArgs e)
        {
            if (e.MessageType == ScsClientSocket.ScsMessageType.SensorData)
            {
                if(!MessageController.IsStarted)
                    MessageController.StartAll();
            }
        }

        private void ScsClient_ErrorReceived(object sender, ScsClientErrorReceivedEventArgs e)
        {
            //throw new NotImplementedException();
        }

        private static BroadcastConfiguration ReadConfigFromFile()
        {
            var serializer = new XmlSerializer(typeof(BroadcastConfiguration));
            try
            {
                using (var reader = XmlReader.Create(AppDomain.CurrentDomain.BaseDirectory + "\\" + BroadcastConfiguration.DefaultFileName))
                {
                    var config = (BroadcastConfiguration)serializer.Deserialize(reader);
                    //Cross reference the sensor profiles
                    foreach (var message in config.Messages)
                    {
                        var port = config.GetPortByGuid(message.IoPortGuid);
                        if (port != null)
                            message.IoPort = port;
                    }
                    return config;
                }
            }
            catch (Exception e)
            {
                var msg =
                    string.Format(
                        "Unable to load the RVDAC.Broadcast configuration file. {0}. Would you like to create a new one?",
                        e.Message);
                var iom = MessageBox.Show(msg, "Config Error", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                if (iom == MessageBoxResult.Yes)
                {
                    var config = new BroadcastConfiguration();
                    config.SaveToFile();
                    return config;
                }
                return new BroadcastConfiguration();
            }
        }

        public ICommand AddNewMessageCommand
        {
            get { return new RelayCommand(AddNewMessageExecute, CanAddNewMessage); }
        }

        private bool CanAddNewMessage()
        {
            return true;
        }

        private void AddNewMessageExecute()
        {
            var model = new Message();
            var viewModel = new MessageEditorViewModel(model, BroadcastConfiguration);
            var window = new MessageEditorWindow()
            {
                DataContext = viewModel,
            };
            viewModel.Close = window.Close;
            window.ShowDialog();
            if (viewModel.CloseMethod == FormCloseRoute.Submit)
            {
                BroadcastConfiguration.Messages.Add(model);
                MessageController.StartOne(model);
                BroadcastConfiguration.SaveToFile();
            }
        }

        public ICommand EditMessageCommand
        {
            get { return new RelayCommand<Guid>(EditMessageExecute, CanEditMessage); }
        }

        private bool CanEditMessage(Guid guid)
        {
            return true;
        }

        private void EditMessageExecute(Guid guid)
        {
            var model = BroadcastConfiguration.Messages.FirstOrDefault(x => x.MessageGuid == guid);
            if (model == null)
                return;
            var viewModel = new MessageEditorViewModel(model, BroadcastConfiguration);
            var window = new MessageEditorWindow()
            {
                DataContext = viewModel,
            };
            viewModel.Close = window.Close;
            window.ShowDialog();
            if (viewModel.CloseMethod == FormCloseRoute.Submit)
            {
                //Restart the message
                MessageController.StopOne(model);
                MessageController.StartOne(model);
                BroadcastConfiguration.SaveToFile();
            }
        }

        public ICommand DeleteMessageCommand
        {
            get { return new RelayCommand<Guid>(DeleteMessageExecute, CanDeleteMessage); }
        }

        private bool CanDeleteMessage(Guid guid)
        {
            return true;
        }

        private void DeleteMessageExecute(Guid guid)
        {
            var confirmation = MessageBox.Show("Are you sure?", "Delete Message", MessageBoxButton.YesNo,
                MessageBoxImage.Asterisk);
            if (confirmation == MessageBoxResult.Yes)
            {
                var message = BroadcastConfiguration.Messages.FirstOrDefault(x => x.MessageGuid == guid);
                if (message != null)
                {
                    MessageController.StopOne(message);
                    BroadcastConfiguration.Messages.Remove(message);
                    BroadcastConfiguration.SaveToFile();
                }
                    
            }
        }

        public ICommand StartMessageCommand
        {
            get { return new RelayCommand<Guid>(StartMessageExecute, CanStartMessage); }
        }

        private bool CanStartMessage(Guid guid)
        {
            
            var message = BroadcastConfiguration.Messages.FirstOrDefault(x => x.MessageGuid == guid);
            if (message == null)
                return false;
            if (message.IsBroadcasting)
                return false;
            return true;
        }

        private void StartMessageExecute(Guid guid)
        {
            if (!ScsClient.IsConnected)
                return;
            var message = BroadcastConfiguration.Messages.FirstOrDefault(x => x.MessageGuid == guid);
            if (message == null)
                return;
            if (!message.IsBroadcasting)
                MessageController.StartOne(message);
        }

        public ICommand StopMessageCommand
        {
            get { return new RelayCommand<Guid>(StopMessageExecute, CanStopMessage); }
        }

        private bool CanStopMessage(Guid guid)
        {
            var message = BroadcastConfiguration.Messages.FirstOrDefault(x => x.MessageGuid == guid);
            if (message == null)
                return false;
            if (message.IsBroadcasting)
                return true;
            return false;
        }

        private void StopMessageExecute(Guid guid)
        {
            var message = BroadcastConfiguration.Messages.FirstOrDefault(x => x.MessageGuid == guid);
            if (message == null)
                return;
            if (message.IsBroadcasting)
                MessageController.StopOne(message);
        }

        public ICommand OpenIoPortManagerCommand
        {
            get { return new RelayCommand(OpenIoPortManagerExecute, CanOpenIoPortManager); }
        }

        private bool CanOpenIoPortManager()
        {
            return true;
        }

        private void OpenIoPortManagerExecute()
        {
            var viewModel = new IoPortWindowViewModel(BroadcastConfiguration);
            var window = new IoPortWindow()
            {
                DataContext = viewModel
            };
            viewModel.Close = window.Close;
            window.ShowDialog();
        }
    }
}
