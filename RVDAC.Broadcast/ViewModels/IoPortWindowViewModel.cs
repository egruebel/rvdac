﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using RVDAC.Broadcast.Models;
using RVDAC.Common;
using RVDAC.UI;

namespace RVDAC.Broadcast.ViewModels
{
    public class IoPortWindowViewModel : ViewModelBase
    {
        public BroadcastConfiguration Config { get; set; }

        public IoPortWindowViewModel()
        {
            
        }

        public IoPortWindowViewModel(BroadcastConfiguration config)
        {
            Config = config;
        }

        public ICommand AddNewSerialPortCommand
        {
            get { return new RelayCommand(AddNewSerialPortExecute, CanAddNewSerialPort); }
        }

        private bool CanAddNewSerialPort()
        {
            return true;
        }

        private void AddNewSerialPortExecute()
        {
            var model = new SerialPort();
            var viewModel = new IoPortEditorViewModel(model, Config);
            var window = new IoPortEditorWindow()
            {
                DataContext=viewModel
            };
            viewModel.Close = window.Close;
            window.ShowDialog();
            if (viewModel.CloseMethod == FormCloseRoute.Submit)
                Config.IoPorts.SerialPorts.Add(model);
        }

        public ICommand AddNewUdpPortCommand
        {
            get { return new RelayCommand(AddNewUdpPortExecute, CanAddNewUdpPort); }
        }

        private bool CanAddNewUdpPort()
        {
            return true;
        }

        private void AddNewUdpPortExecute()
        {
            var model = new UdpPort();
            var viewModel = new IoPortEditorViewModel(model, Config);
            var window = new IoPortEditorWindow()
            {
                DataContext = viewModel
            };
            viewModel.Close = window.Close;
            window.ShowDialog();
            if (viewModel.CloseMethod == FormCloseRoute.Submit)
                Config.IoPorts.UdpPorts.Add(model);
        }

        public ICommand EditPortCommand
        {
            get { return new RelayCommand<Guid>(EditPortExecute, CanEditPort); }
        }

        private bool CanEditPort(Guid guid)
        {
            return true;
        }

        private void EditPortExecute(Guid guid)
        {
            var model = Config.GetPortByGuid(guid);
            if (model == null)
                return;
            var viewModel = new IoPortEditorViewModel(model, Config);
            var window = new IoPortEditorWindow()
            {
                DataContext = viewModel
            };
            viewModel.Close = window.Close;
            window.ShowDialog();
        }

        public ICommand DeletePortCommand
        {
            get { return new RelayCommand<Guid>(DeletePortExecute, CanDeletePort); }
        }

        private bool CanDeletePort(Guid guid)
        {
            return !Config.Messages.Select(x => x.IoPortGuid).Contains(guid);
        }

        private void DeletePortExecute(Guid guid)
        {
            foreach (var port in Config.IoPorts.UdpPorts)
            {
                if (port.PortId.Equals(guid))
                {
                    Config.IoPorts.UdpPorts.Remove(port);
                    return;
                }
            }
            foreach (var port in Config.IoPorts.SerialPorts)
            {
                if (port.PortId.Equals(guid))
                {
                    Config.IoPorts.SerialPorts.Remove(port);
                    return;
                }
            }
        }

        public ICommand SaveCommand
        {
            get { return new RelayCommand(SaveExecute, CanSave); }
        }

        private bool CanSave()
        {
            return true;
        }

        private void SaveExecute()
        {
            Config.SaveToFile();
            Close();
        }

        public ICommand CancelCommand
        {
            get { return new RelayCommand(CancelExecute, CanCancel); }
        }

        private bool CanCancel()
        {
            return true;
        }

        private void CancelExecute()
        {

        }
    }
}
