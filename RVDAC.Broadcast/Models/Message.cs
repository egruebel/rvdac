﻿using System;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Runtime.ExceptionServices;
using System.Threading;
using System.Timers;
using System.Xml.Serialization;
using RVDAC.Common;
using RVDAC.Common.LogFile;
using RVDAC.Common.Models;
using Timer = System.Timers.Timer;

namespace RVDAC.Broadcast.Models
{
    public class Message: ValidatableModel
    {
        private string _name = "", _sentencePreamble = "", _messageText="";
        private char _delimiter = ',';
        private bool _includeChecksum, _portActivity, _isBroadcasting;
        private int _broadcastIntervalSeconds = 2;
        private IIoPort _ioPort;
        private Timer _messageTimer;
        public event EventHandler ReadyForMessage;
        public IoError _errorStatus;

        [Required]
        public string Name
        {
            get { return _name; }
            set
            {
                SetProperty(ref _name, value);
            }
        }

        [Required]
        public char Delimiter
        {
            get { return _delimiter; }
            set
            {
                SetProperty(ref _delimiter, value);
            }
        }

        [Required]
        public int BroadcastIntervalSeconds
        {
            get { return _broadcastIntervalSeconds; }
            set
            {
                SetProperty(ref _broadcastIntervalSeconds, value);
            }
        }

        public string SentencePreamble
        {
            get { return _sentencePreamble; }
            set
            {
                SetProperty(ref _sentencePreamble, value);
            }
        }

        public bool IncludeChecksum
        {
            get { return _includeChecksum; }
            set
            {
                SetProperty(ref _includeChecksum, value);
            }
        }

        public Guid IoPortGuid { get; set; } //This GUID is saved in the XML config file so we don't have redundant IO ports

        [XmlIgnore]
        [Required]
        public IIoPort IoPort {
            get { return _ioPort; }
            set
            {
                if (value == null)
                    return;
                SetProperty(ref _ioPort, value);
                IoPortGuid = value.PortId;
            }
        }

        [XmlIgnore]
        public IoError ErrorStatus { get; private set; } = new IoError();

        [XmlIgnore]
        public string MessageText
        {
            get { return _messageText; }
            set
            {
                SetProperty(ref _messageText, value);
            }
        }

        [XmlIgnore]
        public bool PortActivity
        {
            get { return _portActivity; }
            private set
            {
                Task.Run(() =>
                {
                    if (_portActivity != value)
                    {
                        _portActivity = value;
                        OnPropertyChanged(nameof(PortActivity));
                        Thread.Sleep(100);
                        _portActivity = false;
                        OnPropertyChanged(nameof(PortActivity));
                    }
                });
            }
        }

        [XmlIgnore]
        public bool IsBroadcasting
        {
            get { return _isBroadcasting; }
            private set
            {
                SetProperty(ref _isBroadcasting, value);
            }
        }

        public void SendMessage(string messageText)
        {
            try
            {
                MessageText = messageText.Replace("\r\n", "<cr><lf>");
                IoPort.SendData(this, Encoding.ASCII.GetBytes(messageText + "\r\n"), true);
                PortActivity = true;
                ErrorStatus.ClearError();
            }
            catch(Exception ex)
            {
                ErrorStatus.SetError(ex.Message);
                LogFile.AddError(ex.Message);
            }
        }

        public void Start()
        {
            _messageTimer = new Timer(BroadcastIntervalSeconds * 1000);
            _messageTimer.Elapsed -= MessageTimerOnElapsed;
            _messageTimer.Elapsed += MessageTimerOnElapsed;
            _messageTimer.AutoReset = true;
            //IoPort.OpenPort();
            _messageTimer.Start();
            IsBroadcasting = true;
        }

        public void Stop()
        {
            _messageTimer?.Stop();
            IsBroadcasting = false;
        }

        private void MessageTimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            Task.Run(() =>
            {
                ReadyForMessage?.Invoke(this, new EventArgs());
            });
        }

        public Guid MessageGuid { get; set; } = Guid.NewGuid();

        public ObservableCollection<SensorCollectionItem> SensorCollection { get; set; } = new ObservableCollection<SensorCollectionItem>();
    }
}
