﻿using System;
using System.Linq;
using System.Threading;
using RVDAC.Common.LogFile;
using RVDAC.Common.ScsClient;

namespace RVDAC.Broadcast.Models
{
    public class MessageBroadcastController
    {
        public MessageBroadcastController(BroadcastConfiguration broadcastConfig, ScsClient scsClient)
        {
            BroadcastConfig = broadcastConfig;
            ScsClient = scsClient;
        }

        private object _listLock = new object();

        private BroadcastConfiguration BroadcastConfig { get; }

        private ScsClient ScsClient { get; }

        public bool IsStarted { get; set; } = false;

        private SensorValueHolder[] ScsSensorValues { get; set; }

        private void ScsClient_MessageReceived(object sender, ScsClientMessageReceivedEventArgs e)
        {
            lock (_listLock)
            {
                ScsSensorValues = ScsClient.SensorConfig.CopyOfSensorValues();
            }
        }

        private void Msg_ReadyForMessage(object sender, EventArgs e)
        {
            //Sometimes the events aren't triggered in order. This only happens at startup
            if (ScsSensorValues == null)
                return;
            var message = (Message) sender;

            //change special 'r' character for <cr><lf>
            var delim = message.Delimiter == 'r' ? "\r\n" : message.Delimiter.ToString();

            //generate the new message in a variable so we're not constantly raising the PropertyChanged event
            var messageTextBuilder = message.SentencePreamble;

            //Add a delimiter only if there was a preamble
            if (!string.IsNullOrEmpty(messageTextBuilder))
                messageTextBuilder += delim;

            //the following loop needs to be very fast so dont do anything unnecessary in here
            //get the sensor values from the SCS client collection
            lock (_listLock)
            {
                for (int i = 0; i < message.SensorCollection.Count; i++)
                {
                    for (int j = 0; j < ScsSensorValues.Length; j++)
                    {
                        if (message.SensorCollection[i].Guid.Equals(ScsSensorValues[j].Guid))
                        {
                            messageTextBuilder += message.SensorCollection[i].UseRawValue ? ScsSensorValues[j].RawValue: ScsSensorValues[j].DecodedValue;
                            break;
                        }
                    }
                    if (i != message.SensorCollection.Count - 1)
                        messageTextBuilder += delim;
                }
            }
            if (message.IncludeChecksum)
                messageTextBuilder += '*' + Checksum(messageTextBuilder);

            if(ScsClient.IsConnected)
                message.SendMessage(messageTextBuilder);
        }

        public static string Checksum(string val)
        {
            int c = 0;

            foreach (char s in val)
                c ^= Convert.ToByte(s);

            return c.ToString("X2");
        }

        public void StartAll()
        {
            ScsClient.MessageReceived -= ScsClient_MessageReceived;
            ScsClient.MessageReceived += ScsClient_MessageReceived;

            foreach (var msg in BroadcastConfig.Messages)
            {
                StartOne(msg);
                Thread.Sleep(25);
            }

            IsStarted = true;
        }

        public void StopAll()
        {
            foreach (var msg in BroadcastConfig.Messages)
            {
                StopOne(msg);
            }
            foreach (var port in BroadcastConfig.AllIoPorts)
            {
                port.ClosePort();
            }
            IsStarted = false;
        }

        public void StartOne(Message message)
        {
            message.ReadyForMessage -= Msg_ReadyForMessage;
            message.ReadyForMessage += Msg_ReadyForMessage;
            message.Start();
            LogFile.AddInfo($"Started message {message.Name}");
        }

        public void StopOne(Message message)
        {
            message.ReadyForMessage -= Msg_ReadyForMessage;
            message.Stop();
            LogFile.AddInfo($"Stopped message {message.Name}");
        }
    }
}
