﻿
using System.Xml.Serialization;
using RVDAC.Common.Models;

namespace RVDAC.Broadcast.Models
{
    public class IoError: ValidatableModel
    {
        private string _message;
        private bool _hasError;

        public void SetError(string message)
        {
            HasError = true;
            Message = message;
        }

        public void ClearError()
        {
            HasError = false;
            Message = string.Empty;
        }

        [XmlIgnore]
        public bool HasError
        {
            get {return _hasError;}
            private set { SetProperty(ref _hasError, value); }
        }

        [XmlIgnore]
        public string Message {
            get { return _message; }
            private set { SetProperty(ref _message, value); }
        }
    }
}
