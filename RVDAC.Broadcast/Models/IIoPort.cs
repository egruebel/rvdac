﻿using System;
using System.Collections.Generic;

namespace RVDAC.Broadcast.Models
{
    public interface IIoPort
    {
        event EventHandler DataReceived;
        string Name { get; }
        Guid PortId { get; set; }
        bool IoInProgress { get; set; }
        bool HasErrors { get; }

        void OpenPort();
        void ClosePort();
        void Validate();
        void CopyMembersTo(IIoPort port);
        void SendData(object sender, byte[] data, bool async = false);
        bool PortAvailable();
    }
}
