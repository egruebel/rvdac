﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection.Emit;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using RJCP.IO.Ports;
using RVDAC.Common.Models;

namespace RVDAC.Broadcast.Models
{
    public enum DataBitsEnum
    {
        Five = 5, Six = 6, Seven = 7, Eight = 8
    }

    public class SerialPort: ValidatableModel, IIoPort
    {
        private SerialPortStream _serialPortStream;
        private string _comPort = "COM1";
        private int _baudRate = 4800;
        private DataBitsEnum _dataBits = DataBitsEnum.Eight;
        private StopBits _stopBits = StopBits.One;
        private Parity _parity = Parity.None;
        private bool _ioInProgress;
        private object _serialPortLock = new object();

        public event EventHandler DataReceived;

        [Required]
        public Guid PortId { get; set; } = Guid.NewGuid();
        
        [XmlIgnore]
        public string Name
        {
            get { return $"{ComPort}-{BaudRate}-{(int)DataBits}-{ParityString(Parity)}-{StopBitsString(StopBits)}"; }
        }

        [Required]
        public string ComPort {
            get { return _comPort; }
            set
            {
                SetProperty(ref _comPort, value);
                OnPropertyChanged(nameof(Name));
            }
        } 
        
        [Required]
        [CustomValidation(typeof(SerialPort), "BaudRateValidation")]
        public int BaudRate {
            get { return _baudRate; }
            set
            {
                SetProperty(ref _baudRate, value);
                OnPropertyChanged(nameof(Name));
            }
        } 

        [XmlIgnore]
        public int[] BaudRates
        {
            get
            {
                return new [] {300, 600, 1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200};
            }
        }

        [Required]
        public DataBitsEnum DataBits {
            get { return _dataBits; }
            set
            {
                SetProperty(ref _dataBits, value);
                OnPropertyChanged(nameof(Name));
            }
        } 

        [Required]
        public Parity Parity {
            get { return _parity; }
            set
            {
                SetProperty(ref _parity, value);
                OnPropertyChanged(nameof(Name));
            }
        } 

        [Required]
        public StopBits StopBits {
            get { return _stopBits; }
            set
            {
                SetProperty(ref _stopBits, value);
                OnPropertyChanged(nameof(Name));
            }
        }

        public bool IoInProgress
        {
            get { return _ioInProgress; }
            set
            {
                SetProperty(ref _ioInProgress, value);
            }
        }

        public void SendData(object sender, byte[] data, bool async = false)
        {
            lock (_serialPortLock)
            {
                if (_serialPortStream == null || !_serialPortStream.IsOpen)
                    OpenPort();
                if (_serialPortStream.IsDisposed)
                    throw new InvalidOperationException("Serial port is closed");
                if (_serialPortStream.WriteBufferSize - data.Length < 0)
                    throw new InvalidOperationException("Serial port write buffer overflow");
                if (_serialPortStream.CanWrite)
                {
                    if (async)
                    {
                        var callback = new AsyncCallback(WriteComplete);
                        _serialPortStream.BeginWrite(data, 0, data.Length, callback, new object());
                        return;
                    }
                    //not async
                    _serialPortStream.Write(data, 0, data.Length);
                    _serialPortStream.Flush();
                    return;
                }
                throw new InvalidOperationException("Serial port stream is unable to write");
            }
        }

        private static void WriteComplete(IAsyncResult result)
        {
            
        }

        public bool PortAvailable()
        {
            foreach (var portName in SerialPortStream.GetPortNames())
            {
                if (portName.Equals(ComPort))
                    return true;
            }
            return false;
        }

        public void OpenPort()
        {
            _serialPortStream = new SerialPortStream(ComPort, BaudRate, (int) DataBits, Parity, StopBits);
            _serialPortStream.Open();
        }

        public void ClosePort()
        {
            if (_serialPortStream.IsOpen)
                _serialPortStream.Close();
            _serialPortStream.Dispose();
        }

        public void CopyMembersTo(IIoPort port)
        {
            if (port == null)
                port = new SerialPort();
            var sp = (SerialPort) port;
            sp.ComPort = ComPort;
            sp.BaudRate = BaudRate;
            sp.Parity = Parity;
            sp.StopBits = StopBits;
        }

        public string StopBitsString(StopBits val)
        {
            switch ((int) val)
            {
                case 0:
                    return "1";
                case 1:
                    return "1.5";
                case 2:
                    return "2";
                default:
                    throw new NotImplementedException("Invalid value for stop bits");
            }
        }

        public string ParityString(Parity val)
        {
            switch ((int) val)
            {
                case 0:
                    return "None";
                case 1:
                    return "Odd";
                case 2:
                    return "Even";
                case 3:
                    return "Mark";
                case 4:
                    return "Space";
                default:
                    throw new NotImplementedException("Invalid value for parity");
            }
        }

        public static ValidationResult BaudRateValidation(object obj, ValidationContext context)
        {
            var serialPort = (SerialPort)context.ObjectInstance;
            if (115200 % serialPort.BaudRate == 0)
                return ValidationResult.Success;

            return new ValidationResult("Invalid baud rate", new List<string> { "BaudRate" });
        }
    }
}
