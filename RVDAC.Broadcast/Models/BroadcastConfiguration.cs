﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;

namespace RVDAC.Broadcast.Models
{
    public class BroadcastConfiguration
    {
        public const string DefaultFileName = "RVDAC.Broadcast.Settings.xml";

        public BroadcastConfigurationIoPorts IoPorts { get; set; } = new BroadcastConfigurationIoPorts();
        public ObservableCollection<Message> Messages { get; set; } = new ObservableCollection<Message>();

        [XmlIgnore]
        public IEnumerable<IIoPort> AllIoPorts
        {
            get
            {
                foreach (var i in IoPorts.SerialPorts) yield return i;
                foreach (var i in IoPorts.UdpPorts) yield return i;
            }
        }

        public IIoPort GetPortByGuid(Guid guid)
        {
            //this returns null if the port doesn't exist
            IIoPort port = IoPorts.SerialPorts.FirstOrDefault(x => x.PortId.Equals(guid));
            if (port == null)
                port = IoPorts.UdpPorts.FirstOrDefault(x => x.PortId.Equals(guid));
            return port;
        }

        public bool SaveToFile(string fileName = DefaultFileName)
        {
            try
            {
                var serializer = new XmlSerializer(this.GetType());
                var writerSettings = new XmlWriterSettings
                {
                    Indent = true
                };
                using (var writer = XmlWriter.Create(AppDomain.CurrentDomain.BaseDirectory + "\\" + DefaultFileName, writerSettings))
                {
                    serializer.Serialize(writer, this);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public class BroadcastConfigurationIoPorts
        {
            public ObservableCollection<SerialPort> SerialPorts { get; set; } = new ObservableCollection<SerialPort>();
            public ObservableCollection<UdpPort> UdpPorts { get; set; } = new ObservableCollection<UdpPort>();
        }

    }
    
}
