﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using RVDAC.Common.Models;

namespace RVDAC.Broadcast.Models
{
    public class UdpPort: ValidatableModel, IIoPort
    {
        private UdpClient _udpClient;
        private IPEndPoint _endPoint;
        private int _portNo = 65531;
        private bool _ioInProgress;
        private string _ipAddress = "255.255.255.255";
        private object _udpSendLock = new object();

        public event EventHandler DataReceived;

        [Required]
        public Guid PortId { get; set; } = Guid.NewGuid();

        public string Name
        {
            get { return $"UDP-{IpAddress}:{PortNo}"; }
        }

        [Range(1, 65535)]
        public int PortNo {
            get { return _portNo; }
            set
            {
                SetProperty(ref _portNo, value);
                OnPropertyChanged(nameof(Name));
            }
        }

        [XmlIgnore]
        public bool IoInProgress
        {
            get { return _ioInProgress; }
            set
            {
                SetProperty(ref _ioInProgress, value);
            }
        }

        [Required]
        [CustomValidation(typeof (UdpPort), "IpAddressValidation")]
        public string IpAddress {
            get { return _ipAddress; }
            set
            {
                SetProperty(ref _ipAddress, value);
            }
        } 

        public void OpenPort()
        {
            _udpClient = new UdpClient();
            _endPoint = new IPEndPoint(IPAddress.Parse(IpAddress), PortNo);
            _udpClient.Connect(_endPoint);
        }

        public void ClosePort()
        {
            if (_udpClient.Client.Connected)
                _udpClient.Client.Close();
            _udpClient.Close();
        }

        public void SendData(object sender, byte[] data, bool async = false)
        {
            lock (_udpSendLock)
            {
                if (_udpClient == null || !_udpClient.Client.Connected)
                    OpenPort();
                if (async)
                {
                    var callback = new AsyncCallback(WriteComplete);
                    _udpClient.BeginSend(data, data.Length, callback, new object());
                    return;
                }
                _udpClient.Send(data, data.Length);
            }
        }

        private static void WriteComplete(IAsyncResult result)
        {

        }

        public void CopyMembersTo(IIoPort port)
        {
            if (port == null)
                port = new UdpPort();
            var udp = (UdpPort) port;
            udp.IpAddress = IpAddress;
            udp.PortNo = PortNo;
        }

        public bool PortAvailable()
        {
            return !System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties()
                .GetActiveUdpListeners()
                .Any(x => x.Port == PortNo);
        }

        public static ValidationResult IpAddressValidation(object obj, ValidationContext context)
        {
            var udpPort = (UdpPort)context.ObjectInstance;
            IPAddress ip;

            if (IPAddress.TryParse(udpPort.IpAddress, out ip))
                return ValidationResult.Success;

            return new ValidationResult("IP Address is not valid", new List<string> { "IpAddress" });
        }
    }
}
