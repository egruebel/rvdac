﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;

namespace RVDAC.RemoteMessenger.Client
{
    public class EndeavorNowStreamListener
    {
        public EndeavorNowStreamListener(int port)
        {
            Listener = new GenericTcpListener(port, OnClientConnected);
        }

        private readonly object _dictionaryLock = new object();

        private GenericTcpListener Listener { get; set; }

        private Dictionary<string, byte[]> MessageDictionary { get; set; } = new Dictionary<string, byte[]>();

        private void OnClientConnected(TcpClient client)
        {
            client.SendTimeout = 1000;
            client.ReceiveTimeout = 1000;
            
            try
            {
                using (var stream = client.GetStream())
                {
                    var receiveBuffer = new byte[1024];
                    var bytesReceived = stream.Read(receiveBuffer, 0, receiveBuffer.Length);
                    var streamNameRequested = Encoding.ASCII.GetString(receiveBuffer, 0, bytesReceived);
                    var messageToSend = GetMessage(streamNameRequested);
                    stream.Write(messageToSend, 0, messageToSend.Length);
                }
            }
            catch(Exception ex)
            {
                DatabaseRepository.AddError($"Error with EndeavorNow client. {ex.Message}.",nameof(OnClientConnected));
            }
        }

        public void AddMessage(MessengerExpandedMessage message)
        {
            lock (_dictionaryLock)
            {
                if (!MessageDictionary.ContainsKey(message.InstanceName))
                {
                    MessageDictionary.Add(message.InstanceName, SerializeMessage(message));
                    return;
                }
                //The value already exists
                MessageDictionary[message.InstanceName] = SerializeMessage(message);
            }
        }

        private byte[] GetMessage(string key)
        {
            lock (_dictionaryLock)
            {
                if (!MessageDictionary.ContainsKey(key))
                    return new byte[] { 0 };
                return MessageDictionary[key];
            }
        }

        private byte[] SerializeMessage(MessengerExpandedMessage message)
        {
            using (var mem = new MemoryStream())
            {
                Serializer.Serialize(mem, message);
                return mem.ToArray();
            }
        }
    }
}
