﻿
namespace RVDAC.RemoteMessenger.Client
{
    public class StreamController
    {
        public StreamController(string name, string logPath)
        {
            Name = name;
            LogFile = new MessengerLogFile(name, logPath);
        }

        public string Name { get; private set; }
        public MessengerLogFile LogFile { get; private set; }
    }
}
