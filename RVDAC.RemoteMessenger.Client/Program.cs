﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Xml.Linq;
using RVDAC.Common;
using RVDAC.Common.LogFile;

namespace RVDAC.RemoteMessenger.Client
{
    class Program
    {
        public MessengerClientSettings Settings { get; set; }
        public SensorConfiguration SensorConfig { get; set; }
        public RemoteMessengerStreamListener RemoteMessengerServer { get; set; }
        public EndeavorNowStreamListener EndeavorNowServer { get; set; }
        public List<StreamController> StreamControllers { get; set; } = new List<StreamController>();
        public EndeavorNowTestPlatform EndeavorNowTest { get; set; }

        static void Main(string[] args)
        {
            LogFile.AddError("Error1");
            LogFile.AddInfo("Info1");
            var p = new Program();
            var pThread = new Thread(p.Run); //p.Run will be our main loop
            pThread.Start();
        }

        public void Run()
        {
            var settingsFilePath = new MessengerClientSettings().FilePath;
            try
            {
                Settings = MessengerClientSettings.ReadSettingsFromFile(settingsFilePath);
            }
            catch
            {
                Console.WriteLine("Unable to load the MessengerClientSettings.xml file. Press 'Y' to create a new one.");
                var answer = Console.ReadKey();
                if (answer.Key != ConsoleKey.Y)
                {
                    Console.WriteLine("Unable to load settings. Application aborted.");
                    Environment.Exit(0);
                }
                Settings = new MessengerClientSettings();
                if(!Settings.SaveToFile(settingsFilePath))
                {
                    Console.WriteLine("Unable to load settings. Application aborted.");
                    Environment.Exit(0);
                }
            }

            if (TryLoadSensorConfiguration())
            {
                Console.WriteLine($"Loaded Configuration File: {SensorConfig.Guid}");
            }
            else
            {
                Console.WriteLine("Failed to load config file. Starting acquisition.");
                SensorConfig = new SensorConfiguration();
            }

            RemoteMessengerServer = new RemoteMessengerStreamListener(Settings.MessengerPortNumber, SensorConfig, TryInsertMessage);
            EndeavorNowServer = new EndeavorNowStreamListener(Settings.EndeavorNowPortNumber);

            Console.WriteLine($"Listening on TCP ports {Settings.MessengerPortNumber}, {Settings.EndeavorNowPortNumber}");
            while (true)
            {
                Thread.Sleep(50);
            }
        }

        private bool TryInsertMessage(MessengerExpandedMessage message)
        {
            if (message.IsConfigurationFile())
                return TryProcessNewSensorConfig(message);
            return TryProcessNewMessage(message);
        }

        private bool TryProcessNewSensorConfig(MessengerExpandedMessage message)
        {
            try
            {
                var xd = XDocument.Parse(message.ScsConfiguration);
                var cfg = new SensorConfiguration();
                var cfgBuilder = new ConfigImporter(xd, cfg);
                if (cfgBuilder.TryBuildFromExtendedScs())
                {
                    //generate a unique file name
                    var configArchivePath = $"{Settings.MessageFolderPath}\\config\\";
                    var configArchiveFile = $"config_{DateTime.Now.ToString("yyyy_MM_dd-HHmmssf")}.xml";
                    //Create directory if it doesn't exist
                    Directory.CreateDirectory(configArchivePath);

                    //Save in the root directory and the archive
                    ConfigExporter.CopyToFile(cfg, Settings.DefaultSensorConfigFilePath);
                    ConfigExporter.CopyToFile(cfg, configArchivePath + configArchiveFile);

                    //Start using the new configuration
                    Console.WriteLine($"New configuration file received with GUID: {cfg.Guid}");
                    RemoteMessengerServer.SensorConfig = cfg;
                    SensorConfig = cfg;

                    return true;
                }
                return false;
            }
            catch(Exception ex)
            {
                AddError(ex.Message);
                return false;
            }
        }

        public bool TryProcessNewMessage(MessengerExpandedMessage message)
        {
            try
            {
                if (!SensorConfig.IsInitialized)
                {
                    AddError("Rejected message due to no valid sensor configuration");
                    return false;
                }
                //Get the datetime of this message
                var dateTimeString = message.DateTime.ToString("yyyy_MM_dd_HH-mm-ss");
                Console.WriteLine($"Received a message generated on {dateTimeString} from the stream {message.InstanceName} ");

                //Find the tracker reference to this stream
                var controller = StreamControllers.FirstOrDefault(x => x.Name == message.InstanceName);
                if (controller == null)
                {
                    controller = new StreamController(message.InstanceName, Settings.MessageFolderPath);
                    StreamControllers.Add(controller);
                }
                
#if DEBUG
                //if (EndeavorNowTest == null)
                    //EndeavorNowTest = new EndeavorNowTestPlatform(Settings.EndeavorNowPortNumber, message.InstanceName, 800);

#endif 

                //Process the message
                EndeavorNowServer.AddMessage(message);
                controller.LogFile.WriteToFile(message);
                if (!message.IsRealTime)
                {
                    DatabaseRepository.AddMessage(message);
                }
                return true;
            }
            catch (Exception ex)
            {
                AddError(ex.Message);
                return false;
            }
        }

        bool TryLoadSensorConfiguration()
        {
            try
            {
                var configXml = XDocument.Load(Settings.DefaultSensorConfigFilePath);
                var config = new SensorConfiguration();
                var configBuilder = new ConfigImporter(configXml, config);
                if (configBuilder.TryBuildFromExtendedScs())
                {
                    SensorConfig = config;
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        public static void AddError(string message, [CallerMemberName] string propertyName = null)
        {
            Console.WriteLine(message);
            Console.WriteLine(propertyName);
            try
            {
                DatabaseRepository.AddError(message, propertyName);
            }
            catch(Exception ex)
            {
                //As a backup, write to a text file if the database doesn't work
                using (var writer = File.AppendText("Error_Log.txt"))
                {
                    writer.WriteLine($"{DateTime.Now}, {propertyName}, {message}, InnerException:{ex.Message}");
                }
            }
        }


    }
}
