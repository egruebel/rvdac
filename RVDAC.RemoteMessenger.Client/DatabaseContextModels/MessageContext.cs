﻿
using System.Data.Entity;


namespace RVDAC.RemoteMessenger.Client.DatabaseContextModels
{
    public class MessageContext: DbContext
    {
        public MessageContext(): base("MessengerDb")
        {

        }

        public DbSet<SensorData> SensorData { get; set; }
        public DbSet<DataPoint> DataPoints { get; set; }
        public DbSet<Connection> Connections { get; set; }
        public DbSet <Error> ErrorLog { get; set; }
    }
}
