﻿using System;

namespace RVDAC.RemoteMessenger.Client.DatabaseContextModels
{
    public class SensorData
    {
        public int Id { get; set; }
        public Guid SensorGuid { get; set; }
        public string SensorName { get; set; }
        public string SensorClass { get; set; }
        public int DataPointId { get; set; }
        public DateTime TimeStamp { get; set; }

        public double? Value { get; set; }
    }
}
