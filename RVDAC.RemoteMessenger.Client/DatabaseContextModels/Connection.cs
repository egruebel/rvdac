﻿using System;

namespace RVDAC.RemoteMessenger.Client.DatabaseContextModels
{
    public class Connection
    {
        public int Id { get; set; }
        public DateTime TimeStamp { get; set; }
        public string IpAddress { get; set; }
    }
}
