﻿using System;

namespace RVDAC.RemoteMessenger.Client.DatabaseContextModels
{
    public class Error
    {
        public int Id { get; set; }
        public DateTime TimeStamp { get; set; }
        public string ErrorLocation { get; set; }
        public string Message { get; set; }
    }
}
