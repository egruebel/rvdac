﻿using System;


namespace RVDAC.RemoteMessenger.Client.DatabaseContextModels
{
    public class DataPoint
    {
        public int Id { get; set; }
        public DateTime TimeStamp { get; set; }
        public DateTime TimeReceived { get; set; }
        public string StreamName { get; set; }
        public double DelayInSeconds { get; set; }
        public string IpAddress { get; set; }
    }
}
