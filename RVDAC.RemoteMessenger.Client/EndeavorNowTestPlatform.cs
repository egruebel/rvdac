﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ProtoBuf;

namespace RVDAC.RemoteMessenger.Client
{
    public class EndeavorNowTestPlatform
    {
        private string _streamName;
        private int _port;
        public EndeavorNowTestPlatform(int port, string streamName, int numberOfRequests)
        {
            //Client = new TcpClient();
            _port = port;
            _streamName = streamName;
            Task.Run(() =>
            {
                for (int i = 0; i < numberOfRequests; i++)
                {
                    var p = i;
                    Task.Run(() =>
                    {
                        BeginSending(p);
                    });
                }
                
            });

        }

        //private TcpClient Client { get; set; }

        private void BeginSending(int instanceNumber)
        {

            while (true)
            {
                var send = Encoding.ASCII.GetBytes(_streamName);
                using (var client = new TcpClient("localhost", _port))
                {
                    using (var net = client.GetStream())
                    {
                        net.Write(send, 0, send.Length);
                        var buffer = new byte[1024];
                        var message = Serializer.Deserialize<MessengerExpandedMessage>(net);
                        if(instanceNumber % 20 == 0)
                            Console.WriteLine($"EndeavorNow {instanceNumber}: " + message.DateTime);
                    }
                }
                Thread.Sleep(new Random().Next(30,1000));
            }
        }
    }
}
