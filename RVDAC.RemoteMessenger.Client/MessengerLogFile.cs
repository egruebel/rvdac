﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RVDAC.Common;
using RVDAC.Common.Messenger;

namespace RVDAC.RemoteMessenger.Client
{
    public class MessengerLogFile
    {
        private readonly string _streamName;
        private readonly string _logPath;
        
        public MessengerLogFile(string name, string logPath)
        {
            _streamName = name;
            _logPath = logPath;
        }

        private List<String> LastMessageSensorList { get; set; } = new List<String>(); 

        private int FileDay { get; set; }

        public void WriteToFile(MessengerExpandedMessage message)
        {
            //Verify nothing has changed in the header since the last write
            if (message.DataPoints.Count == LastMessageSensorList.Count)
            {
                for (int i = 0; i < LastMessageSensorList.Count; i++)
                {
                    if (message.DataPoints[i].SensorName != LastMessageSensorList[i])
                    {
                        StartNewFile(message);
                        break;
                    }
                }
            }
            //Make sure this isn't a new day
            if(FileDay != message.DateTime.DayOfYear)
                StartNewFile(message);

            //reset the last sensor list
            LastMessageSensorList = new List<string>();

            //make sure the directory exists
            var directory = Path.GetDirectoryName(FileName);
            if(directory != null)
                Directory.CreateDirectory(directory);

            using (var writer = File.AppendText(FileName))
            {
                writer.Write("\r\n" + message.DateTime.ToUniversalTime().ToString("O"));
                foreach (var item in message.DataPoints)
                {
                    writer.Write("," + item.Value);
                    LastMessageSensorList.Add(item.SensorName);
                }
            }
        }

        public string FileName { get; set; }

        private void StartNewFile(MessengerExpandedMessage message)
        {
            FileName = $"{_logPath}\\{_streamName}\\{_streamName}_{message.DateTime.ToString("yyyyMMddHHmmss")}.csv";
            FileDay = message.DateTime.DayOfYear;
            using (var writer = File.CreateText(FileName))
            {
                writer.Write("DateTime_ISO8601");
                foreach (var item in message.DataPoints)
                {
                    writer.Write("," + item.SensorName);
                }
            }
        }
    }
}
