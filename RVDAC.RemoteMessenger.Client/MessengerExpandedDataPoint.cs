﻿using System;
using System.Xml.Serialization;
using ProtoBuf;

namespace RVDAC.RemoteMessenger.Client
{
    [ProtoContract]
    public struct MessengerExpandedDataPoint
    {

        public MessengerExpandedDataPoint(Guid guid, string sensorName, string measurementClass, double? value)
        {
            SensorGuid = guid;
            SensorName = sensorName;
            SensorMeasurementClass = measurementClass;
            Value = value;
        }

        [ProtoMember(1)]
        public Guid SensorGuid { get; set; }

        [ProtoMember(2)]
        public string SensorName { get; set; }

        [ProtoMember(3)]
        public string SensorMeasurementClass { get; set; }

        [XmlElement("Value", IsNullable = false)]
        public string SerializableValue
        {
            get { return Value == null ? "" : Value.ToString(); }
            set { Value = Convert.ToDouble(value); }
        }

        [XmlIgnore]
        [ProtoMember(4)]
        public double? Value { get; set; }
    }
}
