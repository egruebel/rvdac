﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RVDAC.RemoteMessenger.Client
{
    class GenericTcpListener
    {
        private readonly TcpListener _listener;
        //public event EventHandler<TcpClient> ClientConnected;
        private readonly Action<TcpClient> _onNewClientConnected;

        public GenericTcpListener(int port, Action<TcpClient> onClientConnected)
        {
            _listener = new TcpListener(IPAddress.Any, port);
            _onNewClientConnected = onClientConnected;
            new Thread(ListenForClients).Start();
        }

        private void ListenForClients()
        {
            _listener.Start();
            while (true)
            {
                //blocks until a client has connected to the server
                TcpClient client = _listener.AcceptTcpClient();
                Task.Run(() => { _onNewClientConnected.Invoke(client); });
            }
        }
    }
}
