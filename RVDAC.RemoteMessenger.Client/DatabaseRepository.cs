﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RVDAC.RemoteMessenger.Client.DatabaseContextModels;

namespace RVDAC.RemoteMessenger.Client
{
    public static class DatabaseRepository
    {
        public static void AddError(string message, string location)
        {
            Console.WriteLine($"Error at {location}: {message}. ");
#if DEBUG
            return;
#endif
            Task.Run(() =>
            {
                using (var db = new MessageContext())
                {
                    var error = new Error()
                    {
                        ErrorLocation = location,
                        Message = message,
                        TimeStamp = DateTime.Now
                    };
                    db.ErrorLog.Add(error);
                    db.SaveChanges();
                }
            });
        }

        public static void AddMessage(MessengerExpandedMessage msg)
        {
#if DEBUG
            return;
#endif    
            using (var db = new MessageContext())
            {
                var m = new DataPoint()
                {
                    TimeStamp = msg.DateTime,
                    TimeReceived = msg.ReceivedDateTime,
                    StreamName = msg.InstanceName,
                    DelayInSeconds = (msg.ReceivedDateTime - msg.DateTime).TotalSeconds,
                    IpAddress = msg.IpAddress
                };
                db.DataPoints.Add(m);
                db.SaveChanges();

                foreach (var d in msg.DataPoints)
                {
                    var dp = new SensorData()
                    {
                        SensorClass = d.SensorMeasurementClass,
                        SensorGuid = d.SensorGuid,
                        SensorName = d.SensorName,
                        DataPointId = m.Id,
                        TimeStamp = m.TimeStamp,
                        Value = d.Value
                    };
                    db.SensorData.Add(dp);
                }
                db.SaveChanges();
            }
        }

        public static void AddConnection(Connection connection)
        {
#if DEBUG
            return;
#endif
            Task.Run(() =>
            {
                using (var db = new MessageContext())
                {

                    db.Connections.Add(connection);
                    db.SaveChanges();
                }
            });
        }
    }
}
