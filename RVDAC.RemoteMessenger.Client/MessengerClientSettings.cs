﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace RVDAC.RemoteMessenger.Client
{
    public class MessengerClientSettings
    {
        private const string DefaultFileName = "RVDAC.Messenger.Client.Settings.xml";

        public int MessengerPortNumber { get; set; } = 13101;

        public int EndeavorNowPortNumber { get; set; } = 13102;

        public string MessageFolderPath { get; set; } = AppDomain.CurrentDomain.BaseDirectory + "messages";

        public string DefaultSensorConfigFilePath { get; set; } = AppDomain.CurrentDomain.BaseDirectory + "Sensor.xml";

        [XmlIgnore]
        public string FilePath { get; } = AppDomain.CurrentDomain.BaseDirectory + DefaultFileName;

        public bool SaveToFile(string filePath)
        {
            try
            {
                var serializer = new XmlSerializer(GetType());
                var writerSettings = new XmlWriterSettings
                {
                    Indent = true
                };
                using (var writer = XmlWriter.Create(filePath, writerSettings))
                {
                    serializer.Serialize(writer, this);
                }
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public static MessengerClientSettings ReadSettingsFromFile(string filePath)
        {
            var serializer = new XmlSerializer(typeof(MessengerClientSettings));

            using (var reader = XmlReader.Create(filePath))
            {
                var config = (MessengerClientSettings)serializer.Deserialize(reader);
                return config;
            }
        }
    }
}
