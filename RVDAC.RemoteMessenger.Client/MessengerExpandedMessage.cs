﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProtoBuf;
using RVDAC.Common;
using RVDAC.Common.Messenger;


namespace RVDAC.RemoteMessenger.Client
{
    [ProtoContract]
    public class MessengerExpandedMessage
    {
        public MessengerExpandedMessage()
        {
            
        }

        public MessengerExpandedMessage(MessengerMessage fromMessage, SensorConfiguration config, string ip)
        {
            DateTime = fromMessage.DateTime;
            ReceivedDateTime = DateTime.Now;
            DelaySeconds = (ReceivedDateTime - DateTime).TotalSeconds;
            InstanceName = fromMessage.InstanceName;
            ScsConfiguration = fromMessage.ScsConfiguration;
            IpAddress = ip;
            IsRealTime = fromMessage.IsRealTime;
            DataPoints = new List<MessengerExpandedDataPoint>();
            if (fromMessage.DataPoints == null)
                return;
            foreach (var dp in fromMessage.DataPoints)
            {
                var matchingSensor = config.AllChildSensors().FirstOrDefault(x => x.Id == dp.SensorId);
                if (matchingSensor != null)
                {
                    DataPoints.Add(new MessengerExpandedDataPoint
                    {
                        SensorGuid = matchingSensor.Guid,
                        Value = dp.Value,
                        SensorMeasurementClass = matchingSensor.Measurement,
                        SensorName = matchingSensor.Name

                    });
                }
            }
        }

        [ProtoMember(1)]
        public DateTime DateTime { get; set; }

        [ProtoMember(2)]
        public DateTime ReceivedDateTime { get; set; }

        [ProtoMember(3)]
        public string InstanceName { get; set; }

        [ProtoMember(4)]
        public string IpAddress { get; set; }

        [ProtoMember(5)]
        public double DelaySeconds { get; set; }

        [ProtoMember(6)]
        public string ScsConfiguration { get; set; } = "";

        [ProtoMember(7)]
        public List<MessengerExpandedDataPoint> DataPoints { get; set; }

        [ProtoMember(8)]
        public bool IsRealTime { get; set; }

        public bool IsConfigurationFile()
        {
            return !string.IsNullOrEmpty(ScsConfiguration);
        }

    }
}
