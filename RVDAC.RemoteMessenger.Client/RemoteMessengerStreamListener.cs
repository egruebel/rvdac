﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using RVDAC.Common;
using RVDAC.Common.Messenger;
using RVDAC.RemoteMessenger.Client.DatabaseContextModels;

namespace RVDAC.RemoteMessenger.Client
{
    public class RemoteMessengerStreamListener
    {
        private const int MaxMessageSizeBytes = 1000 * 800; //800 kb
        private readonly Func<MessengerExpandedMessage, bool> _tryProcessMessage;
       

        public RemoteMessengerStreamListener(int port, SensorConfiguration sensorConfig, Func<MessengerExpandedMessage, bool> tryProcessMessage  )
        {
            SensorConfig = sensorConfig;
            Listener = new GenericTcpListener(port, OnClientConnected);
            _tryProcessMessage = tryProcessMessage;
        }

        private GenericTcpListener Listener { get; set; }

        public SensorConfiguration SensorConfig { get; set; }

        private void OnClientConnected(TcpClient client)
        {
            client.ReceiveTimeout = 8000;
            client.SendTimeout = 8000;
            try
            {
                using (client)
                {
                    client.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);
                    var netStream = client.GetStream();
                    var connectString = $"*{SensorConfig.Guid.ToString()}* RVDAC.RemoteMessenger\r\n";
                    var connectMessage = Encoding.ASCII.GetBytes(connectString);
                    //say hello
                    netStream.Write(connectMessage, 0, connectMessage.Length);
                    //if you're here it means that the connection has been made
                    DatabaseRepository.AddConnection(new Connection() {IpAddress=client.Client.RemoteEndPoint.ToString(),TimeStamp = DateTime.Now});
                    var socketTimeout = new Stopwatch();
                    socketTimeout.Start();
                    while (client.Client.Connected)
                    {
                        if (netStream.DataAvailable)
                        {
                            
                            socketTimeout = new Stopwatch();
                            socketTimeout.Start();
                            var message = ReadFromNetworkStream(client);
                            if (message == null)
                            {
                                //message could not be deserialized, it doesn't matter if the server gets a response
                                TrySendResponse(client, false);
                            }
                            else
                            {
                                ProcessMessage(message, client);
                            }
                            
                        }
                        if (socketTimeout.ElapsedMilliseconds > (1000 * 60 * 60))
                        {
                            //no data received for over an hour
                            DatabaseRepository.AddError("No socket activity for 1 hour", nameof(OnClientConnected));
                            return;
                        }
                        Thread.Sleep(5);
                    }
                    DatabaseRepository.AddError("Client disconnected", nameof(OnClientConnected));
                }
            }
            catch (Exception ex)
            {
                DatabaseRepository.AddError(ex.Message, nameof(OnClientConnected));
            }
        }

        private void ProcessMessage(MessengerMessage message, TcpClient client)
        {
            //message is valid
            //try convert to expanded message
            var messageProcessed = false;
            var expandedMessage = TryBuildExpandedMessage(message, SensorConfig,
                client.Client.RemoteEndPoint.ToString());
            if (expandedMessage != null && _tryProcessMessage(expandedMessage))
            {
                messageProcessed = true;
            }

            if (!message.IsRealTime)
                TrySendResponse(client, messageProcessed);
        }

        private static MessengerMessage ReadFromNetworkStream(TcpClient client)
        {
            var buffer = new byte[MaxMessageSizeBytes];
            var delim = MessengerSerializer.Delimiter;
            var netStream = client.GetStream();
            netStream.ReadTimeout = 5000;

            try
            {
                int bytesRead = 0;
                int indexOfDelimiter = -1;
                int consecutiveReads = 0;
                while (indexOfDelimiter == -1)
                {
                    consecutiveReads++;
                    bytesRead += netStream.Read(buffer, bytesRead, buffer.Length - bytesRead);
                    indexOfDelimiter = IndexOfDelimiter(buffer, delim);
                    if (consecutiveReads > 3)
                        break;
                }

                var endOfMessage = bytesRead - delim.Length;
                var messageStartsWithGzipIds = (buffer[0] == 31 && buffer[1] == 139);
                var messageEndsWithDelimiter = indexOfDelimiter == endOfMessage;
                if (messageStartsWithGzipIds && messageEndsWithDelimiter)
                {
                    var message = new byte[endOfMessage];
                    Array.Copy(buffer, message, message.Length);
                    return DecodeMessage(message);
                }
                //If you're here it means that data was received, but not readable.
                return null;
                //The method exits and we'll try again on the next networkStream.DataAvailable
            }
            catch (Exception ex)
            {
                DatabaseRepository.AddError(ex.Message, nameof(ReadFromNetworkStream));
                return null;
            }
        }

        private static MessengerExpandedMessage TryBuildExpandedMessage(MessengerMessage message, SensorConfiguration config,
            string ip)
        {
            try
            {
                return new MessengerExpandedMessage(message, config, ip);
            }
            catch(Exception ex)
            {
                DatabaseRepository.AddError("Error expanding message. " + ex.Message, nameof(TryBuildExpandedMessage));
                return null;
            }
        }

        private static MessengerMessage DecodeMessage(byte[] data)
        {
            try
            {
                var message = MessengerSerializer.Deserialize(data);
                if (message == null)
                    throw new InvalidDataException("Unable to deserialize message.");
                return message;
            }
            catch
            {
                return null;
            }
        }

        private bool TrySendResponse(TcpClient client, bool success)
        {
            try
            {
                byte x = success ? (byte)10 : (byte)20;
                client.GetStream().Write(new byte[] { x }, 0, 1);
                return true;
            }
            catch(Exception ex)
            {
                DatabaseRepository.AddError(ex.Message, nameof(TrySendResponse));
                return false;
            }
        }

        private static int IndexOfDelimiter(byte[] array, byte[] delim)
        {
            //var delim = MessengerSerializer.Delimiter;
            for (int i = 0; i <= (array.Length - delim.Length); i++)
            {
                if (array[i] == delim[0])
                {
                    var found = true;
                    for (int j = 1; j < delim.Length; j++)
                    {
                        if (array[i + j] != delim[j])
                        {
                            found = false;
                            break;
                        }
                    }
                    if (found)
                        return i;
                }
            }
            return -1;
        }
    }
}
