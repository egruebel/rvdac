﻿using System.Windows.Input;
using RVDAC.Common;
using RVDAC.Logger.Models;
using RVDAC.UI;

namespace RVDAC.Logger.ViewModels
{
    public class SensorProfileEditorViewModel: ViewModelBase
    {
        public SensorProfileEditorViewModel()
        {
            
        }

        public SensorProfileEditorViewModel(SensorProfile sensorProfile)
        {
            //Populate viewmodel
            SensorProfileReference = sensorProfile;
            SensorProfileEditable = new SensorProfile();
            CopyProfile(SensorProfileReference, SensorProfileEditable);
        }

        public SensorProfile SensorProfileEditable { get; set; } = new SensorProfile();

        private SensorProfile SensorProfileReference { get; set; }

        public SensorConfiguration SensorConfig { get; set; }

        public void CopyProfile(SensorProfile from, SensorProfile to)
        {
            to.Name = from.Name;
            to.ProfileGuid = from.ProfileGuid;
            to.Sensors.Clear();
            foreach (var guid in from.Sensors)
            {
                to.Sensors.Add(guid);
            }
        }

        public ICommand SaveCommand
        {
            get { return new RelayCommand(SaveExecute, CanSave); }
        }

        private bool CanSave()
        {
            return !SensorProfileEditable.HasErrors;
        }

        private void SaveExecute()
        {
            SensorProfileEditable.Validate();
            if (SensorProfileEditable.HasErrors)
                return;

            CopyProfile(SensorProfileEditable, SensorProfileReference);
            CloseMethod = FormCloseRoute.Submit;
            Close();
        }

        public ICommand CancelCommand
        {
            get { return new RelayCommand(CancelExecute, CanCancel); }
        }

        private bool CanCancel()
        {
            return true;
        }

        private void CancelExecute()
        {
            CloseMethod = FormCloseRoute.Cancel;
            Close();
        }
    }
}
