﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Xml;
using System.Xml.Serialization;
using RVDAC.Common;
using RVDAC.Common.ScsClient;
using RVDAC.Logger.Models;
using RVDAC.Logger.Views;
using RVDAC.UI;

namespace RVDAC.Logger.ViewModels
{
    public class MainWindowViewModel: ViewModelBase
    {
        private string _scsServerIp = "127.0.0.1";

        public ScsClient ScsClient { get; set; }

        public LoggerConfiguration LoggerConfiguration { get; set; }

        private LogFileController LogFileController { get; }

        public bool HasActivity { get; private set; }

        public MainWindowViewModel()
        {
#if DEBUG
            //Stops Visual Studio from running this code in the Designer Window
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject())) return;
            _scsServerIp = ScsClient.DebugScsServerIp;
#endif
            ScsClient = new ScsClient(_scsServerIp, 505);
            //Poll every second
            ScsClient.LockPollingTimeToClock();
            //Bind client events
            ScsClient.ErrorReceived += ScsClient_ErrorReceived;
            ScsClient.MessageReceived += ScsClient_MessageReceived;
            ScsClient.SensorConfigurationReceived += ScsClient_SensorConfigurationReceived;
            //Load client stream configurations
            LoggerConfiguration = ReadConfigFromFile();
            //Build the file controller
            LogFileController = new LogFileController(LoggerConfiguration);
            //Begin ACQ
            Task.Run(() => ScsClient.BeginAcquisition(1000));
        }

        private static LoggerConfiguration ReadConfigFromFile()
        {
            var serializer = new XmlSerializer(typeof (Models.LoggerConfiguration));
            try
            {
                using (var reader = XmlReader.Create(AppDomain.CurrentDomain.BaseDirectory + "\\" + LoggerConfiguration.DefaultFileName))
                {
                    var config = (LoggerConfiguration)serializer.Deserialize(reader);
                    //Cross reference the sensor profiles
                    foreach (var logFile in config.LogFiles)
                    {
                        var sensorProfile =
                            config.SensorProfiles.FirstOrDefault(x => x.ProfileGuid.Equals(logFile.SensorProfileGuid));
                        if (sensorProfile != null)
                            logFile.SensorProfile = sensorProfile;
                    }
                    return config;
                }
            }
            catch (Exception e)
            {
                var msg =
                    string.Format(
                        "Unable to load the RVDAC.Logger configuration file. {0}. Would you like to create a new one?",
                        e.Message);
                var iom = MessageBox.Show(msg, "Config Error", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                if (iom == MessageBoxResult.Yes)
                {
                    var config = new LoggerConfiguration();
                    config.SaveToFile();
                    return config;
                }
                return new LoggerConfiguration();
            }
        }

        private void ScsClient_SensorConfigurationReceived(object sender, EventArgs e)
        {
            
            //Start logging
            LogFileController.StartLogging(ScsClient);
        }

        private void ScsClient_MessageReceived(object sender, ScsClientMessageReceivedEventArgs e)
        {

        }

        private void ScsClient_ErrorReceived(object sender, ScsClientErrorReceivedEventArgs e)
        {
            
        }

        public ICommand AddNewLogFileCommand
        {
            get { return new RelayCommand(AddNewLogFileExecute, CanAddNewLogFile); }
        }

        private bool CanAddNewLogFile()
        {
            return true;
        }

        private void AddNewLogFileExecute()
        {
            var window = new LogFileEditor();
            var model = new LogFile();
            LoggerConfiguration.LogFiles.Add(model);
            var viewModel = new LogFileEditorViewModel(model, LoggerConfiguration)
            {
                Close = window.Close,
            };
            window.DataContext = viewModel;
            window.ShowDialog();
            //Dialog has closed
            if (viewModel.CloseMethod == FormCloseRoute.Submit)
            {
                LoggerConfiguration.SaveToFile();
                LogFileController.StartLogging(model);
                return;
            }
            //Dialog was cancelled
            LoggerConfiguration.LogFiles.Remove(model);
        }

        public ICommand ResetContinuousFilesCommand
        {
            get { return new RelayCommand(ResetContinuousFilesExecute, CanResetContinuousFiles); }
        }

        private bool CanResetContinuousFiles()
        {
            return LoggerConfiguration.LogFiles.Any(x => x.FileResetHours <= 0);
        }

        private void ResetContinuousFilesExecute()
        {
            var filesToReset = LoggerConfiguration.LogFiles.Where(x => x.FileResetHours <= 0);
            foreach (var logFile in filesToReset)
            {
                logFile.TriggerNewFile();
            }
        }

        public ICommand SensorProfilesWindowCommand
        {
            get { return new RelayCommand(SensorProfilesWindowExecute, CanShowSensorProfiles); }
        }

        private bool CanShowSensorProfiles()
        {
            return true;
        }

        private void SensorProfilesWindowExecute()
        {
            var viewModel = new SensorProfilesWindowViewModel(LoggerConfiguration);
            var window = new SensorProfiles()
            {
                DataContext = viewModel
            };
            viewModel.Close = window.Close;
            window.ShowDialog();
        }

        public ICommand EditLogFileCommand
        {
            get { return new RelayCommand<LogFile>(EditLogFileExecute, CanEditLogFile);}
        }

        private bool CanEditLogFile(LogFile model)
        {
            return true;
        }

        private void EditLogFileExecute(LogFile model)
        {
            var window = new LogFileEditor();
            var viewModel = new LogFileEditorViewModel(model, LoggerConfiguration)
            {
                Close = window.Close,
            };
            window.DataContext = viewModel;
            window.ShowDialog();
            //Dialog has closed
            if (viewModel.CloseMethod == FormCloseRoute.Submit)
            {
                LoggerConfiguration.SaveToFile();
                LogFileController.StartLogging(model);
            }
            //Dialog was cancelled
        }

        public ICommand DeleteLogFileCommand
        {
            get { return new RelayCommand<LogFile>(DeleteLogFileExecute, CanDeleteLogFile); }
        }

        private bool CanDeleteLogFile(LogFile model)
        {
            return true;
        }

        private void DeleteLogFileExecute(LogFile model)
        {
            var confirmation = MessageBox.Show("Are you sure?", "Delete Log File", MessageBoxButton.YesNo,
                MessageBoxImage.Asterisk);
            if (confirmation == MessageBoxResult.Yes)
            {
                LoggerConfiguration.LogFiles.Remove(model);
                LoggerConfiguration.SaveToFile();
            }
        }
    }
}
