﻿using System.Windows.Forms;
using System.Windows.Input;
using RVDAC.Common;
using RVDAC.Logger.Models;
using RVDAC.UI;

namespace RVDAC.Logger.ViewModels
{
    public class LogFileEditorViewModel: ViewModelBase
    {
        public LogFileEditorViewModel(LogFile logFile, LoggerConfiguration loggerConfig)
        {
            LogFileReference = logFile;
            LogFileEditable = new LogFile();
            CopyLogFile(LogFileReference, LogFileEditable);
            LoggerConfig = loggerConfig;
        }

        public LogFileEditorViewModel() { }

        public LogFile LogFileEditable { get; set; } = new LogFile();

        private LogFile LogFileReference { get; set; } = new LogFile();

        public LoggerConfiguration LoggerConfig { get; private set; } = new LoggerConfiguration();

        private void CopyLogFile(LogFile from, LogFile to)
        {
            to.Name = from.Name;
            to.Description = from.Description;
            to.FileResetHours = from.FileResetHours;
            to.Path = from.Path;
            to.PollingIntervalSeconds = from.PollingIntervalSeconds;
            to.SensorProfile = from.SensorProfile;
        }

        public ICommand SaveCommand
        {
            get { return new RelayCommand(SaveCommandExecute, CanSave); } 
        }

        private bool CanSave()
        {
            return !LogFileEditable.HasErrors;
        }

        private void SaveCommandExecute()
        {
            LogFileEditable.Validate();
            if (LogFileEditable.HasErrors)
                return;
            CloseMethod = FormCloseRoute.Submit;
            CopyLogFile(LogFileEditable, LogFileReference);
            Close();
        }

        public ICommand CancelCommand
        {
            get { return new RelayCommand(CancelCommandExecute, CanCancel); }
        }

        private bool CanCancel()
        {
            return true;
        }

        private void CancelCommandExecute()
        {
            CloseMethod = FormCloseRoute.Cancel;
            Close();
        }

        public ICommand SelectPathCommand
        {
            get { return new RelayCommand(SelectPathCommandExecute, CanSelectPath); }
        }

        private bool CanSelectPath()
        {
            return true;
        }

        private void SelectPathCommandExecute()
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog()
            {
                ShowNewFolderButton = true
            };
            var result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                LogFileEditable.Path = dialog.SelectedPath;
            }

        }
    }
}
