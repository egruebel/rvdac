﻿using System.Windows;
using System.Windows.Input;
using RVDAC.Common;
using RVDAC.Logger.Models;
using RVDAC.Logger.Views;
using RVDAC.UI;

namespace RVDAC.Logger.ViewModels
{
    public class SensorProfilesWindowViewModel: ViewModelBase
    {
        public SensorProfilesWindowViewModel()
        {
            
        }

        public SensorProfilesWindowViewModel(LoggerConfiguration loggerConfig)
        {
            LoggerConfig = loggerConfig;
        }

        public LoggerConfiguration LoggerConfig { get; set; } = new LoggerConfiguration();

        public ICommand AddNewSensorProfileCommand
        {
            get { return new RelayCommand(AddNewSensorProfileExecute, CanAddNewSensorProfile); }
        }

        private bool CanAddNewSensorProfile()
        {
            return true;
        }

        private void AddNewSensorProfileExecute()
        {
            var model = new SensorProfile();
            var viewModel = new SensorProfileEditorViewModel(model);
            LoggerConfig.SensorProfiles.Add(model);
            var window = new SensorProfileEditor()
            {
                DataContext = viewModel
            };
            viewModel.Close = window.Close;
            window.ShowDialog();
            //form has closed
            if (viewModel.CloseMethod == FormCloseRoute.Submit)
            {
                LoggerConfig.SaveToFile();
                return;
            }
            //user cancelled
            LoggerConfig.SensorProfiles.Remove(model);
        }

        public ICommand EditSensorProfileCommand
        {
            get { return new RelayCommand<SensorProfile>(EditSensorProfileExecute, CanEditSensorProfile);}
        }

        private bool CanEditSensorProfile(SensorProfile model)
        {
            return true;
        }

        private void EditSensorProfileExecute(SensorProfile model)
        {
            var viewModel = new SensorProfileEditorViewModel(model);
            var window = new SensorProfileEditor()
            {
                DataContext = viewModel
            };
            viewModel.Close = window.Close;
            window.ShowDialog();
            //form has closed
            if (viewModel.CloseMethod == FormCloseRoute.Submit)
            {
                LoggerConfig.SaveToFile();
                return;
            }
        }

        public ICommand DeleteSensorProfileCommand
        {
            get { return new RelayCommand<SensorProfile>(DeleteSensorProfileExecute, CanDeleteSensorProfile); }
        }

        private bool CanDeleteSensorProfile(SensorProfile model)
        {
            foreach (var file in LoggerConfig.LogFiles)
            {
                if (file.SensorProfile == null)
                    continue;
                if (file.SensorProfile.Equals(model))
                    return false;
            }
            return true;
        }

        private void DeleteSensorProfileExecute(SensorProfile model)
        {
            var result = MessageBox.Show("Are you sure?", "Delete Profile", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                LoggerConfig.SensorProfiles.Remove(model);
                LoggerConfig.SaveToFile();
            }
        }
    }
}
