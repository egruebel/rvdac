﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using RVDAC.Common;
using RVDAC.Common.Models;

namespace RVDAC.Logger.Models
{
    public class SensorProfile: ValidatableModel
    {
        private string _name;

        public Guid ProfileGuid { get; set; } = Guid.NewGuid();

        [XmlIgnore]
        public int Count
        {
            get { return Sensors.Count; }
        }

        [Required]
        public string Name
        {
            get { return _name;}
            set { SetProperty(ref _name, value); }
        }

        public ObservableCollection<SensorCollectionItem> Sensors { get; set; } = new ObservableCollection<SensorCollectionItem>();

    }
}
