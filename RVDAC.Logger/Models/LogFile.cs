﻿using System;
using System.Xml.Serialization;
using RVDAC.Common.Models;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using RVDAC.Common;
using RVDAC.Common.ScsClient;
using Timer = System.Timers.Timer;

namespace RVDAC.Logger.Models
{
    public class LogFile: ValidatableModel
    {
        private readonly Timer _countdownToDatapoint = new Timer(600);
        public event EventHandler<DateTime> ReadyForDataPoint;
        public event EventHandler<string> LogError;

        private string _name, _path, _description;
        private int _pollingIntervalSeconds = 60, _fileResetHours = 24;
        private uint _fileLineCount ;
        private DateTime _nextDatapoint, _nextFile;
        private SensorProfile _sensorProfile;
        private bool _status, _activity;

        public LogFile()
        {
            _countdownToDatapoint.Elapsed += _countdown_Elapsed;
        }

        [Required]
        public string Name
        {
            get { return _name; }
            set
            {
                SetProperty(ref _name, value);
                TriggerNewFile();
            }
        }

        [Required]
        public string Path
        {
            get { return _path; }
            set
            {
                SetProperty(ref _path, value);
                TriggerNewFile();
            }
        }

        [Required]
        public string Description
        {
            get { return _description; }
            set { SetProperty(ref _description, value); }
        }

        [Required]
        [Range(1, 3600)]
        public int PollingIntervalSeconds
        {
            get { return _pollingIntervalSeconds; }
            set
            {
                if (value < 1)
                    value = 1;
                SetProperty(ref _pollingIntervalSeconds, value);
                ResetCountdownTimer();
            }
        } 

        [Required]
        [Range(0, 720)]
        public int FileResetHours
        {
            get { return _fileResetHours; }
            set
            {
                SetProperty(ref _fileResetHours, value);
                if (value <= 0)
                {
                    NextFile = new DateTime();
                    return;
                }
                ResetFileDate();
            }
        }

        public Guid SensorProfileGuid { get; set; } //This GUID is saved in the XML config file so we don't have redundant collections

        [XmlIgnore]
        public SensorProfile SensorProfile
        {
            get { return _sensorProfile; }
            set
            {
                SetProperty(ref _sensorProfile, value);
                if (value != null)
                {
                    SensorProfileGuid = value.ProfileGuid;
                    SensorProfile.Sensors.CollectionChanged -= Sensors_CollectionChanged;
                    SensorProfile.Sensors.CollectionChanged += Sensors_CollectionChanged;
                }
                    
            }
        }

        [XmlIgnore]
        public uint FileLineCount
        {
            get { return _fileLineCount; }
            set
            {
                if (value < int.MaxValue)
                    SetProperty(ref _fileLineCount, value);
            }
        }

        [XmlIgnore]
        public bool Activity
        {
            get { return _activity;}
            set
            {
                Task.Run(() =>
                {
                    if (value == true)
                    {
                        SetProperty(ref _activity, value);
                        Thread.Sleep(100);
                    }
                    SetProperty(ref _activity, false);
                });
            }
        }

        [XmlIgnore]
        public bool IsLogging
        {
            get { return _status;}
            set { SetProperty(ref _status, value); }
        }

        [XmlIgnore]
        public DateTime NextDatapoint
        {
            get { return _nextDatapoint; }
            set { SetProperty(ref _nextDatapoint, value); }
        }

        [XmlIgnore]
        public DateTime NextFile
        {
            get { return _nextFile; }
            set { SetProperty(ref _nextFile, value); }
        }

        [XmlIgnore]
        public FileWriter ActiveFile { get; set; }

        private void _countdown_Elapsed(object sender, ElapsedEventArgs e)
        {
            ReadyForDataPoint?.Invoke(this, NextDatapoint);
            ResetCountdownTimer();
        }

        private void Sensors_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            TriggerNewFile();
        }

        public void TriggerNewFile()
        {
            ActiveFile = null;
        }

        public void Start()
        {
            ResetCountdownTimer();
        }

        public void Stop()
        {
            _countdownToDatapoint.Stop();
        }

        public void WriteToFile(SensorValueHolder[] values, DateTime timestamp, SensorConfiguration sensorConfig)
        {
            try
            {
                if (this.SensorProfile.Sensors.Count == 0)
                    return;
                Activity = true;
                if (ActiveFile == null) 
                {
                    //The application has just started, or a change to the logfile settings has forced a new file
                    ActiveFile = new FileWriter();
                    StartNewFile(timestamp, sensorConfig);
                }
                var dif = (NextFile - timestamp).TotalSeconds;
                if (dif <= 0 && FileResetHours > 0)
                {
                    //Time for a new file
                    ResetFileDate();
                    StartNewFile(timestamp, sensorConfig);
                }
                if (!sensorConfig.Guid.Equals(ActiveFile.SensorConfigGuid))
                {
                    //Sensor configuration has changed
                    StartNewFile(timestamp, sensorConfig);
                }
                //Log here
                ActiveFile.TryWriteToFile(timestamp, values, this);
                FileLineCount ++;
                IsLogging = true;
            }
            catch(Exception ex)
            {
                LogError?.Invoke(this, ex.Message);
                if(new Random().Next(0,100) > 80)
                    Common.LogFile.LogFile.AddError($"Error writing to file. {ex.Message}.");
                IsLogging = false;
            }
        }

        private void StartNewFile(DateTime timestamp, SensorConfiguration sensorConfig)
        {
            try
            {
                ActiveFile.TryCreateNewFile(timestamp, sensorConfig, this);
                IsLogging = true;
                FileLineCount = 0;
            }
            catch(Exception ex)
            {
                LogError?.Invoke(this, ex.Message);
                Common.LogFile.LogFile.AddError($"Error creating file. {ex.Message}.");
                IsLogging = false;
            }
        }

        public async void ResetCountdownTimer()
        {
            var nextDatapoint = new DateTime();
            await Task.Run(() =>
            {
                
                if (PollingIntervalSeconds > 60)
                {
                    _countdownToDatapoint.Interval =
                        SyncronousTimer.GetNextIntervalSpan(PollingIntervalSeconds/60,
                            SyncronousTimer.TimeSpanInterval.Minutes, out nextDatapoint).TotalMilliseconds;
                }
                else
                {
                    _countdownToDatapoint.Interval = SyncronousTimer.GetNextIntervalSpan(PollingIntervalSeconds,
                        SyncronousTimer.TimeSpanInterval.Seconds, out nextDatapoint).TotalMilliseconds;
                }
            });
            NextDatapoint = nextDatapoint;
            _countdownToDatapoint.Start();
        }

        public void ResetFileDate()
        {
            NextFile = SyncronousTimer.GetNextIntervalDate(FileResetHours, SyncronousTimer.TimeSpanInterval.Hours);
        }
    }
}
