﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RVDAC.Common.ScsClient;

namespace RVDAC.Logger.Models
{
    public class LogFileController
    {
        private object _sensorListLock = new object();

        public LogFileController(LoggerConfiguration loggerConfig)
        {
            LoggerConfig = loggerConfig;
        }

        private ScsClient ScsClient { get; set; }

        private SensorValueHolder[] LatestSensorValues { get; set; }

        private LoggerConfiguration LoggerConfig { get; set; }

        public void StartLogging(ScsClient scsClient)
        {
            ScsClient = scsClient;
            //scsClient.SensorConfigurationReceived -= ScsClient_SensorConfigurationReceived;
            //scsClient.SensorConfigurationReceived += ScsClient_SensorConfigurationReceived;
            scsClient.MessageReceived -= ScsClient_MessageReceived;
            scsClient.MessageReceived += ScsClient_MessageReceived;

            foreach (var log in LoggerConfig.LogFiles)
            {
                log.ReadyForDataPoint -= Log_ReadyForDataPoint;
                log.ReadyForDataPoint += Log_ReadyForDataPoint;
                log.Start();
            }
        }

        public void StartLogging(LogFile log)
        {
            log.ReadyForDataPoint -= Log_ReadyForDataPoint;
            log.ReadyForDataPoint += Log_ReadyForDataPoint;
            log.Start();
        }

        public void StopLogging()
        {
            foreach (var log in LoggerConfig.LogFiles)
            {
                log.Stop();
            }
        }

        private void Log_ReadyForDataPoint(object sender, DateTime timeStamp)
        {
            var logFile = (LogFile) sender;
            if (LatestSensorValues == null)
                return;
            Task.Run(() =>
            {
                var svCopy = new SensorValueHolder[LatestSensorValues.Length];
                lock (_sensorListLock)
                {
                    Array.Copy(LatestSensorValues, svCopy, LatestSensorValues.Length);
                }
                logFile.WriteToFile(svCopy, timeStamp, ScsClient.SensorConfig);
            });
        }

        private void ScsClient_MessageReceived(object sender, ScsClientMessageReceivedEventArgs e)
        {
            if (e.MessageType == ScsClientSocket.ScsMessageType.SensorData)
            {
                lock (_sensorListLock)
                {
                    LatestSensorValues = ScsClient.SensorConfig.CopyOfSensorValues();
                }
            }
        }
    }
}
