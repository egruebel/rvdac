﻿using System;
using System.Collections.ObjectModel;
using System.Xml;
using System.Xml.Serialization;
using RVDAC.Common;

namespace RVDAC.Logger.Models
{
    public class LoggerConfiguration
    {
        public const string DefaultFileName = "RVDAC.Logger.Settings.xml";

        public ObservableCollection<SensorProfile> SensorProfiles { get; set; } = new BindableObservableCollection<SensorProfile>();
        public ObservableCollection<LogFile> LogFiles { get; set; } = new BindableObservableCollection<LogFile>();

        [XmlIgnore]
        public string FileName { get; set; } = DefaultFileName;

        public bool SaveToFile(string fileName = DefaultFileName)
        {
            try
            {
                var serializer = new XmlSerializer(this.GetType());
                var writerSettings = new XmlWriterSettings
                {
                    Indent = true
                };
                using (var writer = XmlWriter.Create(AppDomain.CurrentDomain.BaseDirectory + "\\" + FileName, writerSettings))
                {
                    serializer.Serialize(writer, this);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
