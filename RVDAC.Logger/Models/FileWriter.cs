﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using RVDAC.Common;
using RVDAC.Common.Models;
using RVDAC.Common.ScsClient;

namespace RVDAC.Logger.Models
{
    public class FileWriter
    {
        public const string Delimiter = ",";
        public FileInfo Info { get; set; }
        public Guid SensorConfigGuid { get; set; }
        public DateTime StartTime { get; set; }

        public static bool CreateFolderIfNotExist(string path)
        {
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                    return true;
                }
                catch(Exception ex)
                {
                    Common.LogFile.LogFile.AddError($"Error creating directory {path}. {ex.Message}.");
                    return false;
                    
                }
            }
            return true;
        }

        public bool TryWriteToFile(DateTime dateTime, SensorValueHolder[] values, LogFile logFile)
        {
            //Make sure the .NET datetime is for the Z time zone
            dateTime = dateTime.ToUniversalTime();
            var sensorCount = 0;
            var lineText = "";
            var lastSensorGuid = logFile.SensorProfile.Sensors.Last().Guid;
            //terminate the previous line in the file
            lineText += "\r\n";
            //add the three datetime columns
            lineText += $"{TextEscape(dateTime.ToString("O"))}{Delimiter}";
            lineText += $"{ToUnixDate(dateTime)}{Delimiter}";
            lineText += $"{ToDecimalDay(dateTime).ToString("000.0000000")}{Delimiter}";

            foreach (var s in logFile.SensorProfile.Sensors)
            {
                var foundMatchingSensor = false;
                var valueToWrite = string.Empty;
                for (int i = 0; i < values.Length; i++)
                {
                    var value = values[i];
                    if (value.Guid.Equals(s.Guid))
                    {
                        foundMatchingSensor = true;
                        if (value.RawValue == null)
                        {
                            valueToWrite = String.Empty;
                            break;
                        }
                        valueToWrite = s.UseRawValue ? TextEscape(value.RawValue) : value.DecodedValue.ToString();
                        break;
                    }
                }
                lineText += foundMatchingSensor ? valueToWrite : "";
                sensorCount++;
                if (!s.Guid.Equals(lastSensorGuid))
                    lineText += $"{Delimiter}";
            }

            //make sure that we have a complete line of text, this is a bugfix 12/27/2016
            if (sensorCount != logFile.SensorProfile.Count)
                return false;

            //write to file
            using (var stream = Info.AppendText())
            {
                stream.Write(lineText);
            }
            return true;
        }

        public bool TryCreateNewFile(DateTime dateTime, SensorConfiguration config, LogFile logFile)
        {
            //This method will throw any exceptions back to the LogFile class to alert the user
            //Create the filename
            var fileName = $"{logFile.Name}_{dateTime.ToString("yyyyMMdd-HHmmss")}.csv";
            //Add a final slash to the end of the path
            var path = logFile.Path;
            if (!path.EndsWith("\\"))
                path += "\\";
            //Try to create the file folder
            if (!CreateFolderIfNotExist(path))
                throw new IOException("The folder " + path + " could not be created.");
            //Get the matching sensors from SCS in the same order as the GUID list
            var sensorCollection = new List<ChildNode>();
            foreach (var s in logFile.SensorProfile.Sensors)
            {
                sensorCollection.Add(config.AllChildSensors().First(x => x.Guid == s.Guid));
            }
            using (var writer = File.CreateText(path + fileName))
            {
                writer.WriteLine("#DataStartLine:" + (6 + 3 + sensorCollection.Count + 2));
                writer.WriteLine("#Description:" + logFile.Description);
                writer.WriteLine("#Encoding:UTF-8");
                writer.WriteLine("#SensorConfigurationGUID:" + config.Guid);
                writer.WriteLine("#COLUMNDEFINITIONSTART#");
                writer.WriteLine($"#Column{Delimiter}" +
                                 $"Name{Delimiter}" +
                                 $"DataType{Delimiter}" +
                                 $"Units{Delimiter}" +
                                 $"MeasurementClass{Delimiter}" +
                                 $"Comment{Delimiter}" +
                                 $"Guid");
                writer.WriteLine($"#001{Delimiter}{TextEscape("DateTime_ISO8601")}{Delimiter}{TextEscape("DateTime")}{Delimiter}{TextEscape("YYYY-MM-DDThh:mm:ss.sssssssZ")}{Delimiter}{Delimiter}{TextEscape("UTC time and date formatted to ISO8601 standard") }{Delimiter}");
                writer.WriteLine($"#002{Delimiter}{TextEscape("DateTime_UNIX")}{Delimiter}{TextEscape("DateTime")}{Delimiter}{TextEscape("ms")}{Delimiter}{Delimiter}{TextEscape("UTC milliseconds since epoch. Divide by 1000 for UNIX seconds.")}{Delimiter}");
                writer.WriteLine($"#003{Delimiter}{TextEscape("DateTime_DecimalDOY")}{Delimiter}{TextEscape("DateTime")}{Delimiter}{Delimiter}{Delimiter}{TextEscape("UTC fractional day of year. Zero indexed. January 1 is day 0.")}{Delimiter}");
                for (int i = 0; i < sensorCollection.Count(); i++)
                {
                    //Get the sensor object by GUID
                    var sensorObject = sensorCollection[i];
                    writer.WriteLine($"#{(i + 4).ToString("000")}{Delimiter}" +
                                     $"{TextEscape(sensorObject.Name)}{Delimiter}" +
                                     $"{TextEscape(sensorObject.DecodeType.ToString())}{Delimiter}" +
                                     $"{TextEscape(sensorObject.Units)}{Delimiter}" +
                                     $"{TextEscape(sensorObject.Measurement)}{Delimiter}" +
                                     $"{TextEscape(sensorObject.Comment)}{Delimiter}" +
                                     $"{sensorObject.Guid}");
                }
                writer.WriteLine("#COLUMNDEFINITIONEND#");
                //Start of data
                //Three column for dateTime
                writer.Write($"DateTime_ISO8601{Delimiter}DateTime_UNIX{Delimiter}DateTime_DecimalDOY{Delimiter}");
                var lastSensor = sensorCollection.Last();
                foreach (var sensor in sensorCollection)
                {
                    writer.Write(sensor.Name);
                    if(!sensor.Equals(lastSensor))
                        writer.Write(Delimiter);
                }
            }

            //Populate the rest of this model
            SensorConfigGuid = config.Guid;
            Info = new FileInfo(path + fileName);
            StartTime = dateTime;
            return true;
        }

        private static string TextEscape(string text)
        {
            if (text == null)
                return null;
            return "\"" + text.Replace("\"", "\"\"") + "\"";
            //return $"\"{text}\"";
        }

        public static double ToJulianDate(DateTime date)
        {
            return date.ToOADate() + 2415018.5;
        }

        public static double ToDecimalDay(DateTime date)
        {
            return (date - new DateTime(date.Year,1,1).ToUniversalTime()).TotalDays ;
        }

        public static long ToUnixDate(DateTime date)
        {
            var unixTime = date - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return (long)unixTime.TotalMilliseconds;
        }
    }
}
