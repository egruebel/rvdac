﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using RVDAC.Common.Converters;
using RVDAC.Common.Models;

namespace RVDAC.Common
{
    public class ConfigImporter
    {

        public ConfigImporter(XDocument xml, SensorConfiguration config)
        {
            ProcessingErrors = new List<string>();
            SensorXmlDoc = xml;
            SensorConfig = config;
        }

        private XDocument SensorXmlDoc { get; set; }

        public SensorConfiguration SensorConfig { get; set; }

        public string LastSensorProcessed { get; set; }

        public string LastFieldProcessed { get; set; }

        private short Id { get; set; }

        public List<string> ProcessingErrors { get; set; }

        private bool HasExtendedFields()
        {
            if (SensorXmlDoc.XPathSelectElements("//*[@extended-field]").Any())
                return true;
            return false;
        }
        
        public bool TryBuildFromLegacyScs()
        {
            //TestFileIo();
            //try to read the Sensor.Xml file
            //SensorXmlDoc = XDocument.Load(FilePath);
            //make sure this isn't already a new file type
            if (HasExtendedFields())
                throw new InvalidOperationException("The Import function is intended for legacy SCS sensor.xml files. The selected file has already been converted. Use the Open function instead.");
            //try to populate a new model
            return TryPopulateModel(false);
        }

        public bool TryBuildFromExtendedScs()
        {
            //TestFileIo();
            //try to read the Sensor.Xml file
            //SensorXmlDoc = XDocument.Load(FilePath);
            //make sure this isn't an old sensor.xml file without extended fields
            if(!HasExtendedFields())
                throw new InvalidOperationException("The Open function is intended for new sensor.xml files with extended functionality. The selected file has not yet been converted. Use the Import function instead.");
            return TryPopulateModel(true);
        }

        public bool TryBuildFromAcq()
        {
            if(!HasExtendedFields())
                throw new InvalidOperationException("The XML configuration provided does not have the extended fields required by this operation");
            return TryPopulateModel(true);
        }

        private bool TryPopulateModel(bool populateExtendedFields)
        {
            //get the GUID and Measurements for this config
            if (populateExtendedFields)
            {
                try
                {
                    var guid = SensorXmlDoc.Element("SensorList")?.Attribute("config-guid").Value;
                    Guid parseGuid;
                    if (!Guid.TryParse(guid, out parseGuid))
                    {
                        throw new Exception();
                    }
                    SensorConfig.Guid = parseGuid;
                }
                catch
                {
                    throw new InvalidDataException("Unable to parse the configuration GUID from xml");
                }
                try
                {
                    var mAttr = SensorXmlDoc.Element("SensorList")?.Attribute("measurement-classes").Value;
                    var measuremnts = mAttr.Split(';').ToList();
                    if(!measuremnts.Any())
                        throw new Exception();
                    SensorConfig.MeasurementClasses = new ObservableCollection<string>(measuremnts);
                }
                catch
                {
                    throw new InvalidDataException("Unable to parse the measurement classes from xml");
                }
            }
            //get xElements of all parent sensors
            var parentsXml =
                SensorXmlDoc.XPathSelectElements("//Sensor[.//SensorType[contains(text(),'PARENT')]]").ToList();

            if (!parentsXml.Any())
            {
                throw new InvalidDataException("The supplied file is not a valid SCS Sensor configuration.");
            }

            //build our collection of sensors from the sensor.xml file
            //short liveId = 0;
            foreach (var parentXml in parentsXml)
            {
                //get the name of this parent element
                var parentName = XmlGetString(parentXml, "SensorName");
                //set the lastsensorprocessed so the user has some idea of where an error lies
                LastSensorProcessed = parentName;
                
                var parentType = XmlGetString(parentXml, "SensorType");
                //get all of the children for this parent object
                var childrenXml =
                    SensorXmlDoc.XPathSelectElements("//Sensor[.//ParentSensorName[text() = '" + parentName + "']]")
                        .ToList();

                ParentNodeBase parent;

                if (parentType.Contains("DERIVED"))
                {
                    parent = BuildVirtual(parentXml);
                    if(populateExtendedFields)
                        TryPopulateExtendedFields((VirtualParentBase)parent, parentXml);
                }
                else
                {
                    parent = BuildParent(parentXml);
                    if(populateExtendedFields)
                        TryPopulateExtendedFields((PhysicalParentNode)parent, parentXml);
                }

                if (parent == null)
                    throw new InvalidOperationException("Unknown sensor type in source file");

                SensorConfig.AllParentSensors.Add(parent);

                foreach (var childXml in childrenXml)
                {
                    var child = BuildChild(childXml);
                    child.Parent = parent;
                    if (populateExtendedFields)
                        TryPopulateExtendedFields(child, childXml);
                    parent.ChildNodes.Add(child);
                }
            }
            //Finally now that we have a valid config, we should cross reference all derived sensors
            PopulateVirtualBaseSensors(SensorXmlDoc);
            //If this is an import, create default measurement classes
            if(!populateExtendedFields)
                SensorConfig.PopulateDefaultMeasurementClasses();
            //make sure that the measurement classes can all cross reference
            ValidateMeasurementClasses();
            //Config is not dirty because we just opened it from storage
            SensorConfig.ClearDirtyStatus();
            //Give all sensors a unique Id
            foreach (var parent in SensorConfig.AllParentSensors)
            {
                parent.Id = GetNextId();
                foreach (var child in parent.ChildNodes)
                    child.Id = GetNextId();
            }
            //Tell the user that this config has been positively populated
            SensorConfig.IsInitialized = true;
            return true;
        }

        private short GetNextId()
        {
            return Id += 1;
        }

        private bool TryPopulateExtendedFields(NodeBase sensor, XElement xml)
        {
            var extendedFields = SensorXmlDoc.XPathSelectElements("//*[@extended-field]");
            if (!extendedFields.Any())
                throw new InvalidOperationException("No elements with the attribute 'extended-field' could be found");


            foreach (var property in sensor.GetType().GetProperties())
            {
                LastFieldProcessed = property.Name;
                if (!property.CanWrite)
                    continue;
                foreach (var attribute in
                        property.GetCustomAttributes(false)
                            .Where(x => x.GetType() == typeof(ScsCrossReferenceAttribute)))
                {
                    //now we edit each field that's marked with the extension attribute
                    var crossRef = (ScsCrossReferenceAttribute)attribute;
                    var stringValue = XmlGetString(xml, crossRef.ReferenceTo);
                    object conversionValue = stringValue;
                    //This next step is needed because we use a lot of nullable types
                    var propertyType = Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType;
                    if (crossRef.ValueConverterType != null)
                    {
                        //There's a special conversion necessary
                        var converter = (ILegacyConverter)Activator.CreateInstance(crossRef.ValueConverterType);
                        conversionValue = converter.ConvertFromLegacy(stringValue);
                    }
                    else
                    {
                        conversionValue = (stringValue == null) ? null : TypeDescriptor.GetConverter(propertyType).ConvertFromInvariantString((string)conversionValue);
                    }

                    property.SetValue(sensor, conversionValue);
                }
            }
            return true;
        }

        private void ValidateMeasurementClasses()
        {
            foreach (var sensor in SensorConfig.AllChildSensors())
            {
                if (string.IsNullOrEmpty(sensor.Measurement))
                    continue;
                if(!SensorConfig.MeasurementClasses.Contains(sensor.Measurement) )
                    SensorConfig.MeasurementClasses.Add(sensor.Measurement);
            }
        }

        private int XmlGetInt32(XElement xml, string elementName)
        {
            LastFieldProcessed = elementName;
            var e = xml.Element(elementName);
            if (e == null)
            {
                throw new ArgumentException("The element " + elementName + " could not be found");
            }

            int getVal;
            if (int.TryParse(e.Value, out getVal))
            {
                return getVal;
            }
            throw new ArgumentException("The value of element " + elementName + " is not a valid int32");
        }

        private bool XmlGetBoolean(XElement xml, string elementName)
        {
            LastFieldProcessed = elementName;
            var e = xml.Element(elementName);
            if (e == null)
            {
                throw new ArgumentException("The element " + elementName + " could not be found");
            }
            bool getVal;
            if (bool.TryParse(e.Value, out getVal))
            {
                return getVal;
            }
            throw new ArgumentException("The value of element " + elementName + " is not a valid boolean");
        }

        private int? XmlGetIntNullable(XElement xml, string elementName)
        {
            LastFieldProcessed = elementName;
            var e = xml.Element(elementName);
            if (e == null)
            {
                throw new ArgumentException("The element " + elementName + " could not be found");
            }
            int getVal;
            if (int.TryParse(e.Value, out getVal))
            {
                return getVal;
            }
            return null;
        }

        private string XmlGetString(XElement xml, string elementName)
        {
            LastFieldProcessed = elementName;
            var e = xml.Element(elementName);
            if (e == null)
            {
                throw new ArgumentException("The element " + elementName + " could not be found");
            }
            if (e.Value == "No Value")
                return null;
            return e.Value;
        }

        private PhysicalParentNode BuildParent(XElement xml)
        {
            var parent = new PhysicalParentNode();
            //fill in the basic info...
            PopulateBaseFields(xml, parent);

            //get parent sensor only info...
            parent.SentenceQualifier = XmlGetString(xml, "SentenceLabel");
            //sensor timeout is undefined for derived values. In this case use 4X the update rate
            //as a last resort use 12 seconds
            parent.Timeout = GetTimeOut(XmlGetString(xml, "Timeout"), XmlGetString(xml, "UpdateRate"));
            parent.SentenceLength = XmlGetIntNullable(xml, "RecordSize");


            //IO Port stuff
            var port = new IoPort();
            //define some variables for tryparse methods
            StopBits stopbits;
            Parity parity;

            port.Guid = Guid.NewGuid();
            parent.EndOfString = XmlGetInt32(xml, "TermChar");

            port.ComPort = XmlGetString(xml, "Device");
            port.BaudRate = XmlGetInt32(xml, "BaudRate");

            port.BaudRate = XmlGetIntNullable(xml, "BaudRate");
            //This is infuriating
            port.DataBits = (int?)new TextNumeralToIntConverter().ConvertFromLegacy(XmlGetString(xml, "DataBits"));
            port.PortNo = XmlGetIntNullable(xml, "Socket");
            //Here's an inconsistency in SCS
            port.PortNo = port.PortNo == 0 ? null : port.PortNo;

            port.IpAddress = XmlGetString(xml, "NetAddress");

            var sb = XmlGetString(xml, "StopBits");
            var pr = XmlGetString(xml, "Parity");
            if (Enum.TryParse(sb, true, out stopbits))
                port.StopBits = stopbits;
            if (Enum.TryParse(pr, true, out parity))
                port.Parity = parity;


            port.PortType =
                (IoPort.DacPortType) new IoPortTypeConverter().ConvertFromLegacy(XmlGetString(xml, "ConnectionType"));

            if (SensorConfig.IoPorts.Contains(port))
            {
                //port already exists in collection
                parent.IoPort = SensorConfig.IoPorts.First(x => x.Equals(port));
            }
            else
            {
                SensorConfig.IoPorts.Add(port);
                parent.IoPort = port;
            }

            return parent;
        }

        private ChildNode BuildChild(XElement xml)
        {
            var child = new ChildNode();
            PopulateBaseFields(xml, child);
            LastSensorProcessed = child.Name;
            child.DecodeType =
                (ChildNode.DecodeTypeEnum) new DecodeTypeConverter().ConvertFromLegacy(XmlGetString(xml, "DecodeType"));
            child.Units = XmlGetString(xml, "Units");
            child.UnitFieldPosition = XmlGetIntNullable(xml, "UnitsFieldPosition");
            child.DataFieldPosition = XmlGetIntNullable(xml, "DataFieldPosition");
            child.StartIndex = XmlGetIntNullable(xml, "StartChar");
            child.EndIndex = XmlGetIntNullable(xml, "StopChar");

            return child;
        }

        private VirtualParentBase BuildVirtual(XElement xml)
        {
            var virtualType = XmlGetString(xml, "SensorType");
            if (virtualType.Contains("TRUEWIND"))
            {
                var virtualTrueWind = new VirtualTrueWind();
                PopulateBaseFields(xml, virtualTrueWind);
                //virtualTrueWind.VirtualSensorType = VirtualParentBase.VirtualSensorTypeEnum.TrueWind;
                virtualTrueWind.SampleMessage = "$DERIV,5,4.1,590.3,119,";
                //sensor timeout is undefined for derived values. In this case use 4X the update rate
                //as a last resort use 12 seconds
                virtualTrueWind.Timeout = GetTimeOut(XmlGetString(xml, "Timeout"), XmlGetString(xml, "UpdateRate"));
                return virtualTrueWind;
            }
            //else
            var virtualAverage = new VirtualAverage();
            PopulateBaseFields(xml, virtualAverage);
            virtualAverage.AverageType = XmlGetString(xml, "AverageType") == "Polar"
                ? VirtualAverage.Average.Polar
                : VirtualAverage.Average.Arithmetic;

            //figure out the average mode. This is another SCS hack
            //negative for time, positive for samples
            var noSamples = XmlGetIntNullable(xml, "NumberOfSamples");
            virtualAverage.AverageMode = noSamples > 0
                ? VirtualAverage.AverageDomain.Samples
                : VirtualAverage.AverageDomain.Time;
            if (noSamples != null)
                virtualAverage.AverageSampleSize = Math.Abs((int)noSamples);
            virtualAverage.Precision =
                (int?) new TextNumeralToIntConverter().ConvertFromLegacy(XmlGetString(xml, "Precision"));
            //virtualAverage.VirtualSensorType = VirtualParentBase.VirtualSensorTypeEnum.Average;
            virtualAverage.SampleMessage = "$DERIV,14.9,14.9,89.5,6,";
            //sensor timeout is undefined for derived values. In this case use 4X the update rate
            //as a last resort use 12 seconds
            virtualAverage.Timeout = GetTimeOut(XmlGetString(xml, "Timeout"), XmlGetString(xml, "UpdateRate"));
            return virtualAverage;
        }

        public void PopulateVirtualBaseSensors(XDocument xml)
        {
            foreach (var s in SensorConfig.AllParentSensors.Where(x => x.IsVirtual))
            {
                //cast this sensor as a virtual
                var virtualParent = s as VirtualParentBase;

                if (virtualParent == null)
                    throw new InvalidOperationException("Could not cast as VirtualParent");
                //get the legacy name
                var vxl = xml.XPathSelectElement("//Sensor[.//SensorName[text() = '" + s.LegacyName + "']]");
                for (int i = 1; i < 7; i++)
                {
                    var baseElementName = "BaseSensor" + i;
                    var baseElementValue = XmlGetString(vxl, baseElementName);
                    if (baseElementValue != null)
                    {
                        //get the matching sensor from our populated collection
                        var baseSensor =
                            SensorConfig.AllParentSensors.Select(x => x.ChildNodes)
                                .SelectMany(x => x)
                                .FirstOrDefault(x => x.LegacyName == baseElementValue);
                        if (baseSensor == null)
                            throw new InvalidOperationException("Could not find base sensor " + baseElementValue + " referred to by " + virtualParent.Name);
                        if (virtualParent.SourceCollection.Count < i)
                            throw new InvalidOperationException("Base sensor collection for " + virtualParent.Name + " is not large enough to accept " + baseElementValue);
                        virtualParent.SourceCollection[i - 1].Source = baseSensor;
                    }
                }
            }
        }

        private void PopulateBaseFields(XElement xml, NodeBase node)
        {
            //fields for all sensors
            node.DeviceType = (NodeBase.DeviceTypeEnum)new SensorTypeConverter().ConvertFromLegacy(XmlGetString(xml, "SensorType"));
            //node.Id = XmlGetInt32(xml, "SensorId");
            node.Guid = Guid.NewGuid();
            node.LegacyName = XmlGetString(xml, "SensorName");
            node.Name = node.LegacyName;
            node.Comment = XmlGetString(xml, "SensorComment");
            node.Enabled = XmlGetBoolean(xml, "SensorEnabled");
        }

        private static int GetTimeOut(string timeout, string updateRate)
        {
                int t;
                if (int.TryParse(timeout, out t))
                {
                    return t;
                }
                if (int.TryParse(updateRate, out t))
                {
                    return t*4;
                }
                return 12;
        }
    }
}