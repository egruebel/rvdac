﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Data.SQLite;
using System.Threading.Tasks;

namespace RVDAC.Common.LogFile
{
    public static class LogFile
    {
        private static int Max_Table_Rows = 100000;

        public static void AddError(string message, [CallerMemberName] string memberName = null)
        {
            Insert(message, TableNames.Error, memberName);
        }

        public static void AddInfo(string message, [CallerMemberName] string memberName = null)
        {
            Insert(message, TableNames.Info, memberName);
        }

        public static List<LogObject> GetErrors(int limit = 200)
        {
            return Select(TableNames.Error, limit);
        }

        public static List<LogObject> GetInfo(int limit = 200)
        {
            return Select(TableNames.Info, limit);
        }

        private static List<LogObject> Select(string table, int limit = 200)
        {
            var vals = new List<LogObject>();
            try
            {
                using (var db = GetConnection())
                {
                    db.Open();
                    using (var command = new SQLiteCommand(db))
                    {
                        command.CommandText = $"SELECT * FROM {table} ORDER BY Id DESC LIMIT {limit.ToString()}";
                        using (SQLiteDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var obj = new LogObject()
                                {
                                    Id = (long)reader["Id"],
                                    TimeStamp = (DateTime)reader["DateTime"],
                                    Message = (string)reader["Message"],
                                    Location = (string)reader["Location"]
                                };
                                vals.Add(obj);
                            }
                        }
                    }
                    db.Close();
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
            }
            return vals;
        }

        private static void Insert(string message, string table, string memberName = null )
        {
            Task.Run(() =>
            {
                try
                {
                    using (var db = GetConnection())
                    {
                        db.Open();
                        using (var transaction = db.BeginTransaction()) //all or nothing
                        {
                            using (var command = new SQLiteCommand(db))
                            {
                                command.Transaction = transaction;
                                command.CommandText =
                                    $"INSERT INTO {table} VALUES (@Id, @DateTime, @Message, @Location );";
                                command.Parameters.Add("@Id", DbType.Int32);
                                command.Parameters.Add("@DateTime", DbType.DateTime).Value = DateTime.Now;
                                command.Parameters.Add("@Message", DbType.AnsiString).Value = message;
                                command.Parameters.Add("@Location", DbType.AnsiString).Value = memberName;
                                command.ExecuteNonQuery();
                            }
                            transaction.Commit();

                        }
                        db.Close();
                    }
                }
                catch (Exception ex)
                {
                    //we cant log the message because the backup database is not working for some reason
                    Console.WriteLine(ex.Message);
                }
            });
        }


        private struct TableNames
        {
            public const string Info = "info";
            public const string Error = "error";
        }

        private static SQLiteConnection GetConnection()
        {
            var assemblyName = Assembly.GetEntryAssembly().Location;
            if (assemblyName == null)
                return null;

            var dbFileName = assemblyName.Replace(".exe", $".Log.{DateTime.Now.Year}.sqlite"); 

            var dbConnectionString = $"Data Source={dbFileName};Version=3;FKSupport=True";

            SQLiteConnection sqlLiteConnection;

            if(File.Exists(dbFileName) || TryCreateDatabase(dbFileName, dbConnectionString, out sqlLiteConnection))
                return new SQLiteConnection(dbConnectionString);

            return null;
        }

        private static bool TryCreateDatabase(string dBFileName, string dBConnectionString, out SQLiteConnection sqLiteConnection)
        {
            try
            {
                SQLiteConnection.CreateFile(dBFileName);

                string createTableInfo = @"CREATE TABLE IF NOT EXISTS [info] (
                          [Id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                          [DateTime] DATETIME NOT NULL,
                          [Message] VARCHAR(2048) NULL,
                          [Location] VARCHAR(2048) NULL
                          )";

                string createTableError = @"CREATE TABLE IF NOT EXISTS [error] (
                          [Id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                          [DateTime] DATETIME NOT NULL,
                          [Message] VARCHAR(2048) NULL,
                          [Location] VARCHAR(2048) NULL
                          )";

                using (var db = new SQLiteConnection(dBConnectionString))
                {
                    db.Open();
                    using (var transaction = db.BeginTransaction())
                    {
                        using (var connection = new SQLiteCommand(db))
                        {
                            connection.Transaction = transaction;
                            connection.CommandText = createTableInfo;
                            connection.ExecuteNonQuery();
                            connection.CommandText = createTableError;
                            connection.ExecuteNonQuery();
                        }
                        transaction.Commit();
                    }
                    db.Close();
                }
                sqLiteConnection = new SQLiteConnection(dBConnectionString);
                return true;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                sqLiteConnection = null;
                return false;
            }
        }

        public struct LogObject
        {
            public long Id { get; set; }
            public DateTime TimeStamp { get; set; }
            public string Message { get; set; }
            public string Location { get; set; }
        }
    }

}
