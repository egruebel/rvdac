﻿using System;

namespace RVDAC.Common
{
    public class PropertyChangingEventArgs: EventArgs
    {
        public string PropertyName { get; set; }
        public object OldValue { get; set; }
        public object NewValue { get; set; }
    }
}
