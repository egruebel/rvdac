﻿using System;
using System.Collections.Generic;
using System.Linq;
using RVDAC.Common.Models;

namespace RVDAC.Common
{
    public struct NodeCrossReferenceResult
    {
        public bool IsReferenced { get; set; }
        public List<NodeBase> ReferencedNodeList { get; set; }
    }
}
