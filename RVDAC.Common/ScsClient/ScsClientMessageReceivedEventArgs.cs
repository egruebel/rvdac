﻿using System;

namespace RVDAC.Common.ScsClient
{
    public class ScsClientMessageReceivedEventArgs: EventArgs
    {
        public ScsClientSocket.ScsMessageType MessageType { get; set; }
        public string Message { get; set; }
        public DateTime ReceiveTime { get; set; }
    }
}
