﻿using System;

namespace RVDAC.Common.ScsClient
{
    public class ScsMessageParsingException: Exception
    {
        public ScsMessageParsingException(string errorMessage, string details = "", string sensorName = "")
        {
            Message = errorMessage;
            Details = details;
            SensorName = sensorName;
        }
        public override string Message { get; }
        public string SensorName { get; }
        public string Details { get; }
    }
}
