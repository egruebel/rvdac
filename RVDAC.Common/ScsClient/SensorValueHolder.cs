﻿using System;
using RVDAC.Common.Models;

namespace RVDAC.Common.ScsClient
{
    public struct SensorValueHolder
    {
        public SensorValueHolder(Guid guid, short id, string rawVal, dynamic val)
        {
            Guid = guid;
            Id = id;
            RawValue = rawVal;
            DecodedValue = val;
        }

        public SensorValueHolder(NodeBase sensorNode)
        {
            Guid = sensorNode.Guid;
            Id = sensorNode.Id;
            RawValue = sensorNode.Data.RawValue;
            DecodedValue = sensorNode.Data.DecodedValue;
        }

        public Guid Guid { get; }
        public short Id { get; }
        public string RawValue { get; }
        public dynamic DecodedValue { get; }
    }
}
