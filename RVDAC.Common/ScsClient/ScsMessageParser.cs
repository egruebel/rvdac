﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using RVDAC.Common.Models;

namespace RVDAC.Common.ScsClient
{
    public class ScsMessageParser
    {
        private string _step;
        public event EventHandler<ScsMessageParsingException> ParsingError;

        private void OnParsingError(ScsMessageParsingException ex)
        {
            ParsingError?.Invoke(this, ex);
        }

        private string ErrorLocation
        {
            get { return $"Error at: {_step}"; }
        }

        public void Parse(string xmlResponse, SensorConfiguration config)
        {
            var response = new XDocument();
            var sensorData = new List<XElement>();
            try
            {
                _step = "convert_scsxmlresponse_to_xdoc";
                if (string.IsNullOrEmpty(xmlResponse))
                    throw new ScsMessageParsingException("Empty response received from SCS server.");
                response = ReadXDocumentWithInvalidCharacters(xmlResponse);
                sensorData = response.XPathSelectElements("//SensorData").ToList();
                if (!sensorData.Any())
                    throw new ScsMessageParsingException("Response from SCS server did not contain any sensors.");
            }
            catch (Exception ex)
            {
                var mpe = new ScsMessageParsingException(ErrorLocation + ex.Message);
                OnParsingError(mpe);
            }

            foreach (var sensor in sensorData)
            {
                try
                {
                    //get the matching parent sensor
                    _step = "parse_parent_sensor_from_xdoc";
                    var sensorName = sensor.Element("sensor_name")?.Value;
                    _step = "find_matching_parent_in_sensorconfig";
                    var parentSensor = config.AllParentSensors.FirstOrDefault(v => v.Name == sensorName);
                    if (parentSensor == null)
                        throw new Exception("Unable to find '" + sensorName + "' in the current configuration.");
                    UpdateParentAndChildren(parentSensor, sensor);
                }
                catch (Exception ex)
                {
                    var mpe =  new ScsMessageParsingException(ErrorLocation + ex.Message, xmlResponse);
                    OnParsingError(mpe);
                }
            }
        }

        public void UpdateChildrenFromSampleString(SensorConfiguration config)
        {
            foreach (var sensor in config.AllParentSensors)
            {
                if (string.IsNullOrEmpty(sensor.SampleMessage))
                    continue;
                sensor.Data.RawValue = sensor.SampleMessage;
                UpdateChildren(sensor);
            }
        }

        private void UpdateParentAndChildren(ParentNodeBase parent, XElement scsResponseXml)
        {
            try
            {
                _step = "parse_error_code_from_xmlresponse";
                int errorCodeParse;
                if (int.TryParse(scsResponseXml.Element("error_code").Value, out errorCodeParse))
                {
                    if (errorCodeParse != 0)
                    {
                        //a value of 1 means that sensor doesn't exist (unlikely)
                        //a value of 2 means that there was no data received...ever
                        SetParentError(parent, (NodeDataObject.ErrorCodeEnum) errorCodeParse, true);
                        return;
                    }
                }
                    //parent.Data.ErrorCode = errorCodeParse;

                //ACQ detected an error
                
                //Check the timestamp, we dont want old data
                _step = "parse_time_stamp_from_xmlresponse";
                parent.Data.TimeStamp = DateTime.Parse(scsResponseXml.Element("time_stamp").Value);
                _step = "compare_time_stamp_determine_timeout";
                var timeDiff = (DateTime.Now.ToUniversalTime() - parent.Data.TimeStamp).TotalSeconds;
                if (timeDiff > parent.Timeout)
                {
                    SetParentError(parent, NodeDataObject.ErrorCodeEnum.TimedOut, true);
                    return;
                }
                //Get the current raw value
                _step = "parse_raw_data_value_from_xmlresponse";
                var rawValue = scsResponseXml.Element("data_value").Value;

                if (string.IsNullOrEmpty(rawValue))
                {
                    SetParentError(parent, NodeDataObject.ErrorCodeEnum.NoDataReceived, true);
                    return;
                }

                //validate the NMEA checksum if applicable
                if (parent.DeviceType == NodeBase.DeviceTypeEnum.Nmea && SentenceHasNmeaChecksum(rawValue))
                {
                    if (!NmeaChecksumIsValid(rawValue))
                    {
                        SetParentError(parent, NodeDataObject.ErrorCodeEnum.BadChecksum, true);
                        return;
                    }
                }
                //If you're here then the parent sensor raw value looks good. 
                SetParentError(parent, NodeDataObject.ErrorCodeEnum.None, false);
                parent.Data.RawValue = rawValue;
                //Parent data is always a string
                parent.Data.DecodedValue = rawValue;
                //Update the child sensors
                UpdateChildren(parent);

            }
            catch (Exception ex)
            {
                SetParentError(parent, NodeDataObject.ErrorCodeEnum.RawValueParsingError, true);
                var mpe = new ScsMessageParsingException(ErrorLocation + ex.Message, scsResponseXml.Value, parent.Name);
                OnParsingError(mpe);
            }
        }

        private void SetParentError(ParentNodeBase parent, NodeDataObject.ErrorCodeEnum newErrorCode, bool nullify, bool logMessage = true)
        {
            //get the existing error code of this parent
            var existingErrorCode = parent.Data.ErrorCode;

            // If error code is already set then we don't need to take action
            if (newErrorCode == existingErrorCode)
                return;

            parent.Data.ErrorCode = newErrorCode;

            if (nullify)
                SetParentSensorToNull(parent);

            if (newErrorCode == NodeDataObject.ErrorCodeEnum.None)
            {
                if (logMessage)
                    LogFile.LogFile.AddError($"Parent sensor error cleared for {parent.Name}.");
            }
            else
            {
                if (logMessage)
                    LogFile.LogFile.AddError($"Parent sensor error at {_step}. {parent.Name}: {newErrorCode}.");
            }
        }

        private void SetChildError(ParentNodeBase parent, ChildNode child, NodeDataObject.ErrorCodeEnum errorCode, bool nullify, bool logMessage = true)
        {
            if (errorCode == child.Data.ErrorCode)
                return;

            if (errorCode == NodeDataObject.ErrorCodeEnum.None)
            {
                //if the parent sensor has a child error and you are here, that means that the child error has cleared. 
                //It's safe to clear the parent error. 
                parent.Data.HasChildError = false;
                if (logMessage)
                    LogFile.LogFile.AddError($"Child sensor error cleared for {child.Name}. Errorcode was {child.Data.ErrorCode}");
            }
            else
            {
                //If you're here it means that this is a fresh error. If the parent doesn't already have an error, set it. 
                //We don't want to override another parent error
                parent.Data.HasChildError = true;
                if (logMessage)
                    LogFile.LogFile.AddError($"Child sensor error at {_step}. {child.Name}: {errorCode}.");
            }

            child.Data.ErrorCode = errorCode;

            if (nullify)
                child.Data.ClearDataValues();
        }

        void SetParentSensorToNull(ParentNodeBase parent)
        {
            parent.Data.ClearDataValues();
            foreach (var child in parent.ChildNodes)
            {
                child.Data.ClearDataValues();
                child.Data.ErrorCode = NodeDataObject.ErrorCodeEnum.None;
            }
        }

        void UpdateChildren(ParentNodeBase parent)
        {
            var sensorName = parent.Name;
            //create an array for data field parsing
            string[] nmeaArray = parent.Data.RawValue.Split(',');

            //strip out the NMEA Checksum
            if (parent.DeviceType == NodeBase.DeviceTypeEnum.Nmea)
            {
                _step = "strip_nmea_checksum_from_parent_string";
                var lastVal = nmeaArray.LastOrDefault();
                if (lastVal != null && lastVal.Contains('*'))
                {
                    nmeaArray[nmeaArray.Length - 1] = lastVal.Substring(0, lastVal.IndexOf('*'));
                }
            }

            foreach (var child in parent.ChildNodes)
            {
                
                try
                {
                    sensorName = child.Name;
                    _step = "parse_child_data_value_from_xmlresponse";
                    switch (child.DeviceType)
                    {
                        case NodeBase.DeviceTypeEnum.NmeaChild:

                            child.Data.RawValue = nmeaArray[child.DataFieldPosition.Value];
                            if (child.DecodeType == ChildNode.DecodeTypeEnum.Navsec)
                            {
                                //Add the NSEW char
                                _step = "parse_nmea_navsec_units_rawvalue";
                                child.Data.RawValue += nmeaArray[child.UnitFieldPosition.Value];
                            }
                            break;
                        case NodeBase.DeviceTypeEnum.VirtualTrueWindChild:
                        case NodeBase.DeviceTypeEnum.VirtualAverageChild:
                            _step = "parse_virtual_child_rawvalue";
                            child.Data.RawValue = nmeaArray[child.DataFieldPosition.Value];
                            break;
                        case NodeBase.DeviceTypeEnum.SerialChild:
                            _step = "parse_serial_child_rawvalue";
                            string parentData = parent.Data.RawValue;
                            var start = child.StartIndex.Value - 1;
                            var len = child.EndIndex.Value - start;
                            child.Data.RawValue = parentData.Substring(start, len);
                            break;
                    }
                    _step = "trim_whitespace_from_rawvalue";
                    //trim whitespace
                    child.Data.RawValue = child.Data.RawValue.Trim();
                    //make empty strings null for consistency
                    if (string.IsNullOrEmpty(child.Data.RawValue))
                    {
                        child.Data.RawValue = null;
                    }
                    //convert to the values native type (DecodeType)
                    if (!ConvertRawToType(child))
                    {
                        SetChildError(parent, child, NodeDataObject.ErrorCodeEnum.DecodedValueParsingError, true);
                        child.Data.DecodedValue = null;
                    }
                    else
                    {
                        //if you're here this was a good data point
                        SetChildError(parent, child, NodeDataObject.ErrorCodeEnum.None, false);
                    }
                    
                }
                catch (Exception ex)
                {
                    child.Data.ClearDataValues();
                    SetChildError(parent, child, NodeDataObject.ErrorCodeEnum.RawValueParsingError, true);
                    var mpe = new ScsMessageParsingException(ErrorLocation + ex.Message, "Raw Message= " + parent.Data.RawValue,
                    sensorName);
                    OnParsingError(mpe);
                }
                
            }
        }

        bool ConvertRawToType(ChildNode child)
        {
            //earlier we converted all empty strings to nulls so this is all-encompassing
            _step = "convert_raw_to_decodetype";
            if (child.Data.RawValue == null)
            {
                child.Data.DecodedValue = null;
                return true;
            }
            try
            {
                switch (child.DecodeType)
                {
                    case ChildNode.DecodeTypeEnum.Text:
                        _step = "convert_raw_to_text_decode";
                        child.Data.DecodedValue = child.Data.RawValue;
                        return true;
                    case ChildNode.DecodeTypeEnum.Integer:
                        _step = "convert_raw_to_integer_decode";
                        int outValInteger;
                        if (int.TryParse(child.Data.RawValue, out outValInteger))
                        {
                            //conversion was successful
                            child.Data.DecodedValue = outValInteger;
                            return true;
                        }
                        //conversion unsuccessful
                        child.Data.DecodedValue = null;
                        throw new Exception("Unable to convert raw value to integer");
                    case ChildNode.DecodeTypeEnum.Decimal:
                        double outValDouble;
                        _step = "convert_raw_to_decimal_decode";
                        if (double.TryParse(child.Data.RawValue, out outValDouble))
                        {
                            //conversion was successful
                            child.Data.DecodedValue = outValDouble;
                            return true;
                        }
                        child.Data.DecodedValue = null;
                        throw new Exception("Unable to convert raw value to decimal");
                    case ChildNode.DecodeTypeEnum.Navsec:
                        double navsec;
                        _step = "convert_raw_to_navsec_decode";

                        if (ConvertToNavsec(child.Data.RawValue, out navsec))
                        {
                            child.Data.DecodedValue = navsec;
                            return true;
                        }
                        child.Data.DecodedValue = null;
                        throw new Exception("Unable to convert raw lat/lon to signed decimal lat/lon");
                }
                return false;
            }
            catch (Exception ex)
            {
                var mpe = new ScsMessageParsingException(ErrorLocation + ex.Message, "Raw Value= " + child.Data.RawValue,
                    child.Name);
                OnParsingError(mpe);
                return false;
            }
        }

        bool ConvertToNavsec(string rawValue, out double navsec)
        {
            //This is a wrapper for the NAVSEC decoder class
            try
            {
                var deg = rawValue.TrimEnd(new[] {'N', 'S', 'W', 'E'});
                var nsew = rawValue[rawValue.Length - 1].ToString();
                if (NavsecDecoder.ToSignedDecimal(deg, nsew, out navsec))
                {
                    return true;
                }
                return false;
            }
            catch
            {
                navsec = 0;
                return false;
            }
        }

        static XDocument ReadXDocumentWithInvalidCharacters(string xmlResponse)
        {
            XDocument xDocument;
            var xmlReaderSettings = new XmlReaderSettings {CheckCharacters = false};
            using (var xmlReader = XmlReader.Create(new StringReader(xmlResponse), xmlReaderSettings))
            {
                // Load our XDocument
                xmlReader.MoveToContent();
                xDocument = XDocument.Load(xmlReader);
            }
            return xDocument;
        }

        public static bool SentenceHasNmeaChecksum(string sentence)
        {
            var sentenceParts = sentence.Split('*');
            return sentenceParts.Length == 2 && sentenceParts[1].Length == 2;
        }

        public static bool NmeaChecksumIsValid(string sentence)
        {
            //Get the checksum from the sentence
            string[] sentenceChecksumArray = sentence.Split('*');
            if (sentenceChecksumArray.Length != 2)
                return false;
            string sentenceChecksun = sentenceChecksumArray[1];
            //Start with first Item
            int calculatedchecksum = Convert.ToByte(sentence[sentence.IndexOf('$') + 1]);
            // Loop through all chars to get a checksum
            for (int i = sentence.IndexOf('$') + 2; i < sentence.IndexOf('*'); i++)
            {
                // No. XOR the checksum with this character's value
                calculatedchecksum ^= Convert.ToByte(sentence[i]);
            }
            // Get the checksum formatted as a two-character hexadecimal
            return calculatedchecksum.ToString("X2") == sentenceChecksun;
        }
    }
}