﻿using System;

namespace RVDAC.Common.ScsClient
{
    public class ScsSensorConfigMessageHolder
    {
        public ScsSensorConfigMessageHolder(Guid guid, string xml)
        {
            ConfigGuid = guid;
            ConfigXml = xml;
        }
        public Guid ConfigGuid { get; set; }
        public string ConfigXml { get; set; }
    }
}
