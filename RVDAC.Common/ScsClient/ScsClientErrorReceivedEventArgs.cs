﻿
namespace RVDAC.Common.ScsClient
{
    public class ScsClientErrorReceivedEventArgs
    {
        public string ErrorMessage { get; set; }
        public string ErrorDetails { get; set; }
        public string SensorName { get; set; }
        public ScsClientErrorType ErrorType { get; set; }



        public enum ScsClientErrorType
        {
            SocketError, ConfigError, DataError, Unknown
        }
    }
}
