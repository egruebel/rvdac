﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace RVDAC.Common.ScsClient
{
    public class ScsClient: INotifyPropertyChanged 
    {
        public event EventHandler<ScsClientErrorReceivedEventArgs> ErrorReceived;
        public event EventHandler<ScsClientMessageReceivedEventArgs> MessageReceived;
        public event EventHandler SensorConfigurationReceived;
        public event PropertyChangedEventHandler PropertyChanged;

        private readonly string _ipAddress;
        private readonly int _portNo;
        private readonly ScsMessageParser _parser = new ScsMessageParser();
        private string _scsRequestMessage, _errorStatus;
        private bool _lockPollingToClock, _isConnected, _isCommunicating, _hasError;
        private int _pollIntervalMs;

        private const int ResetDelayMs = 5000, MinPollIntervalMs = 50; //Minimum value of 50ms (20Hz)

        public const string DebugScsServerIp = "192.168.1.32";

        private readonly SynchronizationContext _uiThread;
        
        public ScsClient(string ipAddress, int portNo)
        {
            IsConnected = false;
            IsCommunicating = false;
            _ipAddress = ipAddress;
            _portNo = portNo;
            SensorConfig = new SensorConfiguration();
            _uiThread = SynchronizationContext.Current;
            _parser.ParsingError += _parser_ParsingError;
        }

#region properties
        private ScsClientSocket ClientSocket { get; set; }

        public int PollIntervalMs
        {
            get { return _pollIntervalMs;}
            set
            {
                if (value < MinPollIntervalMs)
                    value = MinPollIntervalMs; 
                _pollIntervalMs = value;
            }
        }

        public bool HasError
        {
            get { return _hasError; }
            set
            {
                if (_hasError != value)
                {
                    _hasError = value;
                    OnPropertyChanged(nameof(HasError));
                }
            }
        }

        public string ErrorStatus
        {
            get { return _errorStatus; }
            set
            {
                if (_errorStatus != value)
                {
                    _errorStatus = value;
                    OnPropertyChanged(nameof(ErrorStatus));
                }
            }
        }

        public bool IsCommunicating
        {
            get { return _isCommunicating; }
            private set
            {
                Task.Run(() =>
                {
                    if (_isCommunicating != value)
                    {
                        _isCommunicating = value;
                        OnPropertyChanged(nameof(IsCommunicating));
                        Thread.Sleep(MinPollIntervalMs - 10);
                        _isCommunicating = false;
                        OnPropertyChanged(nameof(IsCommunicating));
                    }
                });
            }
        }

        public bool IsConnected
        {
            get { return _isConnected; }
            private set
            {
                if (_isConnected != value)
                {
                    _isConnected = value;
                    OnPropertyChanged(nameof(IsConnected));
                }
            }
        }

        public SensorConfiguration SensorConfig { get; set; }
        #endregion

#region eventhandlers
        private void _parser_ParsingError(object sender, ScsMessageParsingException e)
        {
            var newArgs = new ScsClientErrorReceivedEventArgs()
            {
                ErrorType = ScsClientErrorReceivedEventArgs.ScsClientErrorType.DataError,
                ErrorMessage = e.Message,
                ErrorDetails = e.Details,
                SensorName = e.SensorName
            };
            ErrorReceived?.Invoke(this, newArgs);
            //LogFile.LogFile.AddError($"ScsClient parsing error for {e.SensorName}. {e.Message}. {e.Details}");
        }

        private void ClientSocket_ConnectedToServer(object sender, EventArgs e)
        {
            LogFile.LogFile.AddInfo($"Connected to ACQ server.");
            IsConnected = true;
            ClearSensorCollection();
            if (TryGetSensorConfigFromAcq())
            {
                //Raise the ConfigurationReceived event
                SensorConfigurationReceived?.Invoke(this, new EventArgs());
                //Begin normal polling
                if (_lockPollingToClock)
                    BeginSyncronousPolling();
                else
                    BeginPolling();
            }
        }

        private void ClientSocket_ErrorReceived(object sender, ScsClientErrorReceivedEventArgs e)
        {
            switch (e.ErrorType)
            {
                case ScsClientErrorReceivedEventArgs.ScsClientErrorType.ConfigError:
                    ClearSensorCollection();
                    HasError = true;
                    SensorConfig.IsInitialized = false;
                    ErrorStatus = e.ErrorMessage;
                    break;
                case ScsClientErrorReceivedEventArgs.ScsClientErrorType.SocketError:
                    NullifySensorCollection();
                    IsConnected = false;
                    HasError = true;
                    ErrorStatus = e.ErrorMessage;
                    break;
                case ScsClientErrorReceivedEventArgs.ScsClientErrorType.DataError:
                case ScsClientErrorReceivedEventArgs.ScsClientErrorType.Unknown:
                    break;
            }

            //pass the error on to the application
            ErrorReceived?.Invoke(this, e);
            LogFile.LogFile.AddError($"{e.ErrorMessage}. {e.ErrorDetails}. ");
        }

        private void ClientSocket_MessageReceived(object sender, ScsClientMessageReceivedEventArgs e)
        {
            if (e.MessageType == ScsClientSocket.ScsMessageType.SensorData)
            {
                _parser.Parse(e.Message, SensorConfig);
            }
            if (e.MessageType == ScsClientSocket.ScsMessageType.SensorConfig)
            {
                //SensorConfigHolder = new ScsSensorConfigMessageHolder(SensorConfig.Guid, e.Message);
            }
            MessageReceived?.Invoke(this, e);
        }
        #endregion



        public void LockPollingTimeToClock()
        {
            _lockPollingToClock = true;
        }

        public void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private void ClearSensorCollection()
        {
            //Always modify the sensor collection on the UI thread
            _uiThread.Send(x =>
            {
                SensorConfig.AllParentSensors.Clear();
            }, null);
        }

        private void NullifySensorCollection()
        {
            foreach (var parent in SensorConfig.AllParentSensors)
            {
                parent.Data.ClearDataValues();
                foreach (var child in parent.ChildNodes)
                {
                    child.Data.ClearDataValues();
                }
            }
        }

        public void BeginAcquisition(int pollIntervalMs)
        {
            //cleanup in case this is a restart
            if (ClientSocket != null)
            {
                ClientSocket.ConnectedToServer -= ClientSocket_ConnectedToServer;
                ClientSocket.MessageReceived -= ClientSocket_MessageReceived;
                ClientSocket.ErrorReceived -= ClientSocket_ErrorReceived;
            }
            
            ClientSocket = new ScsClientSocket(_ipAddress, _portNo);
            ClientSocket.ConnectedToServer += ClientSocket_ConnectedToServer;
            ClientSocket.MessageReceived += ClientSocket_MessageReceived;
            ClientSocket.ErrorReceived += ClientSocket_ErrorReceived;
            //Set the polling interval
            PollIntervalMs = pollIntervalMs;
            while (true)
            {
                IsCommunicating = true;
                //Connects to SCS and gets the current config.xml file
                ClientSocket.ConnectToScsServer();

                Thread.Sleep(ResetDelayMs);
            }
        }

        private bool TryGetSensorConfigFromAcq()
        {
            var configurationRequest =
                "<SCSXMLTemplate><request_command>$SensorDescription</request_command></SCSXMLTemplate>\r\n";
            var xmlResponse = ClientSocket.PollScsServer(configurationRequest, ScsClientSocket.ScsMessageType.SensorConfig, "</SensorList>&#xA;\r\n");

            try
            {
                var xd = XDocument.Parse(xmlResponse);
                //Always modify the sensor collection on the UI thread
                _uiThread.Send(x => {
                    var builder = new ConfigImporter(xd, SensorConfig);
                    //builder.ConfigLockObject = _configLockObject;
                    if (builder.TryBuildFromAcq())
                    {
                        //make sure there's at least one sensor
                        if (!SensorConfig.AllParentSensors.Any())
                            throw new ScsConfigurationException("The SCS configuration does not define any sensors.", xmlResponse);
                        //validate the configuration
                        SensorConfig.ValidateAll();
                        if (SensorConfig.HasErrors)
                            throw new ScsConfigurationException("The SCS configuration has validation errors.", xmlResponse);
                    }
                }, null);
                var settings = new XmlWriterSettings()
                {
                    Indent = true
                };
                //Save a copy of the SensorConfig in the application directory
                using (var writer = XmlWriter.Create(AppDomain.CurrentDomain.BaseDirectory + "\\Sensor.xml", settings))
                {
                    xd.Save(writer);
                }
                return true;
            }
            catch (Exception ex)
            {
                var args = new ScsClientErrorReceivedEventArgs()
                {
                    ErrorMessage = ex.Message,
                    ErrorDetails = xmlResponse,
                    ErrorType = ScsClientErrorReceivedEventArgs.ScsClientErrorType.ConfigError
                };

                ClientSocket_ErrorReceived(this, args);
            }
            return false;
        }

        private void BeginPolling()
        {
            _scsRequestMessage = ClientSocket.BuildScsDataQueryString(SensorConfig);
            while (IsConnected)
            {
                IsCommunicating = true;
                ClientSocket.PollScsServer(_scsRequestMessage, ScsClientSocket.ScsMessageType.SensorData, "</SensorDataList>\r\n");
                Thread.Sleep(PollIntervalMs);
            }
            //socket is not okay
            BeginAcquisition(PollIntervalMs);
        }

        private void BeginSyncronousPolling()
        {
            _scsRequestMessage = ClientSocket.BuildScsDataQueryString(SensorConfig);
            while (IsConnected)
            {
                try
                {
                    var interval = GetInterval();
                    Thread.Sleep(interval);
                }
                catch
                {
                    //todo there's an occasional error that only occurs on startup where the syncinterval returns a negative number
                }
                IsCommunicating = true;
                ClientSocket.PollScsServer(_scsRequestMessage, ScsClientSocket.ScsMessageType.SensorData, "</SensorDataList>\r\n");
            }
            //socket is not okay
            BeginAcquisition(PollIntervalMs);
        }

        TimeSpan GetInterval()
        {
            if (PollIntervalMs >= 60000)
            {
                return SyncronousTimer.GetNextIntervalSpan(PollIntervalMs / 60000,
                    SyncronousTimer.TimeSpanInterval.Minutes);
            }
            if (PollIntervalMs >= 1000)
            {
                return SyncronousTimer.GetNextIntervalSpan(PollIntervalMs / 1000,
                    SyncronousTimer.TimeSpanInterval.Seconds);
            }

            return SyncronousTimer.GetNextIntervalSpan(PollIntervalMs,
                    SyncronousTimer.TimeSpanInterval.Milliseconds);

        }
    }
}
