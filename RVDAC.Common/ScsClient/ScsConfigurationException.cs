﻿using System;

namespace RVDAC.Common.ScsClient
{
    public class ScsConfigurationException: Exception
    {
        public ScsConfigurationException(string errorMessage, string configXml = "")
        {
            Message = errorMessage;
            ConfigXml = configXml;
        }
        public override string Message { get;}
        public string ConfigXml { get; }
    }
}
