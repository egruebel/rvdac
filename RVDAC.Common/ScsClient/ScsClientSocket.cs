﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace RVDAC.Common.ScsClient
{
    public class ScsClientSocket
    {
        public event EventHandler<ScsClientMessageReceivedEventArgs> MessageReceived;
        public event EventHandler<ScsClientErrorReceivedEventArgs> ErrorReceived;
        public event EventHandler ConnectedToServer;

        const int RequestTimeout = 1500;

        public enum ScsMessageType
        {
            SensorConfig, SensorData, TimeStamp
        }

        public ScsClientSocket(string ipAddress, int port)
        {
            IPAddress ip;
            if (!IPAddress.TryParse(ipAddress, out ip))
                throw new ArgumentException("Provided IP address is invalid", nameof(ipAddress));

            ScsServerEndpoint = new IPEndPoint(ip,port);
        }

        public void OnMessageReceived(ScsMessageType msgType, string message)
        {
            var args = new ScsClientMessageReceivedEventArgs {Message = message, MessageType = msgType, ReceiveTime = DateTime.Now};
            MessageReceived?.Invoke(this, args);
        }

        public void OnErrorReceived(Exception ex)
        {
            
            var args = new ScsClientErrorReceivedEventArgs()
            {
                ErrorMessage = ex.Message,
            };

            if (ex is SocketException)
            {
                if (ScsTcpClient.Connected)
                    ScsTcpClient.GetStream().Close();
                args.ErrorType = ScsClientErrorReceivedEventArgs.ScsClientErrorType.SocketError;
            }
            else if (ex is ScsMessageParsingException)
            {
                args.ErrorType = ScsClientErrorReceivedEventArgs.ScsClientErrorType.DataError;
                args.ErrorDetails = ((ScsMessageParsingException) ex).Details;
            }
            else if (ex is ScsConfigurationException)
            {
                if (ScsTcpClient.Connected)
                    ScsTcpClient.GetStream().Close();
                args.ErrorType = ScsClientErrorReceivedEventArgs.ScsClientErrorType.ConfigError;
                args.ErrorDetails = ((ScsConfigurationException)ex).ConfigXml;
            }
            else
            {
                args.ErrorType = ScsClientErrorReceivedEventArgs.ScsClientErrorType.Unknown;
            }
            ErrorReceived?.Invoke(this,args);    
        }

        public void ConnectToScsServer()
        {
            ScsTcpClient = new TcpClient();
            var receivedMessage = "";
            var unexpectedMessage = "Unexpected connection message received after connecting to SCS Acq server";
            var requestTimeout = new System.Timers.Timer { Interval = RequestTimeout };
            requestTimeout.Elapsed += RequestTimeout_Elapsed;
            requestTimeout.Start();
            try
            {
                ScsTcpClient.Connect(ScsServerEndpoint);
                var stream = ScsTcpClient.GetStream();
                while (!receivedMessage.EndsWith("\r\n"))
                {
                    var bytes = new byte[ScsTcpClient.ReceiveBufferSize];
                    var bytesRead = stream.Read(bytes, 0, ScsTcpClient.ReceiveBufferSize);
                    receivedMessage += Encoding.ASCII.GetString(bytes, 0, bytesRead);
                    if (receivedMessage.Length > 50)
                        throw new ScsMessageParsingException(unexpectedMessage, receivedMessage);
                }
                if (string.Equals(receivedMessage, "???$SCS Server connection made\r\n"))
                {
                    requestTimeout.Stop();
                    ConnectedToServer?.Invoke(this, new EventArgs());
                    return;
                }
                throw new ScsMessageParsingException(unexpectedMessage, receivedMessage);
            }
            catch(Exception ex)
            {
                requestTimeout.Stop();
                OnErrorReceived(ex);
            }
        }

        public string BuildScsDataQueryString(SensorConfiguration config)
        {
            //This builds a query string to send to the SCS server to get all parent sensors
            var sb = new StringBuilder();
            sb.Append("<SCSXMLTemplate><request_command>$Get</request_command><SensorNameList>");
            foreach (var ps in config.AllParentSensors)
            {
                sb.Append("<sensor_name>" + ps.Name + "</sensor_name>");
            }
            sb.Append("</SensorNameList></SCSXMLTemplate>\r\n");

            return sb.ToString();
        }

        public string PollScsServer(string requestMessage, ScsMessageType msgType, string eom = "\r\n")
        {
            var requestTimeout = new System.Timers.Timer {Interval = RequestTimeout};
            requestTimeout.Elapsed += RequestTimeout_Elapsed;
            requestTimeout.Start();
            var receivedString = string.Empty;
            var stream = ScsTcpClient.GetStream();
            try
            {
                var bytesToSend = Encoding.ASCII.GetBytes(requestMessage);
                stream.Write(bytesToSend, 0, bytesToSend.Length);
                while (!receivedString.EndsWith(eom))
                {
                    var bytes = new byte[ScsTcpClient.ReceiveBufferSize];
                    var bytesRead = stream.Read(bytes, 0, ScsTcpClient.ReceiveBufferSize);
                    receivedString += Encoding.ASCII.GetString(bytes, 0, bytesRead);
                }
                //Thanks for inserting XML control characters for no reason.....SCS
                receivedString = receivedString.Replace("&#xA;", string.Empty).Replace("&#xD;", string.Empty);
            }
            catch(Exception ex)
            {
                requestTimeout.Stop();
                OnErrorReceived(ex);
            }
            requestTimeout.Stop();
            OnMessageReceived(msgType, receivedString);
            return receivedString;
        }

        private void RequestTimeout_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            var t = (System.Timers.Timer) sender;
            t.Stop();
            OnErrorReceived(new SocketException(10060));
        }

        private IPEndPoint ScsServerEndpoint { get; set; }

        private TcpClient ScsTcpClient { get; set; }
    }
}
