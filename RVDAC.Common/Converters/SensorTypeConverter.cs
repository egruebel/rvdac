﻿using System;
using RVDAC.Common.Models;

namespace RVDAC.Common.Converters
{
    public class SensorTypeConverter: ILegacyConverter
    {
        public object ConvertFromLegacy(object value)
        {
            var type = (string)value;
            switch (type)
            {
                case "NMEA ASYNC PARENT":
                    return NodeBase.DeviceTypeEnum.Nmea;
                case "NMEA ASYNC CHILD":
                    return NodeBase.DeviceTypeEnum.NmeaChild;
                case "SERIAL ASYNC PARENT":
                    return NodeBase.DeviceTypeEnum.Serial;
                case "SERIAL ASYNC CHILD":
                    return NodeBase.DeviceTypeEnum.SerialChild;
                case "DERIVED TRUEWIND PARENT":
                    return NodeBase.DeviceTypeEnum.VirtualTrueWind;
                case "DERIVED TRUEWIND CHILD":
                    return NodeBase.DeviceTypeEnum.VirtualTrueWindChild;
                case "DERIVED AVERAGE PARENT":
                    return NodeBase.DeviceTypeEnum.VirtualAverage;
                case "DERIVED AVERAGE CHILD":
                    return NodeBase.DeviceTypeEnum.VirtualAverageChild;
                default:
                    throw new ArgumentException("Device type of " + type + " is not supported.");
            }
        }

        public object ConvertToLegacy(object value)
        {
            var type = (NodeBase.DeviceTypeEnum)value;
            switch (type)
            {
                case NodeBase.DeviceTypeEnum.Nmea:
                    return "NMEA ASYNC PARENT";
                case NodeBase.DeviceTypeEnum.NmeaChild:
                    return "NMEA ASYNC CHILD";
                case NodeBase.DeviceTypeEnum.Serial:
                    return "SERIAL ASYNC PARENT";
                case NodeBase.DeviceTypeEnum.SerialChild:
                    return "SERIAL ASYNC CHILD";
                case NodeBase.DeviceTypeEnum.VirtualTrueWind:
                    return "DERIVED TRUEWIND PARENT";
                case NodeBase.DeviceTypeEnum.VirtualTrueWindChild:
                    return "DERIVED TRUEWIND CHILD";
                case NodeBase.DeviceTypeEnum.VirtualAverage:
                    return "DERIVED AVERAGE PARENT";
                case NodeBase.DeviceTypeEnum.VirtualAverageChild:
                    return "DERIVED AVERAGE CHILD";
                    
                default:
                    throw new ArgumentException("Device type of " + type + " is not supported.");
            }
        }
    }
}
