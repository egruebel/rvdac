﻿
using System;
using System.IO.Ports;

namespace RVDAC.Common.Converters
{
    public class ParityConverter: ILegacyConverter
    {
        public object ConvertFromLegacy(object value)
        {
            var i = (string) value;
            Parity p;
            if (Enum.TryParse(i, true, out p))
                return p;
            return null;
        }

        public object ConvertToLegacy(object value)
        {
            return (Parity) value;
        }
    }
}
