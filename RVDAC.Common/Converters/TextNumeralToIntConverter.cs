﻿
namespace RVDAC.Common.Converters
{
    public class TextNumeralToIntConverter: ILegacyConverter
    {
        public object ConvertFromLegacy(object value)
        {
            switch ((string)value)
            {
                case "One":
                    return 1;
                case "Two":
                    return 2;
                case "Three":
                    return 3;
                case "Four":
                    return 4;
                case "Five":
                    return 5;
                case "Six":
                    return 6;
                case "Seven":
                    return 7;
                case "Eight":
                    return 8;
            }
            return 8;
        }

        public object ConvertToLegacy(object value)
        {
            switch ((int)value)
            {
                case 1:
                    return "One";
                case 2:
                    return "Two";
                case 3:
                    return "Three";
                case 4:
                    return "Four";
                case 5:
                    return "Five";
                case 6:
                    return "Six";
                case 7:
                    return "Seven";
                case 8:
                    return "Eight";
            }
            return "Eight";
        }
    }
}
