﻿using RVDAC.Common.Models;

namespace RVDAC.Common.Converters
{
    public class DecodeTypeConverter: ILegacyConverter
    {
        public object ConvertFromLegacy(object value)
        {
            var type = (string)value;
            switch (type)
            {
                case "Real*4":
                case "Real*8":
                    return ChildNode.DecodeTypeEnum.Decimal;
                case "ASCII":
                    return ChildNode.DecodeTypeEnum.Text;
                case "Int*4":
                    return ChildNode.DecodeTypeEnum.Integer;
                case "NAVSEC":
                    return ChildNode.DecodeTypeEnum.Navsec;
            }
            return ChildNode.DecodeTypeEnum.Text;
        }

        public object ConvertToLegacy(object value)
        {
            var type = (ChildNode.DecodeTypeEnum)value;
            switch (type)
            {
                case ChildNode.DecodeTypeEnum.Decimal:
                    return "Real*8";
                case ChildNode.DecodeTypeEnum.Integer:
                    return "Int*4";
                case ChildNode.DecodeTypeEnum.Navsec:
                    return "NAVSEC";
                case ChildNode.DecodeTypeEnum.Text:
                    return "ASCII";
            }
            return "No Value";
        }
    }
}
