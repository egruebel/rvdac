﻿
namespace RVDAC.Common.Converters
{
    public interface ILegacyConverter
    {
        object ConvertFromLegacy(object value);

        object ConvertToLegacy(object value);

    }
}
