﻿
using RVDAC.Common.Models;

namespace RVDAC.Common.Converters
{
    public class IoPortTypeConverter: ILegacyConverter
    {
        public object ConvertFromLegacy(object value)
        {
            switch ((string)value)
            {
                case "ComPort":
                    return IoPort.DacPortType.Serial;
                case "UDP Socket":
                    return IoPort.DacPortType.Udp;
                case "TCP Socket":
                    return IoPort.DacPortType.Tcp;
            }
            return IoPort.DacPortType.Serial;
        }

        public object ConvertToLegacy(object value)
        {
            switch ((IoPort.DacPortType)value)
            {
                case IoPort.DacPortType.Serial:
                    return "ComPort";
                case IoPort.DacPortType.Tcp:
                    return "TCP Socket";
                case IoPort.DacPortType.Udp:
                    return "UDP Socket";
            }
            return "ComPort";
        }
    }
}
