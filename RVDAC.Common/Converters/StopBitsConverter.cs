﻿
using System;
using System.IO.Ports;

namespace RVDAC.Common.Converters
{
    public class StopBitsConverter: ILegacyConverter
    {
        public object ConvertFromLegacy(object value)
        {
            var i = (string) value;
            StopBits b;
            if (Enum.TryParse(i, true, out b))
                return b;
            return null;
        }

        public object ConvertToLegacy(object value)
        {
            var i = (StopBits) value;
            return (int) i;
        }
    }
}
