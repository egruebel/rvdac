﻿using System;

namespace RVDAC.Common
{
    public static class SyncronousTimer
    {
        public static DateTime GetNextIntervalDate(int pollingInterval, TimeSpanInterval intervalType)
        {
            var now = DateTime.Now;
            return now + GetNextInterval(pollingInterval, intervalType, now);
        }

        public static TimeSpan GetNextIntervalSpan(int pollingInterval, TimeSpanInterval intervalType)
        {
            return GetNextInterval(pollingInterval, intervalType, DateTime.Now);
        }

        public static TimeSpan GetNextIntervalSpan(int pollingInterval, TimeSpanInterval intervalType, out DateTime outDateTime)
        {
            //return both a timespan and a datetime. 
            outDateTime = DateTime.Now;
            var interval = GetNextInterval(pollingInterval, intervalType, outDateTime);
            outDateTime = outDateTime + interval;
            return interval;
        }

        private static TimeSpan GetNextInterval(int pollingInterval, TimeSpanInterval intervalType, DateTime now)
        {
            var nextInterval = now;
            int remainder;
            switch (intervalType)
            {
                case TimeSpanInterval.Hours:
                    remainder = (24 - now.Hour) % pollingInterval;
                    if (remainder == 0)
                        remainder = pollingInterval;
                    nextInterval = nextInterval.AddHours(remainder);
                    nextInterval = nextInterval.AddMinutes(-now.Minute); //zero out the minutes
                    nextInterval = nextInterval.AddSeconds(-now.Second); //zero out the seconds
                    nextInterval = nextInterval.AddMilliseconds(-now.Millisecond); //zero out the ms
                    break;
                case TimeSpanInterval.Minutes:
                    remainder = (60 - now.Minute) % pollingInterval;
                    if (remainder == 0)
                        remainder = pollingInterval;

                    nextInterval = nextInterval.AddMinutes(remainder);
                    nextInterval = nextInterval.AddSeconds(-now.Second); //zero out the seconds 
                    nextInterval = nextInterval.AddMilliseconds(-now.Millisecond); //zero out the ms
                    break;
                case TimeSpanInterval.Seconds:
                    remainder = ((60 - now.Second) % pollingInterval);
                    if (remainder == 0)
                        remainder = pollingInterval;
                    nextInterval = nextInterval.AddSeconds(remainder);
                    nextInterval = nextInterval.AddMilliseconds(-now.Millisecond); //zero out the ms
                    break;
                case TimeSpanInterval.Milliseconds:
                    remainder = ((1000 - now.Millisecond) % pollingInterval);
                    if (remainder == 0)
                        remainder = pollingInterval;
                    nextInterval = nextInterval.AddMilliseconds(remainder);
                    break;
            }

            
            nextInterval = nextInterval.AddTicks(-(nextInterval.Ticks % TimeSpan.TicksPerSecond)); //Zero out the microseconds
            
            return nextInterval - now;
        }

        
        public enum TimeSpanInterval
        {
            Milliseconds, Seconds, Minutes, Hours
        }
    }
}
