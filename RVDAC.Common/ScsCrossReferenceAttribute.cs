﻿using System;

namespace RVDAC.Common
{
    class ScsCrossReferenceAttribute: Attribute 
    {
        public ScsCrossReferenceAttribute(string referenceTo, Type converterType = null, bool isExtension = false)
        {
            ReferenceTo = referenceTo;
            ValueConverterType = converterType;
            IsExtendedField = isExtension;
        }
        public string ReferenceTo { get; private set; }
        public Type ValueConverterType { get; private set; }
        public bool IsExtendedField { get; private set; }
    }
}
