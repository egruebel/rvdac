﻿using System;
using System.IO.Ports;
using RVDAC.Common.Models;

namespace RVDAC.Common
{
    public static class NodeBuilder
    {
        public static VirtualAverage NewVirtualAverageSensor()
        {
            var name = "New-Average-Sensor";
            var avg = new VirtualAverage()
            {
                Name = name,
                Guid = Guid.NewGuid(),
                DeviceType = NodeBase.DeviceTypeEnum.VirtualAverage,
            };
            avg.ChildNodes.Add(new ChildNode()
            {
                Guid = Guid.NewGuid(),
                Name = name + "-Value",
                DeviceType = NodeBase.DeviceTypeEnum.VirtualAverageChild,
                Parent = avg
            });
            return avg;
        }

        public static VirtualTrueWind NewVirtualTrueWindSensor()
        {
            var name = "New-TrueWind-Sensor";
            var twd = new VirtualTrueWind()
            {
                Name = name,
                Guid = Guid.NewGuid(),
                DeviceType = NodeBase.DeviceTypeEnum.VirtualTrueWind
            };
            twd.ChildNodes.Add(new ChildNode()
            {
                Guid = Guid.NewGuid(),
                Name = name + "-Speed",
                DeviceType = NodeBase.DeviceTypeEnum.VirtualTrueWindChild,
                Parent = twd
            });
            twd.ChildNodes.Add(new ChildNode()
            {
                Guid = Guid.NewGuid(),
                Name = name + "-Direction",
                DeviceType = NodeBase.DeviceTypeEnum.VirtualTrueWindChild,
                Parent = twd
            });
            return twd;
        }

        public static IoPort NewIoPort()
        {
            return new IoPort()
            {
                PortType = IoPort.DacPortType.Serial,
                Guid = Guid.NewGuid(),
                ComPort = "COM1",
                DataBits = 8,
                BaudRate = 4800,
                Parity = Parity.None,
                StopBits = StopBits.One
            };
        }

        public static PhysicalParentNode NewSerialParentSensor()
        {
            //add a sample sensor
            var parent = new PhysicalParentNode()
            {
                Guid = Guid.NewGuid(),
                DeviceType = NodeBase.DeviceTypeEnum.Serial,
                Name = "New-Serial-Sensor",
                SentenceLength = 35
            };
            //add a child
            parent.ChildNodes.Add(new ChildNode()
            {
                Guid = Guid.NewGuid(),
                Parent = parent,
                Name = parent.Name + "-Child",
                StartIndex = 1,
                EndIndex = 4,
                DeviceType = NodeBase.DeviceTypeEnum.SerialChild
            });

            return parent;
        }

        public static PhysicalParentNode NewNmeaParentSensor()
        {
            //add a sample sensor
            var parent = new PhysicalParentNode()
            {
                Guid = Guid.NewGuid(),
                DeviceType = NodeBase.DeviceTypeEnum.Nmea,
                Name = "New-NMEA-Sensor",
                Timeout = 4,
                EndOfString = 10,
                SentenceQualifier = "$GPGGA"
            };
            //add a child
            parent.ChildNodes.Add(new ChildNode()
            {
                Guid = Guid.NewGuid(),
                Parent = parent,
                Name = parent.Name + "-Child",
                DataFieldPosition = 1,
                DeviceType = NodeBase.DeviceTypeEnum.NmeaChild
            });

            return parent;
        }
    }
}
