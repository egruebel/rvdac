﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using RVDAC.Common.Converters;


namespace RVDAC.Common.Models
{
    public abstract class NodeBase: ValidatableModel
    {
        private string _name, _make, _model, _serialNo, _legacyName, _positionDescription, _comment, _description;
        private int? _positionX, _positionY, _positionZ;
        private bool _enabled;
        private Guid _guid;
        private DeviceTypeEnum _deviceType;
        

        [ScsCrossReference("SensorGuid",isExtension:true)]
        public Guid Guid
        {
            get { return _guid; }
            set
            {
                //GUID Should never be set more than once
                //if (Guid != Guid.Empty)
                    //throw new InvalidOperationException("An existing GUID cannot be replaced.");
                SetProperty(ref _guid, value);
            }
        }

        [Required,MinLength(6)]
        [ScsCrossReference("SensorName")]
        public virtual string Name
        {
            get { return _name; }
            set
            {
                //Allow 5 characters to prevent NULL and trigger a validation error (minimum 6)
                if (value.Length >= 5)
                {
                    SetProperty(ref _name, value);
                }
                else if (_name.Length >= 5)
                {
                    //value is less than 5 characters
                    SetProperty(ref _name, _name.Substring(0, 5));
                }
                else
                {
                    SetProperty(ref _name, "node1");
                }
            }
        }

        [ScsCrossReference("SensorType",typeof(SensorTypeConverter))]
        public DeviceTypeEnum DeviceType
        {
            get { return _deviceType; }
            set { SetProperty(ref _deviceType, value); }
        }
        
        public string LegacyName
        {
            get { return _legacyName; }
            set { SetProperty(ref _legacyName, value); }
        }

        public short Id { get; set; }

        [ScsCrossReference("SensorComment")]
        public string Comment
        {
            get { return _comment; }
            set { SetProperty(ref _comment, value); }
        }

        [ScsCrossReference("SensorDescription", isExtension: true)]
        public string Description
        {
            get { return _description; }
            set { SetProperty(ref _description, value); }
        }

        [ScsCrossReference("Manufacturer", isExtension: true)]
        public string Make
        {
            get { return _make; }
            set { SetProperty(ref _make, value); }
        }

        [ScsCrossReference("ModelNumber", isExtension: true)]
        public string Model
        {
            get { return _model; }
            set { SetProperty(ref _model, value); }
        }

        [ScsCrossReference("SerialNumber", isExtension: true)]
        public string Serial
        {
            get { return _serialNo; }
            set { SetProperty(ref _serialNo, value); }
        }

        [CustomValidation(typeof(NodeBase), "PositionValidation")]
        [ScsCrossReference("PositionX", isExtension: true)]
        public int? PositionX
        {
            get { return _positionX; }
            set { SetProperty(ref _positionX, value); }
        }

        [CustomValidation(typeof(NodeBase), "PositionValidation")]
        [ScsCrossReference("PositionY", isExtension: true)]
        public int? PositionY
        {
            get { return _positionY; }
            set { SetProperty(ref _positionY, value); }
        }

        [CustomValidation(typeof(NodeBase), "PositionValidation")]
        [ScsCrossReference("PositionZ", isExtension: true)]
        public int? PositionZ
        {
            get { return _positionZ; }
            set { SetProperty(ref _positionZ, value); }
        }

        [ScsCrossReference("PositionDescription", isExtension: true)]
        public string PositionDescription
        {
            get { return _positionDescription; }
            set { SetProperty(ref _positionDescription, value); }
        }

        [ScsCrossReference("SensorEnabled")]
        public bool Enabled
        {
            get { return _enabled; }
            set { SetProperty(ref _enabled, value); }
        }

        public bool IsVirtual
        {
            get
            {
                return DeviceType == DeviceTypeEnum.VirtualAverage ||
                       DeviceType == DeviceTypeEnum.VirtualTrueWind;
            }
        }
        
        public NodeDataObject Data { get; set; } = new NodeDataObject();

        public bool IsVirtualChild
        {
            get
            {
                return DeviceType == DeviceTypeEnum.VirtualAverageChild ||
                       DeviceType == DeviceTypeEnum.VirtualTrueWindChild;
            }
        }

        public enum DeviceTypeEnum
        {
            Nmea, NmeaChild, Serial, SerialChild, VirtualTrueWind, VirtualTrueWindChild, VirtualAverage, VirtualAverageChild
        }

        public static ValidationResult PositionValidation(object obj, ValidationContext context)
        {
            var nodeBase = (NodeBase)context.ObjectInstance;
            var nullFields = new List<string>();
            if(nodeBase.PositionX == null)
                nullFields.Add("PositionX");
            if(nodeBase.PositionY == null)
                nullFields.Add("PositionY");
            if(nodeBase.PositionZ == null)
                nullFields.Add("PositionZ");

            if (nullFields.Any() && nullFields.Count < 3)
            {
                return new ValidationResult("All position fields are required",nullFields);
            }
            return ValidationResult.Success;
        }

    }
}
