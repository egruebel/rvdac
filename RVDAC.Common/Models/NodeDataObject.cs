﻿using System;
using System.ComponentModel;
using System.Diagnostics;

namespace RVDAC.Common.Models
{
    public class NodeDataObject: INotifyPropertyChanged
    {
        private string _rawValue;
        private dynamic _decodedValue;
        private bool _hasChildError;
        //private bool _isTimedOut;
        private ErrorCodeEnum _errorCode;
        public event PropertyChangedEventHandler PropertyChanged;

        public enum ErrorCodeEnum
        {
            None, SensorInvalid, NoDataReceived, RawValueParsingError, DecodedValueParsingError, TimedOut, BadChecksum
        }

        private static readonly string[] ErrorCodeDisplayValue = { "None", "Sensor Invalid", "No Data Received", "Raw Value Parsing Error", "Decoded Value Parsing Error", "Timed Out", "Bad Checksum" };

        public string RawValue
        {
            get { return _rawValue; }
            set
            {
                if (_rawValue != value)
                {
                    _rawValue = value;
                    OnPropertyChanged(nameof(RawValue));
                    OnPropertyChanged(nameof(DisplayValue));
                }
            }
        }

        public string DisplayValue
        {
            get
            {
                if (HasError)
                    return $"-{ErrorCodeDisplayValue[(int)ErrorCode]}-{RawValue}";
                if (HasChildError)
                    return $"-Child Sensor Error-{RawValue}";
                if (string.IsNullOrEmpty(RawValue))
                    return "-NULL-";
                return RawValue;
            }
        }

        public bool HasError { get; private set; }

        public bool HasChildError {
            get
            {
                return _hasChildError;
            }
            set
            {
                if (_hasChildError != value)
                {
                    _hasChildError = value;
                    OnPropertyChanged(nameof(HasChildError));
                }
            }
        }

        public void ClearDataValues()
        {
            RawValue = null;
            DecodedValue = null;
        }

        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this,new PropertyChangedEventArgs(name));
        }

        public dynamic DecodedValue
        {
            get { return _decodedValue; }
            set
            {
                if (_decodedValue != value)
                {
                    _decodedValue = value;
                    OnPropertyChanged(nameof(DecodedValue));
                }
            }
        }

        public DateTime TimeStamp { get; set; }

        public ErrorCodeEnum ErrorCode
        {
            get { return _errorCode; }
            set
            {
                if (_errorCode != value)
                {
                    _errorCode = value;
                    HasError = _errorCode != ErrorCodeEnum.None;
                    OnPropertyChanged(nameof(ErrorCode));
                    OnPropertyChanged(nameof(HasError));
                    OnPropertyChanged(nameof(DisplayValue));
                }
            }
        }

    }
}

