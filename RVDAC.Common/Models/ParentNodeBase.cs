﻿using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Collections.Specialized;

namespace RVDAC.Common.Models
{
    public abstract class ParentNodeBase: NodeBase
    {
        private string _namePrefix;
        private bool _childHasErrors;
        private bool _childIsDirty;
        private int _timeout;
        private string _sample;

        public ObservableCollection<ChildNode> ChildNodes { get; set; }

        protected ParentNodeBase()
        {
            ChildNodes = new ObservableCollection<ChildNode>();
            ChildNodes.CollectionChanged += ChildNodes_CollectionChanged;
        }

        [Range(1,1000)]
        [ScsCrossReference("Timeout")]
        public int Timeout
        {
            get { return _timeout; }
            set { SetProperty(ref _timeout, value); }
        }

        public string NamePrefix
        {
            get { return _namePrefix;}
            set { SetProperty(ref _namePrefix, value); }
        }

        [ScsCrossReference("SampleMessage", isExtension: true)]
        public string SampleMessage
        {
            get { return _sample; }
            set { SetProperty(ref _sample, value); }
        }

        private void ChildNodes_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (var item in e.NewItems)
                {
                    var child = (ChildNode)item;
                    child.ModelDirtyChanged += Child_ModelDirtyChanged;
                    child.HasErrorsChanged += Child_HasErrorsChanged;
                }
            }
            if (e.OldItems != null)
            {
                foreach (var item in e.OldItems)
                {
                    var child = (ChildNode)item;
                    child.ModelDirtyChanged -= Child_ModelDirtyChanged;
                    child.HasErrorsChanged -= Child_HasErrorsChanged;
                }
            }
            IsDirty = true;
        }

        private void Child_HasErrorsChanged(object sender, bool hasErrors)
        {
            ChildHasErrors = hasErrors;
        }

        private void Child_ModelDirtyChanged(object sender, bool isDirty)
        {
            ChildIsDirty = isDirty;
            //This parent object is now also dirty. This is also a silly thing to show people. 
            IsDirty = true;
        }

        //Forces a validation error if any child has errors
        [Range(typeof(bool),"False","False",ErrorMessage="One or more child objects has validation errors")] 
        public bool ChildHasErrors
        {
            get { return _childHasErrors; }
            set { SetProperty(ref _childHasErrors, value); }
        }
        
        public bool ChildIsDirty
        {
            get{ return _childIsDirty; }
            set { SetProperty(ref _childIsDirty, value); }
        }
    }
}
