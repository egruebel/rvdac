﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using RVDAC.Common.Converters;

namespace RVDAC.Common.Models
{
    public class ChildNode: NodeBase
    {
        private int? _startIndex, _endIndex, _fieldPosition, _unitPosition;
        private string _units, _measurement; //todo add support for delimiter
        private DecodeTypeEnum _decodeType;
        private ParentNodeBase _parent;

        public ChildNode()
        {
        }

        public ChildNode(ParentNodeBase parent)
        {
            _parent = parent;
        }

        public enum DecodeTypeEnum
        {
            Text, Integer, Decimal, Navsec
        }

        public bool IsIndependentSensor()
        {
            if (PositionX != null)
                return true;
            if (!string.IsNullOrEmpty(Make))
                return true;
            if (!string.IsNullOrEmpty(Model))
                return true;
            if (!string.IsNullOrEmpty(Serial))
                return true;
            return false;
        }

        [Required]
        [ScsCrossReference("DecodeType",typeof(DecodeTypeConverter))]
        public DecodeTypeEnum DecodeType
        {
            get { return _decodeType; }
            set { SetProperty(ref _decodeType, value);}
        }

        [Obsolete]
        [ScsCrossReference("ParentSensorName")]
        public string ParentSensorName
        {
            get { return Parent.Name; }
        }

        [CustomValidation(typeof(ChildNode), "DeviceTypeBasedValidation")]
        [ScsCrossReference("StartChar")]
        public int? StartIndex
        {
            get { return _startIndex; }
            set { SetProperty(ref _startIndex, value); }
        }

        [CustomValidation(typeof(ChildNode), "DeviceTypeBasedValidation")]
        [ScsCrossReference("StopChar")]
        public int? EndIndex
        {
            get { return _endIndex; }
            set { SetProperty(ref _endIndex, value); }
        }

        [CustomValidation(typeof(ChildNode), "DeviceTypeBasedValidation")]
        [ScsCrossReference("DataFieldPosition")]
        public int? DataFieldPosition
        {
            get { return _fieldPosition; }
            set { SetProperty(ref _fieldPosition, value); }
        }

        [CustomValidation(typeof(ChildNode), "DeviceTypeBasedValidation")]
        [ScsCrossReference("UnitsFieldPosition")]
        public int? UnitFieldPosition
        {
            get { return _unitPosition; }
            set { SetProperty(ref _unitPosition, value); }
        }

        [ScsCrossReference("Units")]
        public string Units
        {
            get { return _units; }
            set { SetProperty(ref _units, value); }
        }

        [ScsCrossReference("MeasurementField",isExtension:true)]
        public string Measurement
        {
            get { return _measurement; }
            set { SetProperty(ref _measurement, value); }
        }

        public ParentNodeBase Parent
        {
            get { return _parent ?? new PhysicalParentNode(); }
            set { _parent = value; }
        }

        public static ValidationResult DeviceTypeBasedValidation(object obj, ValidationContext context)
        {
            var child = (ChildNode)context.ObjectInstance;
           
            if (child.DeviceType == DeviceTypeEnum.NmeaChild)
            {
                //only the data field position is required
                if(child.DataFieldPosition == null)
                    return new ValidationResult("Data field position is required",new List<string>{"DataFieldPosition"});
            }

            if (child.DeviceType == DeviceTypeEnum.SerialChild)
            {
                //a start and end field is required
                if(child.StartIndex == null || child.EndIndex == null)
                    return new ValidationResult("Start Index and End Index are required", new List<string> { "StartIndex","EndIndex" });
                if(child.EndIndex <= child.StartIndex)
                    return new ValidationResult("Start Index must be less than End Index", new List<string> { "StartIndex", "EndIndex" });
            }

            if (child.DecodeType == DecodeTypeEnum.Navsec)
            {
                if(child.UnitFieldPosition == null)
                    return new ValidationResult("Units Field is required", new List<string> { "UnitFieldPosition" });
            }
            return ValidationResult.Success;
        }
    }
}
