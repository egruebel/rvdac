﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace RVDAC.Common.Models
{
    public class VirtualNodeSource: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private ChildNode _source;

        [Required]
        public ChildNode Source {
            get { return _source; }
            set {
                _source = value;
                OnPropertyChanged(nameof(Source));
            }
        }

        public List<ChildNode.DecodeTypeEnum> AcceptableTypes { get; set; }
        
        public string RequiredMeasurementClass { get; set; } 

        public string Name { get; set; }

        public string AcceptableTypesToString()
        {
            var s = string.Empty;
            for (int i = 0; i < AcceptableTypes.Count; i++)
            {
                s += AcceptableTypes[i];
                if (i != AcceptableTypes.Count - 1)
                    s += " or ";
            }
            return s;
        }

        public void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this,new PropertyChangedEventArgs(name)); 
        }
    }
}
