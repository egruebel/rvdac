﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using RVDAC.Common.Converters;

namespace RVDAC.Common.Models
{
    public sealed class VirtualAverage: VirtualParentBase
    {
        private Average _average;
        private AverageDomain _averageMode;
        private int? _averageModeSamples, _precision;

        public VirtualAverage()
        {
            PopulateSourceCollection();
        }

        public override void PopulateSourceCollection()
        {
            var acceptableDecodes = new List<ChildNode.DecodeTypeEnum>
                {
                    ChildNode.DecodeTypeEnum.Integer, ChildNode.DecodeTypeEnum.Decimal
                };
            SourceCollection.Add(new VirtualNodeSource
            {
                AcceptableTypes = acceptableDecodes,
                Name = "Source"
            });
        }

        public override void UpdateChildNodes()
        {
            //inherited properties = comment, units, datafieldposition
            if (!ChildNodes.Any())
                return;
            if(ChildNodes.Count() > 1)
                throw new InvalidDataException("Virtual average types may only have one child node");
            ChildNodes[0].DataFieldPosition = 1;
            ChildNodes[0].Name = Name + "-Value";
            ChildNodes[0].DecodeType = ChildNode.DecodeTypeEnum.Decimal;
            ChildNodes[0].Units = Units;
            ChildNodes[0].Comment = Comment;
        }

        public enum Average
        {
            Arithmetic, Polar
        }

        public enum AverageDomain
        {
            Time, Samples
        }

        [Required]
        [ScsCrossReference("AverageType")]
        public Average AverageType
        {
            get { return _average;}
            set { SetProperty(ref _average, value); }
        }

        [Required]
        public AverageDomain AverageMode
        {
            get { return _averageMode;}
            set { SetProperty(ref _averageMode, value); }
        }

        [Obsolete]
        [ScsCrossReference("NumberOfSamples")] //This field only exists because SCS decided to be cute. For XML serialization only
        public int? NumberOfSamples
        {
            get
            {
                if (AverageMode == AverageDomain.Samples)
                    return AverageSampleSize;
                return -1*AverageSampleSize;
            }
        }

        [Required]
        [Range(1,5000)]
        public int? AverageSampleSize
        {
            get { return _averageModeSamples; }
            set { SetProperty(ref _averageModeSamples, value); }
        }

        [Required]
        [Range(1,8)]
        [ScsCrossReference("Precision",typeof(TextNumeralToIntConverter))]
        public int? Precision
        {
            get { return _precision; }
            set { SetProperty(ref _precision, value); }
        }
    }
}
