﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO.Ports;
using System.Linq;
using System.Net;
using RVDAC.Common.Converters;

namespace RVDAC.Common.Models
{
    public class IoPort: ValidatableModel
    {
        private DacPortType _portType;
        private string _name, _comPort, _ipAddress;
        private int? _baudRate, _dataBits, _portNo;
        private Parity _parity;
        private StopBits _stopBits;
        private Guid _guid = Guid.NewGuid();

        public enum DacPortType
        {
            Serial, Tcp, Udp //in the future add http
        }

        [ScsCrossReference("ConnectionType",typeof(IoPortTypeConverter))]
        public DacPortType PortType
        {
            get { return _portType;}
            set { SetProperty(ref _portType, value); }
        }

        public string Name
        {
            get { return _name ?? string.Format("New {0} Port",PortType); }
            private set { SetProperty(ref _name, value); }
        }

        public Guid Guid
        {
            get { return _guid; }
            set { SetProperty(ref _guid,value); }
        }

        private void GeneratePortName()
        {
            switch (PortType)
            {
                case DacPortType.Serial:
                    Name = string.Format("{0}- {1},{2},{3},{4}", ComPort, BaudRate, DataBits, Parity, StopBits);
                    break;
                case DacPortType.Tcp:
                    Name = string.Format("TCP- {0}:{1}",IpAddress,PortNo);
                    break;
                case DacPortType.Udp:
                    Name = string.Format("UDP- {0}:{1}",IpAddress,PortNo);
                    break;
            }
        }

        [CustomValidation(typeof(IoPort), "IoPortCustomValidation")]
        [ScsCrossReference("Device")]
        public string ComPort
        {
            get { return _comPort; }
            set
            {
                SetProperty(ref _comPort, value);
                GeneratePortName();
            }
        }

        [CustomValidation(typeof(IoPort), "IpAddressValidation")]
        [CustomValidation(typeof(IoPort), "IoPortCustomValidation")]
        [ScsCrossReference("NetAddress")]
        public string IpAddress
        {
            get { return _ipAddress; }
            set
            {
                SetProperty(ref _ipAddress, value);
                GeneratePortName();
            }
        }

        [CustomValidation(typeof(IoPort), "IoPortCustomValidation")]
        [ScsCrossReference("BaudRate")]
        public int? BaudRate
        {
            get { return _baudRate;}
            set
            {
                SetProperty(ref _baudRate, value);
                GeneratePortName();
            }
        }

        [CustomValidation(typeof(IoPort), "IoPortCustomValidation")]
        [ScsCrossReference("DataBits",typeof(TextNumeralToIntConverter))]
        public int? DataBits
        {
            get { return _dataBits; }
            set
            {
                SetProperty(ref _dataBits, value);
                GeneratePortName();
            }
        }

        [Range(1,65535)]
        [CustomValidation(typeof(IoPort), "IoPortCustomValidation")]
        [ScsCrossReference("Socket")]
        public int? PortNo
        {
            get { return _portNo; }
            set
            {
                SetProperty(ref _portNo, value);
                GeneratePortName();
            }
        }

        [CustomValidation(typeof(IoPort), "IoPortCustomValidation")]
        [ScsCrossReference("Parity",typeof(ParityConverter))]
        public Parity Parity
        {
            get { return _parity; }
            set
            {
                SetProperty(ref _parity, value);
                GeneratePortName();
            }
        }

        [CustomValidation(typeof(IoPort), "IoPortCustomValidation")]
        [ScsCrossReference("StopBits",typeof(StopBitsConverter))]
        public StopBits StopBits
        {
            get { return _stopBits; }
            set
            {
                SetProperty(ref _stopBits, value);
                GeneratePortName();
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public IoPort CopyOf()
        {
            return (IoPort)MemberwiseClone();
        }

        public override bool Equals(object obj)
        {
            IoPort port;
            try
            {
                port = (IoPort)obj;
                if (PortType != port.PortType)
                    return false;

                switch (port.PortType)
                {
                    case DacPortType.Serial:
                        if (string.Equals(port.ComPort, ComPort))
                            return true;
                        break;
                    case DacPortType.Tcp:
                    case DacPortType.Udp:
                        if (port.PortNo != PortNo)
                            return false;
                        if (port.IpAddress.Equals(IpAddress))
                            return true;
                        break;
                }
                return false;
            }
            catch
            {
                return false;
            }
            
        }

        public static ValidationResult IoPortCustomValidation(object obj, ValidationContext context)
        {
            var ioPort = (IoPort)context.ObjectInstance;
            var invalidFields = new List<string>();
            var message = "";
            switch (ioPort.PortType)
            {
                case DacPortType.Serial:
                    message = "All serial port fields are required";
                    if (string.IsNullOrWhiteSpace(ioPort.ComPort))
                        invalidFields.Add("ComPort");
                    if(ioPort.BaudRate == null)
                        invalidFields.Add("BaudRate");
                    if (ioPort.DataBits == null)
                        invalidFields.Add("DataBits");
                    break;
                case DacPortType.Tcp:
                case DacPortType.Udp:
                    message = "IP address and port number are required";
                    if(string.IsNullOrWhiteSpace(ioPort.IpAddress))
                        invalidFields.Add("IpAddress");
                    if(ioPort.PortNo == null)
                        invalidFields.Add("PortNo");
                        break;
            }
            if(invalidFields.Any())
                return new ValidationResult(message, invalidFields);

            return ValidationResult.Success;
        }

        public static ValidationResult IpAddressValidation(object obj, ValidationContext context)
        {
            var ioPort = (IoPort) context.ObjectInstance;
            if (ioPort.PortType == DacPortType.Serial)
                return ValidationResult.Success;
            IPAddress ip;
            if(IPAddress.TryParse(ioPort.IpAddress, out ip))
                return ValidationResult.Success;

            return new ValidationResult("IP Address is not valid", new List<string> { "IpAddress" });
        }
    }
}
