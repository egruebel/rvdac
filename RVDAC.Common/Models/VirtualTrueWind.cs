﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace RVDAC.Common.Models
{
    public sealed class VirtualTrueWind: VirtualParentBase
    {
        public VirtualTrueWind()
        {
            PopulateSourceCollection();
        }

        public override void PopulateSourceCollection()
        {
            var fields = new Dictionary<string, string>()
            {
                {"Wind Speed","WindSpeedRelative"},
                {"Wind Direction", "WindDirectionRelative" },
                {"Ship Track", "Track" },
                {"Ground Speed", "GroundSpeed" },
                {"Heading", "Heading" }
            };
            var acceptableDecodes = new List<ChildNode.DecodeTypeEnum>
                {
                    ChildNode.DecodeTypeEnum.Integer, ChildNode.DecodeTypeEnum.Decimal
                };
            foreach (var f in fields)
            {
                SourceCollection.Add(new VirtualNodeSource()
                {
                    AcceptableTypes = acceptableDecodes,
                    Name = f.Key,
                    RequiredMeasurementClass = f.Value
                });
            }
        }

        public override void UpdateChildNodes()
        {
            //inherited properties = comment, units, datafieldposition
            if (ChildNodes.Count() != 2)
                return;
            ChildNodes[0].DataFieldPosition = 1;
            ChildNodes[0].DecodeType = ChildNode.DecodeTypeEnum.Decimal;
            ChildNodes[0].Name = Name + "-Speed";
            ChildNodes[0].Units = Units; //only for speed
            ChildNodes[0].Measurement = "TrueWindSpeed";
            ChildNodes[0].Comment = Comment;

            ChildNodes[1].DataFieldPosition = 2;
            ChildNodes[1].DecodeType = ChildNode.DecodeTypeEnum.Decimal;
            ChildNodes[1].Units = "Deg";
            ChildNodes[1].Name = Name + "-Direction";
            ChildNodes[1].Measurement = "TrueWindDirection";
            ChildNodes[1].Comment = Comment;
        }
    }
}
