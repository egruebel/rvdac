﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace RVDAC.Common.Models
{
    public abstract class VirtualParentBase: ParentNodeBase
    {
        private bool _sourceCollectionError;
        
        protected VirtualParentBase()
        {
            SourceCollection = new ObservableCollection<VirtualNodeSource>();
            SourceCollection.CollectionChanged += SourceCollection_CollectionChanged;
            PropertyChanged += VirtualParentBase_PropertyChanged;
        }

        public abstract void PopulateSourceCollection();

        public abstract void UpdateChildNodes();

        public bool SourceCollectionError
        {
            get { return _sourceCollectionError;}
            set { SetProperty(ref _sourceCollectionError, value); }
        }

        private void VirtualParentBase_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            UpdateChildNodes();
            ValidateAsync();
        }

        private void ValidateSourceCollection()
        {
            
        }

        private void SourceCollection_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (var item in e.NewItems)
                {
                    var src = (VirtualNodeSource)item;
                    src.PropertyChanged += SourceItem_PropertyChanged;
                    if (src.Source != null)
                        src.Source.HasErrorsChanged += BaseNode_HasErrorsChanged;
                }
            }
            if (e.OldItems != null)
            {
                foreach (var item in e.OldItems)
                {
                    var src = (VirtualNodeSource)item;
                    src.PropertyChanged -= SourceItem_PropertyChanged;
                    if (src.Source != null)
                        src.Source.HasErrorsChanged -= BaseNode_HasErrorsChanged;
                }
            }
            UpdateChildNodes();
        }

        private void SourceItem_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            OnPropertyChanged(nameof(SourceCollection));
        }

        private void BaseNode_HasErrorsChanged(object sender, bool hasErrors)
        {
            ChildHasErrors = hasErrors;
        }

        [ScsCrossReference("Units")]
        public string Units
        {
            get
            {
                //This only works because conveniently BaseSensor1 sets the units for both derived types
                if (SourceCollection.Any() && SourceCollection[0].Source != null)
                    return SourceCollection[0].Source.Units;
                return null;
            }
        }
        
        public struct VirtualSourceRequirement
        {
            public VirtualSourceRequirement(List<ChildNode.DecodeTypeEnum> decodeTypes, bool requireMeasurementClass,
                string measurementClass = "")
            {
                RequireMeasurementClass = requireMeasurementClass;
                DecodeTypes = decodeTypes;
                MeasurementClass = measurementClass;
            }
            public bool RequireMeasurementClass { get; set; }
            public string MeasurementClass { get; set; }
            public List<ChildNode.DecodeTypeEnum> DecodeTypes { get; set; }
        }

        [CustomValidation(typeof(VirtualParentBase), "SourceCollectionValidation")]
        public ObservableCollection<VirtualNodeSource> SourceCollection { get; set; }

        
        public static ValidationResult SourceCollectionValidation(object obj, ValidationContext context)
        {
            var vp = (VirtualParentBase) context.ObjectInstance;
            var errorField = new List<string>() {"SourceCollection"};
            foreach (var vns in vp.SourceCollection)
            {
                var src = vns.Source;
                if (src == null)
                    return new ValidationResult(vns.Name + " is required", errorField);

                if(!string.IsNullOrEmpty(vns.RequiredMeasurementClass) && src.Measurement != vns.RequiredMeasurementClass)
                    return new ValidationResult("The field " + vns.Name + " requires an input with the measurement class " + vns.RequiredMeasurementClass, errorField);

                if(!vns.AcceptableTypes.Contains(src.DecodeType))
                    return new ValidationResult("The field " + vns.Name + " requires an input with a decode type of " + vns.AcceptableTypesToString(), errorField);
            }

            return ValidationResult.Success;
        }
    }
}
