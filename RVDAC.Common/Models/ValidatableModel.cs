﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace RVDAC.Common.Models
{
    public abstract class ValidatableModel: INotifyPropertyChanged,INotifyDataErrorInfo
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler<PropertyChangingEventArgs> PropertyChanging;
        private ConcurrentDictionary<string, List<string>> _errors =
        new ConcurrentDictionary<string, List<string>>();
        [XmlIgnore]
        private List<string> _validationSummary = new List<string>();
        private bool _hasErrors;
        private bool _modelDirty;
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
        public event EventHandler<bool> HasErrorsChanged;
        public event EventHandler<bool> ValidationComplete;
        public event EventHandler<bool> ModelDirtyChanged;
        public event EventHandler ValidationStarted;

        private void PopulateErrorSummary()
        {
            if (Errors == null || Errors.Count == 0)
            {
                //no errors
                ValidationSummary.Clear();
                HasErrors = false;
                return;
            }
            HasErrors = true;
            //check if the errors have changed
            var errors = Errors.Select(x => x.Value).SelectMany(x => x).Distinct().OrderBy(x => x).ToList();
            if (!errors.SequenceEqual(_validationSummary))
            {
                _validationSummary = errors;
                OnPropertyChanged(nameof(ValidationSummary));
            }
        }

        [XmlIgnore]
        public List<string> ValidationSummary
        {
            get { return _validationSummary; }
            set { _validationSummary = value; }
        } 

        protected bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = null)
        {
            if (Equals(storage, value))
            {
                return false;
            }
            //set the value
            OnPropertyChanging(storage, value, propertyName);
            storage = value;
            //set the model dirtiness
            if (propertyName != "IsDirty")
            {
                IsDirty = true;
            }
            //raise the property changed event
            OnPropertyChanged(propertyName);
            return true;
        }

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var eventHandler = PropertyChanged;
            if (eventHandler != null)
            {
                eventHandler(this, new PropertyChangedEventArgs(propertyName));
                ValidateAsync();
            }
        }

        protected void OnPropertyChanging(object oldValue,
            object newValue, [CallerMemberName] string propertyName = null )
        {
            PropertyChanging?.Invoke(this,new PropertyChangingEventArgs()
            {
                NewValue = newValue,
                OldValue = oldValue,
                PropertyName = propertyName
            });
        }

        [XmlIgnore]
        public ConcurrentDictionary<string, List<string>> Errors
        {
            get { return _errors; }
            private set { _errors = value; }
        }

        public void OnErrorsChanged(string propertyName)
        {
            ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));
        }

        public void OnHasErrorsChanged(bool hasErrors)
        {
            HasErrorsChanged?.Invoke(this,hasErrors);
        }

        public void OnModelDirtyChanged(bool isDirty)
        {
            ModelDirtyChanged?.Invoke(this,isDirty);
        }

        public void OnValidationStarted()
        {
            ValidationStarted?.Invoke(this, new EventArgs());
        }

        public void OnValidationComplete(bool hasErrors)
        {
            ValidationComplete?.Invoke(this, hasErrors);
        }

        public IEnumerable GetErrors(string propertyName)
        {
            List<string> errorsForName;
            if(propertyName == null)
                return new List<string>();
            _errors.TryGetValue(propertyName, out errorsForName);
            return errorsForName;
        }

        [XmlIgnore]
        public bool HasErrors
        {
            get { return _hasErrors; }
            private set
            {
                SetProperty(ref _hasErrors, value); 
                OnHasErrorsChanged(value);
            }
        }

        [XmlIgnore]
        public bool IsDirty
        {
            get { return _modelDirty; }
            set
            {
                SetProperty(ref _modelDirty, value); 
                OnModelDirtyChanged(value);
            }
        }

        public async void ValidateAsync()
        {
            OnValidationStarted();
            await Task.Run(() => ValidateAll());
            PopulateErrorSummary();
            OnValidationComplete(HasErrors);
        }

        public void Validate()
        {
            OnValidationStarted();
            ValidateAll();
            PopulateErrorSummary();
            OnValidationComplete(HasErrors);
        }

        private readonly object _lock = new object();

        private bool ValidateAll()
        {
            lock (_lock)
            {
                var validationContext = new ValidationContext(this, null, null);
                var validationResults = new List<ValidationResult>();
                Validator.TryValidateObject(this, validationContext, validationResults, true);

                //Remove errors that no longer exist
                foreach (var kv in _errors.ToList())
                {
                    if (validationResults.All(r => r.MemberNames.All(m => m != kv.Key)))
                    {
                        List<string> outLi;
                        _errors.TryRemove(kv.Key, out outLi);
                        OnErrorsChanged(kv.Key);
                    }
                }

                var q = from r in validationResults
                        from m in r.MemberNames
                        group r by m into g
                        select g;
                
                //Add new errors
                foreach (var prop in q)
                {
                    var messages = prop.Select(r => r.ErrorMessage).ToList();
                    if (_errors.ContainsKey(prop.Key))
                    {
                        List<string> outLi;
                        _errors.TryRemove(prop.Key, out outLi);
                    }
                    _errors.TryAdd(prop.Key, messages);
                    OnErrorsChanged(prop.Key);
                }
            }
            return true;
        }
    }
}
