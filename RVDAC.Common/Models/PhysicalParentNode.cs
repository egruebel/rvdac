﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RVDAC.Common.Models
{
    public class PhysicalParentNode: ParentNodeBase
    {
        private int? _endOfString; //decimal value of ASCII EOS
        private int? _sentenceLength;
        private string _sentenceQualifier;
        private IoPort _ioPort;

        [Required]
        [ScsCrossReference("TermChar")]
        public int? EndOfString
        {
            get { return _endOfString; }
            set { SetProperty(ref _endOfString, value); }
        }

        [CustomValidation(typeof(PhysicalParentNode), "NmeaValidation")]
        [ScsCrossReference("SentenceLabel")]
        public string SentenceQualifier
        {
            get { return _sentenceQualifier; }
            set { SetProperty(ref _sentenceQualifier, value); }
        }

        [CustomValidation(typeof(PhysicalParentNode), "SerialValidation")]
        [ScsCrossReference("RecordSize")]
        public int? SentenceLength
        {
            get { return _sentenceLength; }
            set { SetProperty(ref _sentenceLength, value); }
        }

        [Required]
        public IoPort IoPort
        {
            get { return _ioPort; }
            set { SetProperty(ref _ioPort, value); }
        }

        public static ValidationResult NmeaValidation(object obj, ValidationContext context)
        {
            var parent = (PhysicalParentNode)context.ObjectInstance;

            if (parent.DeviceType == DeviceTypeEnum.Nmea)
            {
                //only the data field position is required
                if (parent.SentenceQualifier == null)
                    return new ValidationResult("Sentence Qualifier is required for NMEA devices", new List<string> { "SentenceQualifier" });
            }
            return ValidationResult.Success;
        }

        public static ValidationResult SerialValidation(object obj, ValidationContext context)
        {
            var parent = (PhysicalParentNode)context.ObjectInstance;

            if (parent.DeviceType == DeviceTypeEnum.Serial)
            {
                //only the data field position is required
                if (parent.SentenceLength == null)
                    return new ValidationResult("Sentence Length is required for serial devices", new List<string> { "SentenceLength" });
            }
            return ValidationResult.Success;
        }
    }
}
