﻿using System;
using System.IO;
using System.Linq;
using System.Xml;
using RVDAC.Common.Models;
using RVDAC.Common.ScsClient;

namespace RVDAC.Common
{
    public static class ReportGenerator
    {
        private static string ValueOrPlaceholder(string val)
        {
            return string.IsNullOrEmpty(val) ? "*None*" : val;
        }

        public static void GeneratePositionTable(NodeBase sensor, XmlWriter html)
        {
            html.WriteStartElement("table");
            html.WriteAttributeString("border", "1");
            //header
            html.WriteStartElement("tr");
            html.WriteElementString("th", "X (m)");
            html.WriteElementString("th", "Y (m)");
            html.WriteElementString("th", "Z (m)");
            html.WriteElementString("th", "Location Description");
            html.WriteEndElement(); // </tr>

            //meat
            html.WriteStartElement("tr");
            html.WriteElementString("td", sensor.PositionX.ToString());
            html.WriteElementString("td", sensor.PositionX.ToString());
            html.WriteElementString("td", sensor.PositionX.ToString());
            html.WriteElementString("td", sensor.PositionDescription);
            html.WriteEndElement(); // </tr>

            html.WriteEndElement(); // </table>
        }

        public static void GenerateBasicInfoTable(NodeBase sensor, XmlWriter html)
        {
            html.WriteStartElement("table");
            html.WriteAttributeString("border", "1");
            //header
            html.WriteStartElement("tr");
            html.WriteElementString("th", "Make");
            html.WriteElementString("th", "Model");
            html.WriteElementString("th", "Serial No");
            html.WriteEndElement(); // </tr>

            //meat
            html.WriteStartElement("tr");
            html.WriteElementString("td", sensor.Make);
            html.WriteElementString("td", sensor.Model);
            html.WriteElementString("td", sensor.Serial);
            html.WriteEndElement(); // </tr>

            html.WriteEndElement(); // </table>
        }

        public static void GenerateChildSensorTable(ParentNodeBase sensor, XmlWriter html)
        {
            html.WriteStartElement("table");
            html.WriteAttributeString("border","1");
            if (sensor.DeviceType == NodeBase.DeviceTypeEnum.Serial)
            {
                html.WriteStartElement("tr");
                html.WriteElementString("th", "Name");
                html.WriteElementString("th", "Sample");
                html.WriteElementString("th", "Start Index");
                html.WriteElementString("th", "End Index");
                html.WriteElementString("th", "Data-Type");
                html.WriteEndElement(); // </tr>
                foreach (var child in sensor.ChildNodes)
                {
                    html.WriteStartElement("tr");
                    html.WriteElementString("td", child.Name);
                    html.WriteElementString("td", ValueOrPlaceholder(child.Data.RawValue));
                    html.WriteElementString("td", child.StartIndex.ToString());
                    html.WriteElementString("td", child.EndIndex.ToString());
                    html.WriteElementString("td", child.DecodeType.ToString());
                    html.WriteEndElement(); // </tr>
                }
            }
            else
            {
                html.WriteStartElement("tr");
                html.WriteElementString("td", "Name");
                html.WriteElementString("td", "Sample");
                html.WriteElementString("td", "Position Index");
                html.WriteElementString("td", "Units Index");
                html.WriteElementString("td", "Data-Type");
                html.WriteElementString("td", "Comment");
                html.WriteEndElement(); // </tr>
                foreach (var child in sensor.ChildNodes)
                {
                    html.WriteStartElement("tr");
                    html.WriteElementString("td", child.Name);
                    html.WriteElementString("td", child.Data.RawValue);
                    html.WriteElementString("td", child.DataFieldPosition.ToString());
                    html.WriteElementString("td", child.UnitFieldPosition.ToString());
                    html.WriteElementString("td", child.DecodeType.ToString());
                    html.WriteElementString("td", child.Comment);
                    html.WriteEndElement(); // </tr>
                }
            }
            html.WriteEndElement(); // </table>
        }

        public static void GenerateReport(SensorConfiguration config, string filePath)
        {
            filePath = Path.ChangeExtension(filePath, "html");
            var updater = new ScsMessageParser();
            updater.UpdateChildrenFromSampleString(config);
            var settings = new XmlWriterSettings()
            {
                OmitXmlDeclaration = true,
                Indent = true,
                NamespaceHandling = NamespaceHandling.OmitDuplicates
            };
            using (var html = XmlWriter.Create(filePath, settings))
            {
                html.WriteStartDocument();
                html.WriteDocType("html", null, null, null);
                html.WriteStartElement("html");
                html.WriteStartElement("head");
                html.WriteComment("Created using RVDAC.Common.ReportGenerator");
                html.WriteEndElement(); // </head>
                html.WriteStartElement("body");
                html.WriteElementString("H2","SCS Sensor Definitions");
                html.WriteElementString("p","Generated: " + DateTime.Now.ToString("s"));
                foreach (var sensor in config.AllParentSensors)
                {
                    html.WriteStartElement("a");
                    html.WriteAttributeString("href","#" + sensor.Guid.ToString());
                    html.WriteString(sensor.Name);
                    html.WriteEndElement(); // </a>
                    html.WriteElementString("span", " -- " + sensor.Comment);
                    html.WriteElementString("br", "");
                    var independentSensors = sensor.ChildNodes.Where(x => x.IsIndependentSensor()).ToList();
                    if (independentSensors.Any())
                    {
                        html.WriteStartElement("ul");
                        html.WriteAttributeString("style", "margin: 0px 0px 0px 12px;");
                        foreach (var independent in independentSensors)
                        {
                            html.WriteStartElement("li");
                            html.WriteStartElement("a");
                            html.WriteAttributeString("href", "#" + independent.Guid.ToString());
                            html.WriteString(independent.Name);
                            html.WriteEndElement(); //</a>
                            html.WriteElementString("span", " -- " + independent.Comment);
                            html.WriteEndElement(); //</li>
                        }
                        html.WriteEndElement(); //</ul>
                    }
                }
                foreach (var sensor in config.AllParentSensors)
                {
                    html.WriteElementString("hr", "");
                    html.WriteStartElement("div");
                    html.WriteStartElement("a");
                    html.WriteAttributeString("id", sensor.Guid.ToString());
                    html.WriteElementString("h2", sensor.Name);
                    html.WriteEndElement(); // </a>
                    html.WriteElementString("h3","Comment");
                    html.WriteElementString("p", ValueOrPlaceholder(sensor.Comment));
                    html.WriteElementString("h3", "Description");
                    html.WriteElementString("p", ValueOrPlaceholder(sensor.Description));
                    html.WriteElementString("h3", "Device Information");
                    GenerateBasicInfoTable(sensor, html);
                    if (sensor.PositionX != null)
                    {
                        html.WriteElementString("h3", "Device Location");
                        GeneratePositionTable(sensor, html);
                    }
                    html.WriteElementString("h3", "Sample Message");
                    html.WriteElementString("p", ValueOrPlaceholder(sensor.SampleMessage));
                    html.WriteElementString("h3","Message Parsing Information");

                    GenerateChildSensorTable(sensor, html);
                    html.WriteEndElement(); // </div>

                    //for independent child sensors
                    
                    var independentSensors = sensor.ChildNodes.Where(x => x.IsIndependentSensor()).ToList();
                    if (independentSensors.Any())
                    {
                        html.WriteStartElement("div");
                        html.WriteAttributeString("style", "margin-left: 15px;");
                        html.WriteElementString("h2", "Associated Independent Sensors");
                        foreach (var independent in independentSensors)
                        {
                            html.WriteStartElement("a");
                            html.WriteAttributeString("id", independent.Guid.ToString());
                            html.WriteElementString("h3", independent.Name);
                            html.WriteEndElement(); //</a>
                            html.WriteElementString("h4", "Description");
                            html.WriteElementString("p", independent.Description);
                            html.WriteElementString("h4", "Device Information");
                            GenerateBasicInfoTable(independent, html);
                            if (independent.PositionX != null)
                            {
                                html.WriteElementString("h4", "Device Location");
                                GeneratePositionTable(independent, html);
                            }
                        }
                        html.WriteEndElement(); // </div>
                    }
                }
                html.WriteEndElement(); // </body>
                html.WriteEndElement(); // </html>
                html.WriteEndDocument();
            }
        }
    }
}
