﻿using System;
using System.Collections.Generic;
using ProtoBuf;

namespace RVDAC.Common.Messenger
{
    [ProtoContract]
    public class MessengerMessage
    {
        [ProtoMember(1)]
        public DateTime DateTime { get; set; }

        [ProtoMember(2)]
        public string InstanceName { get; set; }

        [ProtoMember(3)]
        public bool IsRealTime { get; set; }

        [ProtoMember(4)]
        public List<MessengerDataPoint> DataPoints { get; set; }

        [ProtoMember(5)]
        public string ScsConfiguration { get; set; }

        public bool IsConfigurationFile()
        {
            return !string.IsNullOrEmpty(ScsConfiguration);
        }
    }
}
