﻿using System;
using System.Xml.Serialization;
using ProtoBuf;

namespace RVDAC.Common.Messenger
{
    [ProtoContract]
    public struct MessengerDataPoint
    {
        public MessengerDataPoint(short id, double? value)
        {
            SensorId = id;
            Value = value;
        }

        [ProtoMember(1)]
        public short SensorId { get; set; }

        [ProtoMember(2)]
        [XmlIgnore]
        public double? Value { get; set; }

        [XmlElement("Value", IsNullable = false)]
        public string SerializableValue
        {
            get { return Value == null ? "" : Value.ToString(); }
            set { Value = Convert.ToDouble(value); }
        }
    }
}
