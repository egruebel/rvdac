﻿using System;
using System.IO;
using System.IO.Compression;
using ProtoBuf;

namespace RVDAC.Common.Messenger
{
    public static class MessengerSerializer
    {
        public static readonly byte[] Delimiter = {4, 3, 2, 1};

        public static byte[] Serialize(object item)
        {
            using (var memStream = new MemoryStream())
            {
                Serializer.Serialize(memStream, item);
                var serializedMsg = memStream.ToArray();
                using (var compressedMsg = new MemoryStream())
                {
                    using (var gzipStream = new GZipStream(compressedMsg, CompressionMode.Compress, true))
                    {
                        gzipStream.Write(serializedMsg, 0, serializedMsg.Length);
                        
                    }
                    //This is the payload of the message
                    //return PrependLengthPrefix(compressedMsg.ToArray());
                    return AppendDelimiter(compressedMsg.ToArray());
                }
            }
        }

        public static MessengerMessage Deserialize(byte[] data)
        {
            using (var mem = new MemoryStream(data))
            {
                using (var gz = new GZipStream(mem, CompressionMode.Decompress))
                {
                    var msg = Serializer.Deserialize<MessengerMessage>(gz);
                    return msg;
                }
            }
        }

        private static byte[] PrependLengthPrefix(byte[] byteArray)
        {
            var lengthPrefix = BitConverter.GetBytes(byteArray.Length + 4);
            //create a new array for the message + length prefix
            byte[] finalPackage = new byte[byteArray.Length + 4];
            //insert length prefix into new byte array
            for (int i = 0; i < 3; i++)
            {
                finalPackage[i] = lengthPrefix[i];
            }
            //copy the rest of the compressed object
            Array.Copy(byteArray, 0, finalPackage, 4, byteArray.Length); // copy the old values
            return finalPackage;
        }

        private static byte[] AppendDelimiter(byte[] sourceArray)
        {
            var delim = Delimiter;
            var completeArray = new byte[sourceArray.Length + delim.Length];
            Array.Copy(sourceArray,completeArray,sourceArray.Length);
            for (int i = sourceArray.Length; i < sourceArray.Length + delim.Length; i++)
            {
                completeArray[i] = delim[i - sourceArray.Length];
            }
            return completeArray;
        }
    }
}
