﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using RVDAC.Common.Models;
using RVDAC.Common.ScsClient;

namespace RVDAC.Common
{
    public class SensorConfiguration: ValidatableModel
    {
        public BindableObservableCollection<ParentNodeBase> AllParentSensors { get; set; }
        public ObservableCollection<string> MeasurementClasses { get; set; }
        public BindableObservableCollection<IoPort> IoPorts { get; set; }
        private bool _configHasErrors;
        private string _filePath;

        public SensorConfiguration()
        {
            AllParentSensors = new BindableObservableCollection<ParentNodeBase>();
            MeasurementClasses = new ObservableCollection<string>();
            IoPorts = new BindableObservableCollection<IoPort>();

            AllParentSensors.CollectionChanged += Config_CollectionChanged;
            AllParentSensors.ItemInCollectionChanged += Config_CollectionChanged;
            IoPorts.CollectionChanged += Config_CollectionChanged;
            IoPorts.ItemInCollectionChanged += Config_CollectionChanged;
        }

        public Guid Guid { get; set; } = Guid.Empty;

        //public XDocument ConfigXml { get; set; }

        private void Config_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            var dirtyFlag = false;
            var errorFlag = false;

            var triggeredCollection = (IEnumerable<ValidatableModel>) sender;

            foreach (var obj in triggeredCollection)
            {
                if (obj.IsDirty)
                    dirtyFlag = true;
                if (obj.HasErrors)
                    errorFlag = true;
            }

            IsDirty = dirtyFlag;
            ConfigHasErrors = errorFlag;
        }

        public List<ChildNode> AllChildSensors()
        {
            return AllParentSensors.SelectMany(x => x.ChildNodes).ToList();
        }

        public List<ChildNode> AllNumericSensors()
        {
            return AllParentSensors.SelectMany(x => x.ChildNodes).Where(x => x.DecodeType != ChildNode.DecodeTypeEnum.Text).ToList(); 
        }

        public IEnumerable<NodeBase> AllSensors()
        {
            foreach (var p in AllParentSensors)
            {
                yield return p;
                foreach (var c in p.ChildNodes)
                    yield return c;
            }
        }

        //Forces a validation error if any child has errors
        [Range(typeof(bool), "False", "False", ErrorMessage = "One or more objects has validation errors")]
        public bool ConfigHasErrors
        {
            get { return _configHasErrors; }
            private set { SetProperty(ref _configHasErrors, value); }
        }

        public bool IsValid { get; set; }

        public bool IsInitialized { get; set; } = false;

        public string FilePath
        {
            get { return _filePath; }
            set { SetProperty(ref _filePath, value); }
        }

        public SensorValueHolder[] CopyOfSensorValues()
        {
            var allSensors = AllSensors().ToList();
            var allSensorsArray = new SensorValueHolder[allSensors.Count];
            for (int i = 0; i < allSensors.Count; i++)
            {
                allSensorsArray[i] = new SensorValueHolder(allSensors[i]);
            }
            return allSensorsArray;
        } 

        public void ClearDirtyStatus()
        {
            foreach (var s in AllParentSensors)
            {
                foreach (var c in s.ChildNodes)
                    c.IsDirty = false;
                s.IsDirty = false;
            }
            foreach (var i in IoPorts)
                i.IsDirty = false;
            //this
            IsDirty = false;
        }

        public void ValidateAll()
        {
            foreach (var parent in AllParentSensors)
            {
                foreach (var child in parent.ChildNodes)
                {
                    child.Validate();
                }
                parent.Validate();
            }
            foreach(var i in IoPorts)
                i.Validate();
        }

        public void ValidateAllAsync()
        {
            foreach (var parent in AllParentSensors)
            {
                foreach (var child in parent.ChildNodes)
                {
                    child.ValidateAsync();
                }
                parent.ValidateAsync();
            }
            foreach (var i in IoPorts)
                i.ValidateAsync();
        }

        public NodeCrossReferenceResult CheckCrossReference(Guid sensorGuid)
        {
            var sensor = GetSensorByGuid(sensorGuid);
            if (sensor == null)
                return new NodeCrossReferenceResult();
            var crf = new NodeCrossReferenceResult();
            crf.ReferencedNodeList = new List<NodeBase>();
            var virtuals = AllParentSensors.OfType<VirtualParentBase>().ToList();

            if (sensor is ChildNode)
            {
                foreach (var v in virtuals)
                {
                    var src = v.SourceCollection.Select(x => x.Source);
                    if (src.Any(x => x.Guid.Equals(sensorGuid)))
                    {
                        crf.ReferencedNodeList.Add(v);
                    }
                }
            }
            else
            {
                //parent
                foreach (var child in ((ParentNodeBase)sensor).ChildNodes)
                {
                    var childGuid = child.Guid;
                    foreach (var v in virtuals)
                    {
                        var src = v.SourceCollection.Select(x => x.Source).Where(x => x != null);
                        if (src.Any(x => x.Guid.Equals(childGuid)))
                        {
                            crf.ReferencedNodeList.Add(v);
                        }
                    }
                }
            }
            crf.IsReferenced = crf.ReferencedNodeList.Any();
            return crf;
        }

        public NodeBase GetSensorByGuid(Guid sensorGuid)
        {
            var sensor = AllParentSensors.FirstOrDefault(x => x.Guid == sensorGuid);
            if (sensor == null)
            {
                var clickedChild = AllParentSensors.SelectMany(x => x.ChildNodes).FirstOrDefault(x => x.Guid == sensorGuid);
                return clickedChild;
            }
            return sensor;
        }

        public void PopulateDefaultMeasurementClasses()
        {
            if (MeasurementClasses.Any())
            {
                throw new InvalidOperationException("Populating default measurement classes is a one-time only operation");
            }
            MeasurementClasses.Add("Heading");
            MeasurementClasses.Add("Track");
            MeasurementClasses.Add("GroundSpeed");
            MeasurementClasses.Add("WaterSpeed");
            MeasurementClasses.Add("Latitude");
            MeasurementClasses.Add("Longitude");
            MeasurementClasses.Add("SatsInUse");
            MeasurementClasses.Add("Altitude");
            MeasurementClasses.Add("FixQuality");
            MeasurementClasses.Add("SatsInUse");
            MeasurementClasses.Add("HDOP");
            MeasurementClasses.Add("WindSpeedRelative");
            MeasurementClasses.Add("WindDirectionRelative");
            MeasurementClasses.Add("WindSpeedTrue");
            MeasurementClasses.Add("WindDirectionTrue");

            MeasurementClasses.Add("AirTemperature");
            MeasurementClasses.Add("RelativeHumidity");
            MeasurementClasses.Add("BarometricPressure");
            MeasurementClasses.Add("Precipitation");
            MeasurementClasses.Add("SeaTemperature");
            MeasurementClasses.Add("Salinity");
            MeasurementClasses.Add("SoundVelocity");
            MeasurementClasses.Add("Conductivity");
            MeasurementClasses.Add("Fluorescence");

            MeasurementClasses.Add("LWIrradiance");
            MeasurementClasses.Add("SWIrradiance");

            MeasurementClasses.Add("Depth");
            MeasurementClasses.Add("Pitch");
            MeasurementClasses.Add("Roll");
            MeasurementClasses.Add("Heading");
            MeasurementClasses.Add("Heave");
            MeasurementClasses.Add("Yaw");
        }

    }
}
