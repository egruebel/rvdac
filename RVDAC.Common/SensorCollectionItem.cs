﻿using System;
using System.Xml.Serialization;

namespace RVDAC.Common
{
    public class SensorCollectionItem
    {
        [XmlText]
        public Guid Guid { get; set; }

        [XmlAttribute]
        public bool UseRawValue { get; set; } = false;
    }
}
