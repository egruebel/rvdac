﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace RVDAC.Common
{
    public sealed class BindableObservableCollection<T>: ObservableCollection<T> where T : INotifyPropertyChanged
    {
        public event EventHandler<NotifyCollectionChangedEventArgs> ItemInCollectionChanged;

        public BindableObservableCollection()
        {
            CollectionChanged += BindableObservableCollection_CollectionChanged;
        }

        public BindableObservableCollection(IEnumerable<T> pItems) : this()
        {
            foreach (var item in pItems)
            {
                Add(item);
            }
        }

        private void BindableObservableCollection_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (INotifyPropertyChanged item in e.NewItems)
                {
                    item.PropertyChanged += Item_PropertyChanged; 
                }
            }
            if (e.OldItems != null)
            {
                foreach (INotifyPropertyChanged item in e.OldItems)
                {
                    item.PropertyChanged -= Item_PropertyChanged;
                }
            }
        }

        private void OnItemInCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            ItemInCollectionChanged?.Invoke(this, e);
        }

        private void Item_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var args = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace,sender, sender, IndexOf((T)sender));
            OnItemInCollectionChanged(args);
        }
    }
}
