﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using RVDAC.Common.Converters;
using RVDAC.Common.Models;

namespace RVDAC.Common
{
    public static class ConfigExporter
    {
        //private XmlWriter xmlWriter;
        //private int _id = -1;

        private static void TestFileIo(string filePath)
        {
            //test this file
            var fileInfo = new FileInfo(filePath);
            //does it exist?
            if (!fileInfo.Exists)
            {
                var stream = fileInfo.Create();
                stream.Close();
            }
                
            //can we open it and read?
            using (var stream = fileInfo.OpenWrite())
            {
                stream.WriteByte(0);
            }
            //okay good
        }

        private static string GetNextId(int input, out int output)
        {
            input++;
            output = input;
            return output.ToString();
        }

        public static string ToString(SensorConfiguration config)
        {
            using (var sw = new StringWriter())
            {
                using (var xmlWriter = XmlWriter.Create(sw))
                {
                    BuildXmlWriter(config, xmlWriter, false);
                }
                return sw.ToString();
            }
        }

        public static void CopyToFile(SensorConfiguration config, string filePath)
        {
            //This method will throw an exception back to the caller if there's an IO problem
            TestFileIo(filePath);
            var settings = new XmlWriterSettings()
            {
                Indent = true,
                NamespaceHandling = NamespaceHandling.OmitDuplicates
            };
            using (var xmlWriter = XmlWriter.Create(filePath, settings))
            {

                BuildXmlWriter(config, xmlWriter,false);
            }
        }

        public static void OverwriteToFile(SensorConfiguration config, string filePath)
        {
            //This method will throw an exception back to the caller if there's an IO problem
            TestFileIo(filePath);
            var settings = new XmlWriterSettings()
            {
                Indent = true,
                NamespaceHandling = NamespaceHandling.OmitDuplicates
            };
            using (var xmlWriter = XmlWriter.Create(filePath, settings))
            {

                BuildXmlWriter(config, xmlWriter,true);
            }
        }

        private static void BuildXmlWriter(SensorConfiguration config, XmlWriter xmlWriter, bool replaceGuid = true)
        {
            xmlWriter.WriteStartDocument();
            xmlWriter.WriteComment("Created using " + Assembly.GetCallingAssembly().FullName);
            xmlWriter.WriteStartElement("SensorList");
            var guid = replaceGuid ? Guid.NewGuid().ToString() : config.Guid.ToString();
            xmlWriter.WriteAttributeString("config-guid", guid);
            WriteMeasurementClasses(config, xmlWriter);

            int nextId = -1;

            foreach (var parent in config.AllParentSensors)
            {
                xmlWriter.WriteStartElement("Sensor");
                WriteTag("SensorId", GetNextId(nextId, out nextId), xmlWriter);
                //start with shared fields
                WriteFields(parent, xmlWriter);
                //Do the base sensors if this is a derived
                if (parent.IsVirtual)
                {
                    int i = 1;
                    var vParent = parent as VirtualParentBase;
                    foreach (var baseSensor in vParent.SourceCollection)
                    {
                        WriteTag("BaseSensor" + i, baseSensor.Source.Name, xmlWriter);
                        i++;
                    }
                    while (i < 7)
                    {
                        WriteTag("BaseSensor" + i, null, xmlWriter);
                        i++;
                    }
                }
                else
                {
                    //physical
                    var pParent = parent as PhysicalParentNode;
                    WriteFields(pParent.IoPort, xmlWriter);
                }

                //now all unused fields
                WriteUnusedFields(xmlWriter);
                xmlWriter.WriteEndElement();

                foreach (var child in parent.ChildNodes)
                {
                    xmlWriter.WriteStartElement("Sensor");
                    WriteTag("SensorId", GetNextId(nextId, out nextId), xmlWriter);
                    WriteFields(child, xmlWriter);
                    WriteUnusedFields( xmlWriter);
                    xmlWriter.WriteEndElement();
                }
            }
            xmlWriter.WriteEndDocument();
        }

        private static void WriteFields(object configObject, XmlWriter xmlWriter)
        {
            var type = configObject.GetType();

            // Get the PropertyInfo object:
            foreach (var property in type.GetProperties())
            {
                foreach (
                    var attribute in
                        property.GetCustomAttributes(false)
                            .Where(x => x.GetType() == typeof(ScsCrossReferenceAttribute)))
                {
                    var crossRef = (ScsCrossReferenceAttribute)attribute;
                    object value = property.GetValue(configObject, null);
                    if (crossRef.ValueConverterType != null)
                    {
                        //There's a special conversion necessary
                        var converter = (ILegacyConverter)Activator.CreateInstance(crossRef.ValueConverterType);

                        value = converter.ConvertToLegacy(value);
                    }

                    if (crossRef.IsExtendedField)
                    {
                        //This is a new field that was not part of legacy SCS
                        xmlWriter.WriteStartElement(crossRef.ReferenceTo);
                        xmlWriter.WriteAttributeString("extended-field", "True");
                        var writeValue = value ?? "No Value";
                        xmlWriter.WriteString(writeValue.ToString());
                        xmlWriter.WriteEndElement();
                    }
                    else
                    {
                        WriteTag(crossRef.ReferenceTo, value, xmlWriter);
                    }
                }
            }
        }

        private static void WriteMeasurementClasses(SensorConfiguration config, XmlWriter xmlWriter)
        {
            var classesToString = "";
            var lastItem = config.MeasurementClasses.LastOrDefault();
            if (lastItem == null)
                return;
            foreach (var m in config.MeasurementClasses)
            {
                classesToString += m;
                if (!m.Equals(lastItem))
                    classesToString += ";";
            }
            xmlWriter.WriteAttributeString("measurement-classes", classesToString);
        }

        private static void WriteTag(string elementName, object value, XmlWriter xmlWriter)
        {
            if (value == null)
            {
                xmlWriter.WriteElementString(elementName, string.Empty, "No Value");
                return;
            }
            xmlWriter.WriteElementString(elementName, string.Empty, value.ToString());
        }

        private static void WriteUnusedFields(XmlWriter xmlWriter)
        {
            foreach (var item in UnusedFields)
            {
                switch ((FieldType) item.Value)
                {
                    case FieldType.Boolean:
                        xmlWriter.WriteElementString(item.Key, string.Empty, "FALSE");
                        break;
                    case FieldType.String:
                        xmlWriter.WriteElementString(item.Key, string.Empty, "No Value");
                        break;
                    case FieldType.Const30:
                        xmlWriter.WriteElementString(item.Key, string.Empty, "30");
                        break;
                    case FieldType.Empty:
                        xmlWriter.WriteElementString(item.Key, string.Empty, string.Empty);
                        break;
                    case FieldType.Integer:
                        xmlWriter.WriteElementString(item.Key, string.Empty, "3.40282346638529E+38");
                        break;
                    case FieldType.Const1:
                        xmlWriter.WriteElementString(item.Key, string.Empty, "1");
                        break;
                    case FieldType.ConstNeg1:
                        xmlWriter.WriteElementString(item.Key, string.Empty, "-1");
                        break;
                    default:
                        throw new ArgumentException("Unhandled FieldType");
                }
            }
        }

        public static Dictionary<string, FieldType> UnusedFields
        {
            get
            {
                return new Dictionary<string, FieldType>
                {
                    {"AquaPackDataType", FieldType.String},
                    {"CalibrationFile", FieldType.String},
                    {"CogSensorId", FieldType.String},
                    {"DeltaCheck", FieldType.Boolean},
                    {"DeltaHigh", FieldType.Integer},
                    {"DeltaLow", FieldType.Integer},
                    {"DenominatorSensorId", FieldType.String},
                    {"ExtendLabel", FieldType.Boolean},
                    {"HdgSensorId", FieldType.String},
                    {"HistoryElements", FieldType.Const30},
                    {"IncludeInCompressFile", FieldType.Boolean},
                    {"LabelExtenders", FieldType.String},
                    {"LabFileDecimation", FieldType.Const30},
                    {"LineEqOffset", FieldType.Integer},
                    {"LineEqSlope", FieldType.Integer},
                    {"LoggingFolder", FieldType.Empty},
                    {"LoggingRate", FieldType.ConstNeg1},
                    {"ManualInput", FieldType.String},
                    {"ManualProgramType", FieldType.String},
                    {"NarrowBeamSensorID", FieldType.String},
                    {"NetConnection", FieldType.String},
                    {"NumberofDataCharacters", FieldType.String},
                    {"NumberOfUnitsCharacters", FieldType.String},
                    {"NumeratorSensorId", FieldType.String},
                    {"OperationType", FieldType.String},
                    {"OutputPrompt", FieldType.String},
                    {"RangeCheck", FieldType.Boolean},
                    {"RangeHigh", FieldType.Integer},
                    {"RangeLow", FieldType.Integer},
                    {"SimFileName", FieldType.String},
                    {"SogSensorId", FieldType.String},
                    {"SoundVelocitySensorId", FieldType.String},
                    {"SyncCheck", FieldType.Boolean},
                    {"SyncPosition", FieldType.String},
                    {"SyncString", FieldType.String},
                    {"TrueWindType",FieldType.String },
                    {"QueueMember",FieldType.Const1},
                    {"UpdateRate",FieldType.Const1},
                    {"TranslationList", FieldType.String},
                    {"TsgDataType", FieldType.String},
                    {"WindDirectionSensorId", FieldType.String},
                    {"WindSpeedSensorId", FieldType.String}
                };
            }
        }

        public enum FieldType
        {
            String,
            Integer,
            Boolean,
            Const30,
            Const1,
            ConstNeg1,
            Empty
        }
    }
}