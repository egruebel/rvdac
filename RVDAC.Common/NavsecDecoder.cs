﻿
using System;

namespace RVDAC.Common
{
    public static class NavsecDecoder
    {
        public static bool ToSignedDecimal(string value, string unit, out double val)
        {
            //some quick error checks
            double ddmmss;
            val = 0.0;
            if (!double.TryParse(value, out ddmmss))
            {
                return false;
            }
            unit = unit.ToUpper();
            if (!(unit == "N" || unit == "S" || unit == "E" || unit == "W"))
            {
                return false;
            }
            //if you've made it here we've already converted to value to a double
            ddmmss = (ddmmss / 100);

            var degrees = (int)ddmmss;

            var minutesseconds = ((ddmmss - degrees) * 100) / 60.0;

            //south and west are negative
            if (unit == "S" || unit == "W")
            {
                val = (degrees + minutesseconds) * -1;
                val = Math.Round(val, 4, MidpointRounding.AwayFromZero);
                return true;
            }

            val = degrees + minutesseconds;
            val = Math.Round(val, 4, MidpointRounding.AwayFromZero);
            return true;
        }
    }
}
