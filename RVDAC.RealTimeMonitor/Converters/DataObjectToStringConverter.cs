﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using RVDAC.Common.Models;

namespace RVDAC.RealTimeMonitor.Converters
{
    class DataObjectToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var obj = (NodeDataObject) value;
            if (obj == null)
                return "-NULL-";
            if (obj.RawValue == null)
                return "-NULL-";
            return obj.RawValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}