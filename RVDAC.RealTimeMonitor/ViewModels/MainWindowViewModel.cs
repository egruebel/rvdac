﻿using System;
using System.ComponentModel;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using RVDAC.Common.LogFile;
using RVDAC.Common.ScsClient;
using RVDAC.UI;
using RVDAC.UI.Views;

namespace RVDAC.RealTimeMonitor.ViewModels
{
    class MainWindowViewModel: ViewModelBase
    {
        private string _scsServerIp = "127.0.0.1";
        private int _pollInterval;
        private bool _hasError;

        public ScsClient ScsClient { get; } 

        public MainWindowViewModel()
        {
#if DEBUG
            //Stops Visual Studio from running this code in the Designer Window
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject())) return;
            _scsServerIp = ScsClient.DebugScsServerIp;
#endif
            var s1 = "$GPGGA,123519,4807.038,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*47";
            var s2 = "$GPGGA,123519,4807.038,N,01132.000,E,1,08,0.9,545.4,M,46.9,M,,*47";

            var r1 = ScsMessageParser.NmeaChecksumIsValid(s1);
            var r2 = ScsMessageParser.NmeaChecksumIsValid(s2);

            ScsClient = new ScsClient(_scsServerIp, 505);
            Task.Run(() => ScsClient.BeginAcquisition(100));
        }

        public int PollInterval
        {
            get { return _pollInterval; }
            set
            {
                _pollInterval = value;
                ScsClient.PollIntervalMs = _pollInterval;
            }
        }

        public bool HasError
        {
            get { return _hasError;}
            set
            {
                if (!value == _hasError)
                {
                    _hasError = value;
                    OnPropertyChanged(nameof(HasError));
                }
            }
        }

        public int[] PollIntervalSelector
        {
            get { return new int[] {2000, 1000, 750, 500, 200, 100, 0}; }
        }

        public double[] FontSelector
        {
            get { return new double[] {12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 28, 30}; }
        }

    }
}