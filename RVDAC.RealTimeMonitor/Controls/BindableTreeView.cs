﻿using System.Windows;
using System.Windows.Controls;

namespace RVDAC.RealTimeMonitor.Controls
{
    internal class BindableTreeView : TreeView
    {
        public static DependencyProperty SelectedItemObject = DependencyProperty.Register("SelectedObject",
            typeof (object), typeof (BindableTreeView), new UIPropertyMetadata(null));

        public BindableTreeView()
        {
            SelectedItemChanged += SelectedItem_Changed;
        }

        void SelectedItem_Changed(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (SelectedItem != null)
            {
                SetValue(SelectedItemObject, SelectedItem);
            }
        }

        public object SelectedObject
        {
            get { return (object) GetValue(SelectedItemObject); }
            set { SetValue(SelectedItemObject, value); }
        }
    }
}